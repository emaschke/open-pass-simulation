/**********************************************************************
* Copyright (c) 2018-2019 in-tech GmbH
*               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/


#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "fakeAgent.h"
#include "fakeStochastics.h"
#include "fakeParameter.h"
#include "fakePublisher.h"
#include "fakeRadio.h"
#include "fakeWorld.h"
#include "fakeWorldData.h"

#include "sensorCar2X.h"
#include "common/sensorDataSignal.h"
#include "core/opSimulation/modules/World_OSI/RadioImplementation.h"

using ::testing::_;
using ::testing::Return;
using ::testing::DoubleEq;
using ::testing::ReturnRef;
using ::testing::DoubleNear;
using ::testing::Expectation;
using ::testing::NiceMock;

TEST(Radio_UnitTests, NoSendersRegistered_DetectsNothing)
{
    RadioImplementation radio{};
    std::vector<osi3::MovingObject> detectedObjects = radio.Receive(0.0, 0.0, 1e-3);

    ASSERT_TRUE(detectedObjects.empty());
}

TEST(Radio_UnitTests, TwoSendersInProximityRegistered_DetectsTwoObjects)
{
    osi3::MovingObject remoteObject;
    RadioImplementation radio{};

    radio.Send(20.0, 0.0, 100.0, remoteObject);
    radio.Send(100.0, 100.0, 100.0, remoteObject);
    std::vector<osi3::MovingObject> detectedObjects = radio.Receive(0.0, 0.0, 1e-7);

    ASSERT_EQ(detectedObjects.size(), 2);
}

TEST(Radio_UnitTests, ThreeSendersRegisteredOneTooFarAway_DetectsTwoObjects)
{
    osi3::MovingObject remoteObject;
    RadioImplementation radio{};

    radio.Send(20.0, 20.0, 100.0, remoteObject);
    radio.Send(100.0, 100.0, 100.0, remoteObject);
    radio.Send(1000.0, 1000.0, 1.0, remoteObject);
    std::vector<osi3::MovingObject> detectedObjects = radio.Receive(0.0, 0.0, 1e-6);

    ASSERT_EQ(detectedObjects.size(), 2);
}

TEST(Radio_UnitTests, TwoSendersRegisteredAtSensivityThreshold_DetectsTwoObjects)
{
    osi3::MovingObject remoteObject;
    RadioImplementation radio{};

    radio.Send(0.0, 0.0, 5.0, remoteObject);
    radio.Send(1000.0, 0.0, 5.0, remoteObject);
    std::vector<osi3::MovingObject> detectedObjects = radio.Receive(500.0, 0.0, 1.5e-6);

    ASSERT_EQ(detectedObjects.size(), 2);
}

TEST(Radio_UnitTests, OneSensorAtSensitivityThresholdOneOutsideThreshold_DetectsOneObject)
{
    osi3::MovingObject remoteObject;
    RadioImplementation radio{};

    radio.Send(0.0, 0.0, 5.0, remoteObject);
    radio.Send(1050.0, 0.0, 5.0, remoteObject);
    std::vector<osi3::MovingObject> detectedObjects = radio.Receive(500.0, 0, 1.5e-6);

    ASSERT_EQ(detectedObjects.size(), 1);
}

TEST(Radio_UnitTests, OneMountedSensor_DetectsTwoObjects)
{
    NiceMock<FakeWorld> fakeWorldInterface;

    NiceMock<FakeStochastics> fakeStochastics;
    ON_CALL(fakeStochastics, GetUniformDistributed(_, _)).WillByDefault(Return(1));
    ON_CALL(fakeStochastics, GetLogNormalDistributed(_, _)).WillByDefault(Return(1));

    NiceMock<FakeParameter> fakeParameters;
    NiceMock<FakePublisher> fakePublisher;

    std::map<std::string, double> fakeDoubles = {{"FailureProbability", 0}, {"Latency", 0}, {"Sensitivity", 1e-5},
        {"Longitudinal", 1.0}, {"Lateral", 1.0}, {"Height", 0.0},
        {"Pitch", 0.0}, {"Yaw", 0.0}, {"Roll", 0.0}
    };
    ON_CALL(fakeParameters, GetParametersDouble()).WillByDefault(ReturnRef(fakeDoubles));

    std::map<std::string, int> fakeInts = {{"Id", 0}};
    ON_CALL(fakeParameters, GetParametersInt()).WillByDefault(ReturnRef(fakeInts));

    NiceMock<FakeAgent> fakeAgentInterface;
    ON_CALL(fakeAgentInterface, GetPositionX()).WillByDefault(Return(100));
    ON_CALL(fakeAgentInterface, GetPositionY()).WillByDefault(Return(100));
    ON_CALL(fakeAgentInterface, GetYaw()).WillByDefault(Return(0));

    std::vector<const WorldObjectInterface*> fakeObjects;
    fakeObjects.push_back(&fakeAgentInterface);
    ON_CALL(fakeWorldInterface, GetWorldObjects()).WillByDefault(ReturnRef(fakeObjects));

    osi3::MovingObject movingObject1;
    osi3::MovingObject movingObject2;

    //Manipulate Radio
    std::vector<osi3::MovingObject> car2XObjects = {movingObject1, movingObject2};
    NiceMock<FakeRadio> fakeRadio;
    EXPECT_CALL(fakeRadio, Receive(DoubleEq(101), DoubleEq(101), DoubleEq(1e-5))).WillOnce(Return(car2XObjects));
    ON_CALL(fakeWorldInterface, GetRadio()).WillByDefault(ReturnRef(fakeRadio));

    SensorCar2X sensor(
        "",
        false,
        0,
        0,
        0,
        0,
        &fakeStochastics,
        &fakeWorldInterface,
        &fakeParameters,
        &fakePublisher,
        nullptr,
        &fakeAgentInterface);

    auto sensorData = sensor.DetectObjects();
    ASSERT_EQ(sensorData.moving_object_size(), 2);
}