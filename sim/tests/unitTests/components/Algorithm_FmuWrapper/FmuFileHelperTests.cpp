/*******************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/


#include "fmuFileHelper.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "osi3/osi_sensordata.pb.h"

//#include "include/fmuHandlerInterface.h"
#include "FmuHandler.h"

#include "fakeWorld.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeParameter.h"
#include "fakeRadio.h"


//#include "include/parameterInterface.h"
//#include "common/sensorDataSignal.h"

#include <vector>
#include <filesystem>
#include <sstream>
#include <QDir>
#include <QFile>

using ::testing::Mock;
using ::testing::_;
using ::testing::NiceMock;
using ::testing::ReturnArg;
using ::testing::ReturnRef;


std::vector<std::string> split(std::string string, std::string delimiter)
{
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = string.find(delimiter, pos_start)) != std::string::npos)
    {
        token = string.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(string.substr(pos_start));
    return res;
}

TEST(FmuFileHelperTests, TestGenerateFilename)
{
    FmuFileHelper::TraceEntry traceEntry;
    traceEntry.time = 0;
    traceEntry.osiType = "sv";

    const std::pair<const std::string, FmuFileHelper::TraceEntry> fileToOutputTrace("test", traceEntry);
    std::string outputType = "TestOutputType";

    auto fileName = FmuFileHelper::GenerateTraceFileName(outputType, fileToOutputTrace);

    std::string delimiter = "_";
    std::vector<std::string> fileNameSplits = split(fileName, delimiter);

    ASSERT_EQ(fileNameSplits.size(), 6);
    ASSERT_EQ(fileNameSplits[1], "sv");

    const auto currentInterfaceVersion = osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version);
    std::stringstream osiVersion;
    osiVersion << std::to_string(currentInterfaceVersion.version_major());
    osiVersion << currentInterfaceVersion.version_minor();
    osiVersion << currentInterfaceVersion.version_patch();
    ASSERT_EQ(fileNameSplits[2], osiVersion.str());

    ASSERT_EQ(fileNameSplits[3], std::to_string(GOOGLE_PROTOBUF_VERSION));
    ASSERT_EQ(fileNameSplits[4], std::to_string(0));

    delimiter = ".";
    std::vector<std::string> fileEndingSplit = split(fileNameSplits[5], delimiter);

    ASSERT_EQ(fileEndingSplit.size(), 2);
    ASSERT_EQ(fileEndingSplit[0], outputType);
    ASSERT_EQ(fileEndingSplit[1], "osi");
}

TEST(FmuFileHelperTests, WriteBinaryTraceFileNameCompleteTest)
{
    QString traceOutputDir{};

    const ParameterInterface *parameters;
    std::string fmuName = "testFmu";

    NiceMock<FakeWorld> fakeWorld;
    NiceMock<FakeAgent> fakeAgent;
    NiceMock<FakeCallback> fakeCallback;
    NiceMock<FakeParameter> fakeParameters;

    fmu_check_data_t *cdata;
    std::unordered_map<std::string, ValueReferenceAndType> fmuVariables;
    std::map<ValueReferenceAndType, FmuValue> *fmuVariableValues;

    //OsmpFmuHandler fmu{cdata, &fakeWorld, &fakeAgent, &fakeCallback, fmuVariables, fmuVariableValues, &fakeParameters};

    //TODO: Testing OSMPFmuHandlerFileOutput

}