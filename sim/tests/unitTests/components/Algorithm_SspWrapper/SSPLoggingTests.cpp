/*******************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/


#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "dontCare.h"
#include "osi3/osi_sensordata.pb.h"

#include "include/fmuHandlerInterface.h"
#include "fakeFmuWrapper.h"
#include "common/sensorDataSignal.h"

#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeWorld.h"

#include "sim/src/components/Algorithm_SspWrapper/AlgorithmSspWrapper.h"

#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/ConnectorHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/OSMPConnector.h"

#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputSignalVisitor.h"

#include <vector>
#include <sstream>

#include "SsdToSspNetworkParser.h"


using ::testing::Mock;
using ::testing::_;
using ::testing::NiceMock;
using ::testing::StrictMock;
using ::testing::Gt;
using ::testing::AtLeast;
using ::testing::ReturnArg;
using ::testing::ReturnRef;
using ::testing::DontCare;
using ::testing::Return;
using ::testing::Eq;

//using namespace ssp;

class SSPLoggingTests : public ::testing::Test
{
public:
    SSPLoggingTests()
    {
        init();
    }
    void init()
    {
        OpenPassInterfaceData::setValues("SspWrapper",
                                          false,
                                          0,
                                          0,
                                          0,
                                          100,
                                          nullptr,
                                          &fakeWorld,
                                          &fakeParameter,
                                          nullptr,
                                          &fakeAgent,
                                          &fakeCallback);
    }

protected:
    NiceMock<FakeWorld> fakeWorld;
    NiceMock<FakeAgent> fakeAgent;
    NiceMock<FakeParameter> fakeParameter;
    StrictMock<FakeCallback> fakeCallback;
};

TEST_F(SSPLoggingTests, ParserNoLogging)
{
    //Strict mock -> when a error is logged it will fail, because every call needs to have an expect_call
    EXPECT_CALL(fakeCallback, Log(Gt(CbkLogLevel::Error), _,  _, _)).Times(AtLeast(0));
    EXPECT_CALL(fakeCallback, Log(CbkLogLevel::Error, _,  _, _)).Times(0); //For strict mock not needed


    SsdToSspNetworkParser ssdToSspNetWorkParser;

    auto ssdSystem = std::make_shared<SsdSystem>("0");
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);

    auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_none);
    componentFirst->setPriority(0);
    componentFirst->setWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
    componentFirst->emplaceConnector("connector1", SspParserTypes::OSMPTripleLink {"output", "output_osiType"});
    componentFirst->SetParameters(parameters);
    ssdSystem->AddComponent(std::move(componentFirst));

    auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_none);
    componentSecond->setPriority(1);
    componentSecond->emplaceConnector("connector2", SspParserTypes::OSMPTripleLink {"input", "input_osiType"});
    componentSecond->addUpdateOutputParameter(0);
    componentSecond->SetParameters(parameters);
    ssdSystem->AddComponent(std::move(componentSecond));

    std::map<std::string, std::string> connection;
    connection.insert(std::make_pair("startElement", "component1"));
    connection.insert(std::make_pair("endElement", "component2"));
    connection.insert(std::make_pair("startConnector", "connector1"));
    connection.insert(std::make_pair("endConnector", "connector2"));
    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));

    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_NO_THROW(ssdToSspNetWorkParser.getRootSystem(ssdFiles));

}

TEST_F(SSPLoggingTests, ParserErrorLogging)
{
    //Strict mock -> when a error is logged it will fail, because every call needs to have an expect_call
    EXPECT_CALL(fakeCallback, Log(Gt(CbkLogLevel::Error), _,  _, _)).Times(AtLeast(0));
    EXPECT_CALL(fakeCallback, Log(CbkLogLevel::Error, _,  _, _)).Times(1);

    SsdToSspNetworkParser ssdToSspNetWorkParser;

    auto ssdSystem = std::make_shared<SsdSystem>("0");
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);

    auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_fmu_sharedlibrary);
    componentFirst->setPriority(0);
    componentFirst->setWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
    componentFirst->emplaceConnector("connector1", SspParserTypes::OSMPTripleLink {"output", "output_osiType"});
    componentFirst->SetParameters(parameters);
    ssdSystem->AddComponent(std::move(componentFirst));

    auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_fmu_sharedlibrary);
    componentSecond->setPriority(1);
    componentSecond->emplaceConnector("connector2", SspParserTypes::OSMPTripleLink {"input", "input_osiType"});
    componentSecond->addUpdateOutputParameter(0);
    componentSecond->SetParameters(parameters);
    ssdSystem->AddComponent(std::move(componentSecond));

    std::map<std::string, std::string> connection;
    connection.insert(std::make_pair("startElement", "component1"));
    connection.insert(std::make_pair("endElement", "component2"));
    connection.insert(std::make_pair("startConnector", "connector1"));
    connection.insert(std::make_pair("wrongConnector", "connector2"));
    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));

    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_THROW(ssdToSspNetWorkParser.getRootSystem(ssdFiles), std::runtime_error);

}

TEST_F(SSPLoggingTests, VisistTestNoError)
{

    //Strict mock -> when a error is logged it will fail, because every call needs to have an expect_call
    EXPECT_CALL(fakeCallback, Log(Gt(CbkLogLevel::Error), _,  _, _)).Times(AtLeast(0));
    EXPECT_CALL(fakeCallback, Log(CbkLogLevel::Error, _,  _, _)).Times(0); // not needed for strict mocks

    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorData.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorData.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorData.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);

    auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

    osi3::SensorData sensorData{};
    sensorData.mutable_version()->set_version_major(1);
    sensorData.mutable_version()->set_version_minor(2);
    sensorData.mutable_version()->set_version_patch(3);

    std::string serializedSensorData{};
    sensorData.SerializeToString(&serializedSensorData);
    fmi2_integer_t lo{}, hi{}, size{static_cast<fmi2_integer_t>(serializedSensorData.length())};
    encode_pointer_to_integer(serializedSensorData.data(), hi, lo);

    EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 0, VariableType::Int)).WillRepeatedly([&baseLoValue](auto value, auto, auto) {
        // baseLoValue = value;
    });
    EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 1, VariableType::Int)).WillRepeatedly([&baseHiValue](auto value, auto, auto) {
        // baseHiValue = value;
    });
    EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 2, VariableType::Int)).WillRepeatedly([&sizeValue](auto value, auto, auto) {
        // sizeValue = value;
    });

    ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));

    std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> targetOutputTracesMap = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();

    std::shared_ptr<ssp::OsmpConnector<osi3::SensorData>> sensorViewConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("SensorData", "SensorData", fakeFmuWrapper, 10);
    sensorViewConnector->SetWriteBinaryTrace(".", targetOutputTracesMap, "SensorData");

    auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
    ssp::UpdateInputSignalVisitor inVisitor{2, sensorDataSignal, 0};
    inVisitor.visit(sensorViewConnector.get());

    ON_CALL(*fakeFmuWrapper, UpdateOutput).WillByDefault([sensorViewConnector](int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) mutable {
        const std::shared_ptr<const osi3::SensorData> sensorData = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnector->getMessage());
        data = std::make_shared<const SensorDataSignal>(*sensorData);
    });
    Mock::AllowLeak(fakeFmuWrapper.get());

    std::shared_ptr<SignalInterface const> outSignal{};
    ssp::UpdateOutputSignalVisitor outVisitor{6, outSignal, 0};
    outVisitor.visit(sensorViewConnector.get());
    auto sensorDataOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data)->sensorData;
}