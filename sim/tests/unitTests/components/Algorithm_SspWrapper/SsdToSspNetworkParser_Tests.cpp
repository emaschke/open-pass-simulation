/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "fakeStochastics.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeWorld.h"

#include "SsdToSspNetworkParser.h"
#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using ::testing::DontCare;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::Return;

class SSPNetworkTests : public ::testing::Test
{
public:
    SSPNetworkTests()
    {
        init();
    }
    void init()
    {
        OpenPassInterfaceData::setValues("SspWrapper",
                                          false,
                                          0,
                                          0,
                                          0,
                                          100,
                                          nullptr,
                                          &fakeWorld,
                                          &fakeParameter,
                                          nullptr,
                                          &fakeAgent,
                                          &fakeCallback);
    }

protected:
    NiceMock<FakeWorld> fakeWorld;
    NiceMock<FakeAgent> fakeAgent;
    NiceMock<FakeParameter> fakeParameter;
    NiceMock<FakeCallback> fakeCallback;
};

/*
 * Ssp Implementation does not handle ScalarConnectors yet, the following two tests are disabled for now
 *
 */

/*TEST(SsdToSspNetworkParser_Test, TwoComponentsCorrectInput)
{
    //    FakeAgent fakeAgent{};
    //    FakeCallback fakeCallback{};
    //    FakeParameter fakeParameter{};
    //    ::testing::Mock::AllowLeak(&fakeAgent);
    //    ::testing::Mock::AllowLeak(&fakeParameter);
    SsdToSspNetworkParser ssdToSspNetWorkParser;

    const auto path = "SystemStructure.ssd";
    auto ssdSystem = std::make_shared<SsdSystem>("0");
    auto componentFirst = std::make_shared<SsdComponent>("component1", path, SspComponentType::x_ssp_definition);
    componentFirst->setPriority(1);
    componentFirst->setWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
    componentFirst->emplaceConnector("output", SspNetwork::OSMPTripleLink {"connector1", "output_osiType"});
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);
    componentFirst->SetParameters(parameters);

    auto componentSecond = std::make_shared<SsdComponent>("component2", path, SspComponentType::x_ssp_definition);
    componentSecond->setPriority(0);
    componentSecond->emplaceConnector("input", SspNetwork::OSMPTripleLink {"connector2", "input_osiType"});
    componentSecond->addUpdateOutputParameter(0);
    componentSecond->SetParameters(parameters);

    ssdSystem->AddComponent(std::move(componentFirst));
    ssdSystem->AddComponent(std::move(componentSecond));

    std::map<std::string, std::string> connection;
    connection.insert(std::make_pair("startElement", "component1"));
    connection.insert(std::make_pair("endElement", "component2"));
    connection.insert(std::make_pair("startConnector", "connector1"));
    connection.insert(std::make_pair("endConnector", "connector2"));

    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_NO_THROW(ssdToSspNetWorkParser.getSspNetwork(ssdFiles));

    ASSERT_THAT(ssdToSspNetWorkParser.getPriorities().size(), Eq(3));
    ASSERT_THAT(ssdToSspNetWorkParser.getPriorities().at("component1"), Eq(1));
    ASSERT_THAT(ssdToSspNetWorkParser.getPriorities().at("component2"), Eq(0));
    ASSERT_THAT(ssdToSspNetWorkParser.getComponentToWrapperMap().size(), Eq(0));
    ASSERT_THAT(ssdToSspNetWorkParser.getConnections().size(), Eq(1));
//    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].first).first, Eq("component1"));
//    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].first).second, Eq("connector1"));
//    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].second).first, Eq("component2"));
//    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].second).second, Eq("connector2"));
    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].first).first, Eq("connector1"));
    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].first).second, Eq("output_osiType"));
    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].second).first, Eq("connector2"));
    ASSERT_THAT(std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnections()[0].second).second, Eq("input_osiType"));
    ASSERT_THAT(ssdToSspNetWorkParser.getConnectorToMessageTypeList().size(), Eq(1));
    SspNetwork::OSMPTripleLink tripleLink = std::get<SspNetwork::OSMPTripleLink>(ssdToSspNetWorkParser.getConnectorToMessageTypeList()[0].first);
    std::string message = tripleLink.second;
    ASSERT_THAT(ssdToSspNetWorkParser.getConnectorToMessageTypeList()[0].second, Eq("output_osiType"));
    ASSERT_THAT(message, Eq("file_type"));
    ASSERT_THAT(ssdToSspNetWorkParser.getUpdateOutputParameters().size(), Eq(2));
    ASSERT_THAT(ssdToSspNetWorkParser.getUpdateOutputParameters().at("component1").size(), Eq(0));
    ASSERT_THAT(ssdToSspNetWorkParser.getUpdateOutputParameters().at("component2").size(), Eq(1));
    ASSERT_THAT(ssdToSspNetWorkParser.getUpdateOutputParameters().at("component2")[0], Eq(0));
}

TEST(SsdToSspNetworkParser_Test, OneComponentCorrectInput)
{
    SsdToSspNetworkParser ssdToSspNetWorkParser;

    auto ssdSystem = std::make_shared<SsdSystem>("0");
    auto componentFirst = std::make_shared<SsdComponent>("component1", "SystemStructure.ssd", SspComponentType::x_ssp_definition);
    componentFirst->setPriority(1);
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);
    componentFirst->SetParameters(parameters);
    ssdSystem->AddComponent(std::move(componentFirst));
    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("SystemStructure.ssd", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_NO_THROW(ssdToSspNetWorkParser.getSspNetwork(ssdFiles));

    ASSERT_THAT(ssdToSspNetWorkParser.getPriorities().size(), Eq(2));
    ASSERT_THAT(ssdToSspNetWorkParser.getPriorities().at("component1"), Eq(1));
    ASSERT_THAT(ssdToSspNetWorkParser.getComponentToWrapperMap().size(), Eq(0));
    ASSERT_THAT(ssdToSspNetWorkParser.getConnections().size(), Eq(0));
    ASSERT_THAT(ssdToSspNetWorkParser.getConnectorToMessageTypeList().size(), Eq(0));
    ASSERT_THAT(ssdToSspNetWorkParser.getUpdateOutputParameters().size(), Eq(1));
    ASSERT_THAT(ssdToSspNetWorkParser.getUpdateOutputParameters().at("component1").size(), Eq(0));
}*/

TEST_F(SSPNetworkTests, TwoComponentsIncorrectConnector)
{
    SsdToSspNetworkParser ssdToSspNetWorkParser;

    auto ssdSystem = std::make_shared<SsdSystem>("0");
    auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_fmu_sharedlibrary);
    componentFirst->setPriority(1);
    componentFirst->setWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
    componentFirst->emplaceConnector("connector1", SspParserTypes::OSMPTripleLink {"output", "output_osiType"});
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);
    componentFirst->SetParameters(parameters);

    auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_fmu_sharedlibrary);
    componentSecond->setPriority(0);
    componentSecond->emplaceConnector("connector2", SspParserTypes::OSMPTripleLink {"input", "input_osiType"});
    componentSecond->addUpdateOutputParameter(0);
    componentSecond->SetParameters(parameters);

    ssdSystem->AddComponent(std::move(componentFirst));
    ssdSystem->AddComponent(std::move(componentSecond));

    std::map<std::string, std::string> connection;
    connection.insert(std::make_pair("startElement", "component1"));
    connection.insert(std::make_pair("endElement", "component2"));
    connection.insert(std::make_pair("startConnector", "connector1"));
    connection.insert(std::make_pair("wrongConnector", "connector2"));

    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_THROW(ssdToSspNetWorkParser.getRootSystem(ssdFiles), std::runtime_error);
}

TEST_F(SSPNetworkTests, TwoComponentsMissingConnectionParameter)
{
    SsdToSspNetworkParser ssdToSspNetWorkParser;

    auto ssdSystem = std::make_shared<SsdSystem>("0");
    auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_fmu_sharedlibrary);
    componentFirst->setPriority(1);
    componentFirst->setWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
    componentFirst->emplaceConnector("connector1", SspParserTypes::OSMPTripleLink {"output", "output_osiType"});
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);
    componentFirst->SetParameters(parameters);

    auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_fmu_sharedlibrary);
    componentSecond->setPriority(0);
    componentSecond->emplaceConnector("connector2", SspParserTypes::OSMPTripleLink {"input", "input_osiType"});
    componentSecond->addUpdateOutputParameter(0);
    componentSecond->SetParameters(parameters);

    ssdSystem->AddComponent(std::move(componentFirst));
    ssdSystem->AddComponent(std::move(componentSecond));

    std::map<std::string, std::string> connection;
    connection.insert(std::make_pair("startElement", "component1"));
    connection.insert(std::make_pair("endElement", "component2"));
    connection.insert(std::make_pair("startConnector", "connector1"));

    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_THROW(ssdToSspNetWorkParser.getRootSystem(ssdFiles), std::runtime_error);
}

TEST_F(SSPNetworkTests, TwoComponentsConnectionWithNonexistentConnector)
{
    SsdToSspNetworkParser ssdToSspNetWorkParser;

    auto ssdSystem = std::make_shared<SsdSystem>("0");
    auto componentFirst = std::make_shared<SsdComponent>("component1", "source1", SspComponentType::x_ssp_definition);
    componentFirst->setPriority(1);
    componentFirst->setWriteMessageParameters({std::make_pair("output_osiType", "file_type")});
    componentFirst->emplaceConnector("connector1", SspParserTypes::OSMPTripleLink{"output", "output_osiType"});
    openpass::parameter::internal::ParameterSetLevel3 parameters;
    parameters.emplace_back("Logging", false);
    parameters.emplace_back("CsvOutput", false);
    parameters.emplace_back("UnzipOncePerInstance", true);
    componentFirst->SetParameters(parameters);

    auto componentSecond = std::make_shared<SsdComponent>("component2", "source2", SspComponentType::x_ssp_definition);
    componentSecond->setPriority(0);
    componentSecond->emplaceConnector("connector2", SspParserTypes::OSMPTripleLink{"input", "input_osiType"});
    componentSecond->addUpdateOutputParameter(0);
    componentSecond->SetParameters(parameters);

    ssdSystem->AddComponent(std::move(componentFirst));
    ssdSystem->AddComponent(std::move(componentSecond));

    std::map<std::string, std::string> connection;
    connection.insert(std::make_pair("startElement", "component1"));
    connection.insert(std::make_pair("endElement", "component2"));
    connection.insert(std::make_pair("startConnector", "newConnector"));
    connection.insert(std::make_pair("endConnector", "connector2"));

    ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(connection));
    std::vector<std::shared_ptr<SsdFile>> ssdFiles{std::make_shared<SsdFile>("", ssdSystem)};

    openpass::common::RuntimeInformation runtimeInformation{{openpass::common::framework}, {"", "outputDirectory", ""}};

    ON_CALL(fakeParameter, GetRuntimeInformation()).WillByDefault(testing::ReturnRef(runtimeInformation));
    ON_CALL(fakeAgent, GetId()).WillByDefault(Return(0));

    EXPECT_THROW(ssdToSspNetWorkParser.getRootSystem(ssdFiles), std::runtime_error);
}