/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "fmuHandlerInterface.h"
#include "signalInterface.h"

/// @brief Interface of FmuWrapper
class FmuWrapperInterface
{
public:
    virtual ~FmuWrapperInterface() = default;

    /// @brief Initialize FMU wrapper interface
    virtual void Init() = 0;

    /*!
     * \brief Update Inputs
     *
     * Function is called by framework when another component delivers a signal over
     * a channel to this component (scheduler calls update taks of other component).
     *
     * Refer to module description for input channels and input ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
     * \param[in]     data           Referenced signal (copied by sending component)
     * \param[in]     time           Current scheduling time
     */
    virtual void UpdateInput(int localLinkId, const std::shared_ptr<const SignalInterface> &data, int time) = 0;
    
    /*!
     * \brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component.
     *
     * Refer to module description for information about the module's task.
     *
     * \param[in]     time           Current scheduling time
     */
    virtual void Trigger(int time) = 0;

    /*!
     * \brief Update outputs.
     *
     * Function is called by framework when this Component.has to deliver a signal over
     * a channel to another component (scheduler calls update task of this component).
     *
     * Refer to module description for output channels and output ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
     * \param[out]    data           Referenced signal (copied by this component)
     * \param[in]     time           Current scheduling time
     */
    virtual void UpdateOutput(int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) = 0;
    
    /// @return Returns reference to the FmuHandlerInterface
    [[nodiscard]] virtual const FmuHandlerInterface *GetFmuHandler() const = 0;

    /// @return Returns reference to the FmuVariables
    [[nodiscard]] virtual const FmuVariables &GetFmuVariables() const = 0;

    /// @brief Get FmuValue
    /// @param valueReference 
    /// @param variableType     Type of variable
    /// @return Returns FMU value
    [[nodiscard]] virtual const FmuValue& GetValue(int valueReference, VariableType variableType) const = 0;
    
    /// @brief Set FmuValue
    /// @param valueReference 
    /// @param variableType     Type of variable
    /// @param fmuValue         FMU value
    virtual void SetValue(const FmuValue& fmuValue, int valueReference, VariableType variableType) = 0;
    
    /// @return Returns the priority of the FMU
    virtual int getPriority() const = 0;
    virtual fmi_version_enu_t getFmiVersion() = 0;

    virtual void SetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue> fmuValuesIn, VariableType dataType) = 0;
    virtual void GetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue>& fmuValuesOut, VariableType dataType) = 0;
};

/// @brief Class representing an exception for non initialized variables
class not_initialized_exception : public std::exception
{
public:

    /// @brief Create a not initialized exception message
    /// @param componentName Name of the component
    not_initialized_exception(const std::string& componentName) : componentName(componentName),
        message(std::string("FMU Excpetion: not initialized + "  + componentName))
    {
    }

    /// @brief Function to return the what type of non initialized exception message
    /// @return Non initialized exception message
    const char * what () const throw ()
    {
        return message.c_str();
    }

private:
    const std::string& componentName;
    const std::string message;
};
