/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentTypeInterface.h
//! @brief This file contains the internal representation of an agent type as
//!        given by the configuration.
//-----------------------------------------------------------------------------

#pragma once

#include <vector>
#include <map>
#include <memory>
#include <string>

namespace core
{

class ComponentType;
class ChannelType;

/// @brief Internal representation of an agent type as given by the configuration
class AgentTypeInterface
{
public:
    AgentTypeInterface() = default;

    /// @brief Create an agent type interface
    /// @param id       Id of the agent type
    /// @param priority Priority of the agent
    AgentTypeInterface(int id, int priority);

    /// @brief default constructor with agent type interface object
    AgentTypeInterface(const AgentTypeInterface&) = default;

    /// @brief default constructor with agent type interface object
    AgentTypeInterface(AgentTypeInterface&&) = default;

    /// @brief operator= overload
    /// @return agent type interface object
    AgentTypeInterface& operator=(const AgentTypeInterface&) = default;

    /// @brief operator= overload
    /// @return agent type interface object
    AgentTypeInterface& operator=(AgentTypeInterface&&) = default;
    virtual ~AgentTypeInterface() = default;

    /// @brief Add a new channel to the agent type interface
    /// @param id Id of the channel to be added
    /// @return true when the channel is successfull added
    virtual bool AddChannel(int id) = 0;

    /// @brief Add a new component to the agent type interface
    /// @param component Reference to the type of component
    /// @return true when the component is successfull added
    virtual bool AddComponent(std::shared_ptr<ComponentType> component) = 0;

    /// @return Returns the list of channels
    virtual const std::vector<int> &GetChannels() const = 0;

    /// @return Returns a map of component name and the reference to corresponding component
    virtual const std::map<std::string, std::shared_ptr<ComponentType>> &GetComponents() const = 0;
};

} // namespace core
