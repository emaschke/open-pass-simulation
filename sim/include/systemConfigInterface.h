/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include <map>
#include <string>
#include "include/agentTypeInterface.h"
#include "include/parameterInterface.h"

/// @brief Interface representing a system configuration
class SystemConfigInterface
{
public:
    SystemConfigInterface() = default;
    virtual ~SystemConfigInterface() = default;

    /// @brief Function to return systems
    /// @return A map of system id and corresponding agent type interfaces
    virtual const std::map<int, std::shared_ptr< core::AgentTypeInterface>>& GetSystems() const = 0;
    
    /// @brief Set systems
    /// @param systems A map of system id and corresponding agent type interfaces
    virtual void SetSystems(std::map<int, std::shared_ptr< core::AgentTypeInterface>> systems) = 0;
    
    /// @brief Add a new system configuration
    /// @param systemId Id of the system
    /// @param system   Reference to the agent type interface
    /// @return True when a system is successfully added
    virtual bool AddSystem(const int& systemId, const std::shared_ptr<core::AgentTypeInterface> system) = 0;
    
    /// @brief Add parameters of the model
    /// @param modelParameters Reference to the parameter interface
    virtual void AddModelParameters(std::shared_ptr<ParameterInterface> modelParameters) = 0;
};

