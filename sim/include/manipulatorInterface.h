/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <vector>

//-----------------------------------------------------------------------------
//! Class representing a manipulator interface
//-----------------------------------------------------------------------------
class ManipulatorInterface
{
public:
    ManipulatorInterface() = default;
    ManipulatorInterface(const ManipulatorInterface&) = delete;
    ManipulatorInterface(ManipulatorInterface&&) = delete;
    ManipulatorInterface& operator=(const ManipulatorInterface&) = delete;
    ManipulatorInterface& operator=(ManipulatorInterface&&) = delete;
    virtual ~ManipulatorInterface() = default;

    /*!
     * \brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component.
     *
     * Refer to module description for information about the module's task.
     *
     * \param[in]     time           Current scheduling time
     */
    virtual void Trigger(int time) = 0;

    /*!
     * Retrieves cycle time of the trigger task of this component
     *
     * @return  Trigger task cycle time
    */
    virtual int GetCycleTime() const = 0;
};


