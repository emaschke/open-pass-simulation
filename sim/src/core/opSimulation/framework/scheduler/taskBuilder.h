/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  taskBuilder.h
*	\brief This file contains the logic of unchangeable tasks
*	\details bootstrap, spawning and finalize tasks have their fix order
*           The taskBuilder generates task lists and fill them with necessary tasks
*/
//-----------------------------------------------------------------------------

#include <functional>
#include <list>
#include <vector>

#include "include/spawnPointNetworkInterface.h"
#include "include/worldInterface.h"
#include "include/dataBufferInterface.h"
#include "include/eventDetectorNetworkInterface.h"
#include "include/manipulatorNetworkInterface.h"
#include "include/observationNetworkInterface.h"
#include "runResult.h"
#include "tasks.h"

namespace core::scheduling {

/** 
 * @brief interface of taskBuilder
 *
 * @ingroup opSimulation
 */
class TaskBuilderInterface
{
public:
    /**
     * @brief CreateBootstrapTasks
     *
     * @details Creates tasks with all tasks of bootstrap phase.
     *          Bootstrap tasks can not be changed.
     *
     * @return	final tasks of bootstrap phase
     */
    virtual std::vector<TaskItem> CreateBootstrapTasks() = 0;

    /**
     * @brief CreateSpawningTasks
     *
     * @details Creates tasks with all tasks of spawning phase.
     *          Spawning tasks can not be changed.
     *
     * @return	final tasks of spawning phase
     */
    virtual std::vector<TaskItem> CreateSpawningTasks() = 0;

    /**
     * @brief CreateSynchronizeTasks
     *
     * @details Creates tasks with all tasks of synchronize phase.
     *
     * @return	final tasks of synchronize phase
     */
    virtual std::vector<TaskItem> CreateSynchronizeTasks() = 0;

    /**
     * @brief CreateFinalizeTasks
     *
     * @details Creates tasks with all tasks of finalize phase.
     *          Finalize tasks can not be changed.
     *
     * @return	final tasks of finalize phase
     */
    virtual std::vector<TaskItem> CreateFinalizeTasks() = 0;

    /**
     * @brief CreatePreAgentTasks
     *
     * @details Creates tasks with all tasks of pre-agent phase.
     *
     * @return	final tasks of pre-agent phase
     */
    virtual std::vector<TaskItem> CreatePreAgentTasks() = 0;
};

/**
 * @brief handles fix tasks of scheduler
 *
 * @ingroup opSimulation
 */
class TaskBuilder : public TaskBuilderInterface
{
private:
    const int &currentTime;
    core::RunResult &runResult;
    const int frameworkUpdateRate;
    const int ScheduleAtEachCycle = 0;

    WorldInterface *const world;
    core::SpawnPointNetworkInterface *const spawnPointNetwork;
    core::ObservationNetworkInterface *const observationNetwork;
    core::EventDetectorNetworkInterface *const eventDetectorNetwork;
    core::ManipulatorNetworkInterface *const manipulatorNetwork;
    DataBufferInterface *const dataInterface;

    std::vector<TaskItem> eventDetectorTasks;
    std::vector<TaskItem> manipulatorTasks;

    /**
     * @brief BuildEventDetectorTasks
     *
     * @details In constructor all eventDetectors are read out and
     *          stored in eventDetector Tasks to fill them later in
     *          tasks.
     */
    void BuildEventDetectorTasks();

    /**
     * @brief BuildManipulatorTasks
     *
     * @details In constructor all manipulators are read out and
     *          stored in eventDetector Tasks to fill them later in
     *          tasks.
     */
    void BuildManipulatorTasks();

public:
    /**
     * @brief TaskBuilder constructor
     * 
     * @param[in] currentTime           Current time
     * @param[in] runResult             Result of a simulation run
     * @param[in] frameworkUpdateRate   Framework update rate
     * @param[in] world                 The world interface
     * @param[in] spawnPointNetwork     Pointer to spawn point network
     * @param[in] observationNetwork    Pointer to observation network
     * @param[in] eventDetectorNetwork  Pointer to event detector network
     * @param[in] manipulatorNetwork    Pointer to manipulator network
     * @param[in] dataInterface         Pointer to the data buffer
     */
    TaskBuilder(const int &currentTime,
                core::RunResult &runResult,
                const int frameworkUpdateRate,
                WorldInterface *const world,
                core::SpawnPointNetworkInterface *const spawnPointNetwork,
                core::ObservationNetworkInterface *const observationNetwork,
                core::EventDetectorNetworkInterface *const eventDetectorNetwork,
                core::ManipulatorNetworkInterface *const manipulatorNetwork,
                DataBufferInterface *const dataInterface);

    virtual ~TaskBuilder() = default;

    std::vector<TaskItem> CreateBootstrapTasks() override;
    std::vector<TaskItem> CreateSpawningTasks() override;
    std::vector<TaskItem> CreatePreAgentTasks() override;
    std::vector<TaskItem> CreateSynchronizeTasks() override;
    std::vector<TaskItem> CreateFinalizeTasks() override;
};

} // namespace openpass::scheduling
