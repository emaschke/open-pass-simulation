/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agent.h
//! @brief This file contains the internal representation of an agent instance
//!        during a simulation run.
//-----------------------------------------------------------------------------

#pragma once

#include <functional>
#include <algorithm>
#include <utility>
#include <map>
#include <list>

#include "include/stochasticsInterface.h"
#include "include/agentInterface.h"
#include "include/componentInterface.h"

class DataBufferWriteInterface;
class AgentBlueprintInterface;

namespace core
{

class Channel;
class ModelBinding;
class ModelParameters;
class SpawnPoint;
class ObservationNetworkInterface;
class SpawnItemParameter;

//! This class represents an agent instance during a simulation run
class Agent
{
public:
    //! Agent constructor
    //!
    //! @param[in] world            The world interface   
    //! @param[in] agentBlueprint   Blueprint holding parameters of the agent
    Agent(WorldInterface *world, const AgentBlueprintInterface& agentBlueprint);
    Agent(const Agent&) = delete;
    Agent(Agent&&) = delete;
    Agent& operator=(const Agent&) = delete;
    Agent& operator=(Agent&&) = delete;
    virtual ~Agent();

    /// @brief Add channel to the agent
    /// @param id       Id of the channel
    /// @param channel  Pointer to the channel
    /// @return True, if successfull
    bool AddChannel(int id, Channel *channel);

    /// @brief Add component to the agent
    /// @param name             Name of the component
    /// @param component        Pointer to the component interface
    /// @return True, if successfull    
    bool AddComponent(std::string name, core::ComponentInterface *component);

    /// @brief Getter function to return channel information
    /// @param id Id of the channel
    /// @return Returns channel
    Channel *GetChannel(int id) const;

    /// @brief Getter function to return component information
    /// @param name Name of the component
    /// @return Returns pointer to the component interface
    ComponentInterface *GetComponent(std::string name) const;

    /// @brief Getter function to return all components
    /// @return Retuns the name of the component with corresponding pointer to these components
    const std::map<std::string, ComponentInterface*> &GetComponents() const;

    //! Returns the agent Id
    //!
    //! @return Id of the agent
    int GetId() const
    {
        return id;
    }

    /// @brief Instantiate the agent
    /// @param agentBlueprint       Reference to the blueprint of the agent
    /// @param modelBinding         Pointer to the model binding
    /// @param stochastics          Pointer to the stochastics interface
    /// @param observationNetwork   Pointer to the observation network interface
    /// @param eventNetwork         Pointer to the event network interface
    /// @param dataBuffer           Pointer to the data buffer writer interface
    /// @return True, if the agent is successfully instantiated
    bool Instantiate(const AgentBlueprintInterface& agentBlueprint,
                     ModelBinding *modelBinding,
                     StochasticsInterface *stochastics,
                     core::ObservationNetworkInterface *observationNetwork,
                     EventNetworkInterface *eventNetwork,
                     DataBufferWriteInterface* dataBuffer);

    /// @return Getter function to return the pointer to the agent interface
    AgentInterface* GetAgentAdapter() const;

    /// @brief Link the scheduler time to the agent
    /// @param schedulerTime Scheduler time
    void LinkSchedulerTime(int* const schedulerTime);

private:
    AgentInterface& agentInterface;
    int id;
    WorldInterface *world = nullptr;

    std::vector<int> idsCollisionPartners;
    std::map<int, Channel*> channels;
    std::map<std::string, ComponentInterface*> components;

    std::unique_ptr<PublisherInterface> publisher;

    int* currentTime = nullptr;
};

} // namespace core


