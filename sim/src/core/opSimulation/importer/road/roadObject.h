/********************************************************************************
 * Copyright (c) 2017-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadObjectInterface.h"

/// Class representing a road object
class RoadObject : public RoadObjectInterface
{
public:
    //! RoadObject constructor
    //!
    //! @param[in] road     The RoadInterface input data
    //! @param[in] object   OpenDrive road objects element
    RoadObject(RoadInterface* road, const RoadObjectSpecification object) :
        road{road},
        object{object}
    {}

    std::string GetId() const override;
    double GetS() const override;
    double GetT() const override;
    double GetZOffset() const override;
    double GetHdg() const override;
    double GetHeight() const override;
    double GetPitch() const override;
    double GetRoll() const override;
    bool IsValidForLane(int laneId) const override;
    RoadObjectType GetType() const override;
    double GetLength() const override;
    double GetWidth() const override;
    bool IsContinuous() const override;
    std::string GetName() const override;

private:
    RoadInterface* road;
    const RoadObjectSpecification object;

public:
    RoadObject(const RoadObject&) = delete;
    RoadObject(RoadObject&&) = delete;
    RoadObject& operator=(const RoadObject&) = delete;
    RoadObject& operator=(RoadObject&&) = delete;
    virtual ~RoadObject() = default;
};
