/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/opExport.h"
#include "include/profilesInterface.h"

/// Class representing profiles
class SIMULATIONCOREEXPORT Profiles : public ProfilesInterface
{
public:
    ~Profiles() override = default;

    const std::unordered_map<std::string, AgentProfile>& GetAgentProfiles() const override;

    bool AddAgentProfile(std::string agentProfileName, AgentProfile agentProfile) override;

    const std::unordered_map<std::string, VehicleProfile>& GetVehicleProfiles() const override;

    void AddVehicleProfile(const std::string& profileName, const VehicleProfile& vehicleProfile) override;

    const ProfileGroups& GetProfileGroups() const override;

    bool AddProfileGroup(std::string profileType, std::string profileName, openpass::parameter::ParameterSetLevel1 parameters) override;

    const StringProbabilities& GetDriverProbabilities(std::string agentProfileName) const override;

    const StringProbabilities& GetVehicleProfileProbabilities(std::string agentProfileName) const override;

    const openpass::parameter::ParameterSetLevel1& GetProfile(const std::string& type, const std::string& name) const override;

    openpass::parameter::ParameterSetLevel1 CloneProfile(const std::string& type, const std::string& name) const override;

private:
    std::unordered_map<std::string, AgentProfile> agentProfiles {};
    std::unordered_map<std::string, VehicleProfile> vehicleProfiles {};
    ProfileGroups profileGroups;
};
