/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <QString>

/// Struct represents output attributes
struct OutputAttributes
{
    const QString FRAMEWORKVERSION = "FrameworkVersion";                    ///< Output framework version
    const QString SCHEMAVERSION = "SchemaVersion";                          ///< Output schema version
    const QString RUNID = "RunId";                                          ///< Id of the current run
    const QString FAILUREPROBABITLITY = "FailureProbability";               ///< Probability object is not detected although it is visible
    const QString LATENCY = "Latency";                                      ///< Sensor latency
    const QString OPENINGANGLEH = "OpeningAngleH";                          ///< Horizontal opening angle of the sensor
    const QString OPENINGANGLEV = "OpeningAngleV";                          ///< Vertical opening angle of the sensor
    const QString MOUNTINGPOSITIONLONGITUDINAL = "MountingPosLongitudinal"; ///< Relative longitudinal position of the sensor in relation to the agent
    const QString MOUNTINGPOSITIONLATERAL = "MountingPosLateral";           ///< Relative lateral position of the sensor in relation to the agent
    const QString MOUNTINGPOSITIONHEIGHT = "MountingPosHeight";             ///< Relative height of the sensor in relation to the agent
    const QString ORIENTATIONYAW = "OrientationYaw";                        ///< Yaw rotation of the sensor in relation to the agent
    const QString ORIENTATIONPITCH = "OrientationPitch";                    ///< Pitch rotation of the sensor in relation to the agent
    const QString ORIENTATIONROLL = "OrientationRoll";                      ///< Roll rotation of the sensor in relation to the agent
    const QString DETECTIONRANGE = "DetectionRange";                        ///< Range how far the sensor reaches in m
    const QString VEHICLEMODELTYPE = "VehicleModelType";                    ///< Name of the vehicle model
    const QString DRIVERPROFILENAME = "DriverProfileName";                  ///< Name of the driver profile
    const QString AGENTTYPEGROUPNAME = "AgentTypeGroupName";                ///< The agent category. This can either be Ego, Scenario or Common
    const QString AGENTTYPENAME = "AgentTypeName";                          ///< Name of the agent profile
    const QString AGENTTYPE = "AgentType";                                  ///< Type of the agent
    const QString TIME = "Time";                                            ///< Time in milliseconds
    const QString TYPE = "Type";                                            ///< Type of the component
    const QString NAME = "Name";                                            ///< Name of the component
    const QString KEY = "Key";                                              ///< Name of parameter as string
    const QString VALUE = "Value";                                          ///< Value of parameter as string
    const QString ID = "Id";                                                ///< Identification number
    const QString SOURCE = "Source";                                        ///< Name of the component which created the event
    const QString WIDTH = "Width";                                          ///< Width of the vehicles bounding box
    const QString LENGTH = "Length";                                        ///< Length of the vehicles bounding box
    const QString HEIGHT = "Height";                                        ///< Height of the vehicles bounding box
    const QString LONGITUDINALPIVOTOFFSET = "LongitudinalPivotOffset";      ///< Distance between center of the bounding box and reference point of the agent
};

/// Struct represents output tags
struct OutputTags
{
    const QString RUNRESULTS = "RunResults";                    ///< This section contains all RunResults that occurred during the invocations
    const QString RUNRESULT = "RunResult";                      ///< This section contains information about the agents, their parameters, events related to the entities
    const QString RUNSTATISTICS = "RunStatistics";              ///< This section contains the RandomSeed and general statistics of the invocation
    const QString SIMULATIONOUTPUT = "SimulationOutput";        ///< This section contains central information about all executed invocations within an experiment
    const QString EVENTS = "Events";                            ///< This section contains all events that occurred during the invocation
    const QString EVENT = "Event";                              ///< This section contains event related to the entity
    const QString EVENTPARAMETER = "EventParameter";            ///< This section contains generic key/value string pairs 
    const QString AGENTS = "Agents";                            ///< This section contains some information on how each agent is configured
    const QString AGENT = "Agent";                              ///< This section contains some information on how the agent is configured
    const QString SENSORS = "Sensors";                          ///< This section contains all sensors of the agent and their parameters
    const QString SENSOR = "Sensor";                            ///< This section contains sensor of the agent and its parameters
    const QString CYCLICS = "Cyclics";                          ///< This section contains all logged parameters of the agents per time step
    const QString CYCLICSFILE = "CyclicsFile";                  ///< Reference to the corresponding CSV file
    const QString HEADER = "Header";                            ///< Layout of all samples
    const QString SAMPLES = "Samples";                          ///< This section contains information for all timesteps for which samples exist
    const QString SAMPLE = "Sample";                            ///< This section contains information for one specific time step
    const QString SCENERYFILE = "SceneryFile";                  ///< Reference to the used OpenDRIVE scenery file
    const QString VEHICLEATTRIBUTES = "VehicleAttributes";      ///< Basic information of the vehicle parameters
    const QString TRIGGERINGENTITIES = "TriggeringEntities";    ///< Entity IDs triggering this event
    const QString AFFECTEDENTITIES = "AffectedEntities";        ///< Entity IDs affected by this event
};

namespace output::tag {
static constexpr char ENTITY[] = "Entity";
static constexpr char PARAMETERS[] = "Parameters";
static constexpr char PARAMETER[] = "Parameter";
} // namespace output::tag

namespace output::attribute {
static constexpr char ID[] = "Id";
static constexpr char TIME[] = "Time";
static constexpr char TYPE[] = "Type";
static constexpr char NAME[] = "Name";
static constexpr char KEY[] = "Key";
static constexpr char VALUE[] = "Value";
} // namespace output::attribute

namespace output::event::tag {
static constexpr char EVENTS[] = "NewEvents";
static constexpr char EVENT[] = "Event";
static constexpr char TRIGGERING_ENTITIES[] = "TriggeringEntities";
static constexpr char AFFECTED_ENTITIES[] = "AffectedEntities";
} // namespace output::event::tag
