/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "ConditionCommonBase.h"
#include "include/parameterInterface.h"

//! This class checks if a triggering entity/entities has reached a given position, within some user specified tolerance
class ReachPositionCondition : public ConditionCommonBase
{
public:
    /**
     * @brief ReachPositionCondition constructor
     *
     * @param[in] parameters    Parameter interface
     */
    ReachPositionCondition(const ParameterInterface &parameters);
    virtual ~ReachPositionCondition() = default;

    /**
     * @brief Function to check if the condition is met
     * 
     * @param agent Agent interface pointer
     * @return true if the condition is met
     */
    bool IsMet(const AgentInterface *agent) const;

    ReachPositionCondition() = delete;
    ReachPositionCondition(const ReachPositionCondition&) = default;        ///< Default copy constructor
    ReachPositionCondition(ReachPositionCondition&&) = delete;
    ReachPositionCondition& operator=(const ReachPositionCondition&) = delete;
    ReachPositionCondition& operator=(ReachPositionCondition&&) = delete;

private:
    double tolerance;
    double targetSCoordinate;
    std::string targetRoadId;
};
