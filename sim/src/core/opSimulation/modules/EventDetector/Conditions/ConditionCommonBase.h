/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include "include/agentInterface.h"

enum class ConditionCategory
{
    Undefined = 0,
    ByEntity,
    ByValue
};

enum class Rule
{
    LessThan = 0,
    EqualTo,
    GreaterThan
};

/// Class for condition common base
class ConditionCommonBase
{
public:
    //! ConditionCommonBase constructor
    //!
    //! @param[in] conditionCategory    Category of the condition
    ConditionCommonBase(const ConditionCategory conditionCategory):
        conditionCategory(conditionCategory){}

    ConditionCommonBase(const ConditionCommonBase&) = default;              ///< Copy constructor
    ConditionCommonBase(ConditionCommonBase&&) = delete;                    ///< Move constructor
    ConditionCommonBase& operator=(const ConditionCommonBase&) = delete;    ///< copy assignment operator
    ConditionCommonBase& operator=(ConditionCommonBase&&) = delete;         ///< move assignment operator

    virtual ~ConditionCommonBase() = default;

    //! Returns the category of the condition
    //!
    //! @return Category of the condition
    ConditionCategory GetConditionCategory()
    {
        return conditionCategory;
    }

private:
    const ConditionCategory conditionCategory;
};
