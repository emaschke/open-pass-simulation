/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "WorldData.h"
#include "include/worldObjectInterface.h"
#include "common/openPassTypes.h"

/// An adapter class to the world object
class WorldObjectAdapter : public virtual WorldObjectInterface
{
public:
    const polygon_t& GetBoundingBox2D() const override;

    /// @return Returns height of the bounding box
    double GetHeight() const;

    /// @return Id of the world object
    int GetId() const;

    /// @return Length of the world object
    double GetLength() const;

    /// @return X coordinate of the world object
    double GetPositionX() const;

    /// @return Y coordinate of the world object
    double GetPositionY() const;

    /// @return z coordinate of the world object
    double GetPositionZ() const;

    /// @return Lateral position of the world object
    double GetPositionLateral() const;

    /// @return width of the world object
    double GetWidth() const;

    /// @return Yaw angle of the world object
    double GetYaw() const;

    /// @return Roll angle of the world object
    double GetRoll() const;

    /// @return Returns the distacnce between a reference point to the leading edge
    double GetDistanceReferencePointToLeadingEdge() const;

    /// @return Returns world object
    const OWL::Interfaces::WorldObject& GetBaseTrafficObject() const;


protected:
    OWL::Interfaces::WorldObject& baseTrafficObject;    ///< A traffic object 

    double s{0.0};                                      ///< S-coordinate of the world object
    mutable bool boundingBoxNeedsUpdate{true};          ///< True, if the bounding box of the world object needs an update

private:
    const polygon_t CalculateBoundingBox() const;

    mutable polygon_t boundingBox;

public:
    /// object is not inteded to be copied or assigned
    /// @param baseTrafficObject A traffic object
    WorldObjectAdapter(OWL::Interfaces::WorldObject& baseTrafficObject);
    WorldObjectAdapter(const WorldObjectAdapter&) = delete;
    WorldObjectAdapter(WorldObjectAdapter&&) = delete;
    WorldObjectAdapter& operator=(const WorldObjectAdapter&) = delete;
    WorldObjectAdapter& operator=(WorldObjectAdapter&&) = delete;
    virtual ~WorldObjectAdapter() = default;
};

namespace WorldObjectCommon {

double GetFrontDeltaS(double length, double width, double hdg, double distanceReferencePointToLeadingEdge);
double GetRearDeltaS(double length, double width, double hdg, double distanceReferencePointToLeadingEdge);

} // namespace WorldObjectCommon

