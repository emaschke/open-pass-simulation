/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/vector2d.h"
#include "include/roadInterface/roadLaneInterface.h"

//! This struct represents the outer boundary points of a lane at one s coordinate
struct BorderPoint
{
    /// @brief Create a border point
    /// @param point Coordinates of the border point
    /// @param t     t coordinate of the border point
    /// @param lane  Pointer to the road lane interface
    explicit BorderPoint(Common::Vector2d point, double t, RoadLaneInterface* lane) noexcept :
        point{point},
        t{t},
        lane{lane} {}

    BorderPoint() = default;

    constexpr BorderPoint(const BorderPoint&) = default;    ///< Default copy constructor
    constexpr BorderPoint(BorderPoint&&) = default;         ///< Default move constructor

    /// @return Copy assignment operator
    BorderPoint& operator=(const BorderPoint&) = default;   

    /// @return Move assignment operator
    BorderPoint& operator=(BorderPoint&&) = default;

    Common::Vector2d point; ///< Coordinates of the border piont
    double t; ///< t coordinate on the road
    RoadLaneInterface *lane; ///< This is needed for later adding the point to this lane
};

//! This struct represents the boundary points of all lanes (including lane 0) at one s coordinate (i.e. one joint)
struct BorderPoints
{
    double s;                           ///< s coordinate
    std::vector<BorderPoint> points;    ///< Boundary points

    /**
     * @brief Returns the highest squared lateral error of this joint to the line(s) defined by two other joints
     * 
     * @param[in]   begin   The beginning of border points
     * @param[in]   end     End of border points
     * @return the highest squared lateral error
     */
    double GetSquaredError(const BorderPoints& begin, const BorderPoints& end) const;
};

//! This struct is the result of the geometry sampling of one road section
struct SampledGeometry
{
    std::vector<BorderPoints> borderPoints; //!< Boundary points of all lanes (including lane 0) at one joint
    double startHeading;                    //!< This is needed for calculating the curvature of the first joint
    double endHeading;                      //!< This is needed for calculating the curvature of the last joint

    //! Combine this SampledGeometry with another, that starts where this ends.
    //!
    //! \param other    another SampledGeometry starting where this ends
    void Combine(SampledGeometry& other);
};

//! This struct contains all information calculated by the JointBuilder for one joint and one lane
struct LaneJoint
{
    RoadLaneInterface* lane;            ///< Road lane interface
    Common::Vector2d left{};            ///< coordinates of left lane
    Common::Vector2d center{};          ///< coordinates of center lane
    Common::Vector2d right{};           ///< coordinates of right lane
    double heading{0.0};                ///< Heading of the lane joint
    double curvature{0.0};              ///< Curvature of the lane joint
    double t_left{0.0};                 ///< t coordinate of the left lane joint
    double t_right{0.0};                ///< t coordinate of the right lane joint
};

//! This struct contains all information calculated by the JointBuilder for one joint
struct Joint
{
    double s;                               //!< s coordinate
    std::map<int, LaneJoint> laneJoints;    //!< List of LaneJoint
};

using Joints = std::vector<Joint>;

//! This class gets the sampled lane boundary points as input and calculates all the other
//! information about the joints, that is needed for adding the joints to the WorldData.
//! The functions have to be called in the order they appear in this header.
class JointsBuilder
{
public:
    /**
     * @brief JointsBuilder constructor
     *
     * @param[in]   sampledGeometry Input of the JointBuilder
     */
    JointsBuilder(const SampledGeometry& sampledGeometry) :
        sampledGeometry(sampledGeometry)
    {}

    /**
     * @brief JointsBuilder constructor. For testing only
     *
     * @param[in]   sampledGeometry     Sampled lane boundary points (input of the JointBuilder)
     * @param[in]   joints              Information about the joints (output of the JointBuilder)
     */
    JointsBuilder(const SampledGeometry& sampledGeometry, const Joints& joints) :
        sampledGeometry(sampledGeometry),
        joints(joints)
    {}

    //! Calculates the left, center and right points of each lane
    //!
    //! @return JointsBuilder
    JointsBuilder& CalculatePoints();

    //! Calculates the headings by interpolating between the center points
    //!
    //! @return JointsBuilder
    JointsBuilder& CalculateHeadings();

    //! Calculates the curvatures from the heading changes
    //!
    //! @return JointsBuilder
    JointsBuilder& CalculateCurvatures();

    //! Returns the calculated joints
    //!
    //! @return Calculated joints
    Joints& GetJoints();

private:
    SampledGeometry sampledGeometry; //! input of the JointBuilder
    Joints joints;                   //! output of the JointBuilder
};
