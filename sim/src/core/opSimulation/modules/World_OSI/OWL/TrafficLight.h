/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef OPENPASS_TRAFFICLIGHT_H
#define OPENPASS_TRAFFICLIGHT_H

#include <utility>

#include "OWL/DataTypes.h"
#include "include/callbackInterface.h"

namespace OWL::Implementation {

/// Base class representing traffic light
class TrafficLightBase : public Interfaces::TrafficLight
{
public:
    /**
     * @brief TrafficLightBase constructor
     * 
     * @param[in] callbacks     Pointer to the callbacks
     * @param[in] id            Id of the traffic light
     */
    TrafficLightBase(const CallbackInterface *callbacks, std::string id);

    virtual ~TrafficLightBase() = default;

    std::string GetId() const override
    {
        return id;
    }

    double GetS() const override
    {
        return s;
    }

    void SetS(double sPos) override
    {
        s = sPos;
    }

    /**
     * @brief Provides callback to LOG() macro
     *
     * @param[in]   logLevel   Importance of log
     * @param[in]   file       Name of file where log is called
     * @param[in]   line       Line within file where log is called
     * @param[in]   message    Message to log
     */
    void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const;

    /**
     * @brief Sets the operating mode of the traffic light if color matches the color of the OSI traffic light
     * 
     * @param[in] osiTrafficLightObject OSI traffic light
     * @param[in] color                 The semantic color of the traffic light
     * @param[in] mode                  The icon of the traffic light
     */
    void SetModeIfColorIsMatchingOsi(osi3::TrafficLight *osiTrafficLightObject,
                                     osi3::TrafficLight::Classification::Color color,
                                     osi3::TrafficLight_Classification_Mode mode);

    /**
     * @brief Sets the base parameters of the traffic light. Converts the specification imported from OpenDrive to OSI.
     * 
     * @param[in] signal                    OpenDrive specification
     * @param[in] position                  Position in the world
     * @param[in] osiTrafficLightObject     OSI traffic light
     * @param[in] numberOfSignals           Number of signals      
     */
    void SetBaseOfOsiObject(const RoadSignalInterface *signal, const Position &position,
                            osi3::TrafficLight *osiTrafficLightObject, int numberOfSignals);

    /**
     * @brief Return the icon for OSI trafficlight signal
     * 
     * @param[in] signal    OpenDrive specification
     * @param[in] iconMap   Icon map
     * @return icon of the traffic light  
     */
    osi3::TrafficLight_Classification_Icon fetchIconsFromSignal(RoadSignalInterface *signal,
                                                                const std::map<std::string, std::map<std::string, osi3::TrafficLight_Classification_Icon>> &iconMap);

    /**
     * @brief Return the color for OSI trafficlight signal
     * 
     * @param[in] signal    OpenDrive specification
     * @param[in] colorMap  Color map
     * @return semantic color of the traffic light 
     */
    std::vector<osi3::TrafficLight::Classification::Color>
    fetchColorsFromSignal(RoadSignalInterface *signal, const std::map<std::string, std::map<std::string, std::vector<osi3::TrafficLight_Classification_Color>>> &colorMap);

    /**
     * @brief Returns the traffic light state (the operating mode of the traffic light)
     * 
     * @param[in] osiTrafficLightObject OSI traffic light
     * @return traffic light state
     */
    CommonTrafficLight::State GetStateOsi(const osi3::TrafficLight *osiTrafficLightObject) const;

    /// @brief Initialize traffic light
    virtual void initDefaultTrafficLight() = 0;

    /// @brief Initialize traffic light
    /// @param pTrafficLight OSI traffic light
    void initDefaultTrafficLight(osi3::TrafficLight *pTrafficLight);

protected:
    std::string id{""};                 ///< OpenDrive ID of the traffic light
    double s{0.0};                      ///< s coordinate

    const CallbackInterface *callbacks; ///< References the callback functions of the framework 
};

 //! ASAM TrafficLight 1.000.001 sub ALL (red, yellow, green) &
 //! ASAM TrafficLight 1.000.011 (red, yellow, green with Arrow Icons)
class ThreeSignalsTrafficLight : public TrafficLightBase
{
public:
    /**
     * @brief ThreeSignalsTrafficLight constructor
     * 
     * @param[in] opendrive_id      OpenDrive ID of the traffic light
     * @param[in] osiLightRed       Red light. Definition of semantic color for traffic light
     * @param[in] osiLightYellow    Orange-yellow light. Definition of semantic color for traffic light
     * @param[in] osiLightGreen     Green light. Definition of semantic color for traffic light
     * @param[in] callbacks         Pointer to the callbacks
     */
    ThreeSignalsTrafficLight(const std::string &opendrive_id, osi3::TrafficLight *osiLightRed,
                             osi3::TrafficLight *osiLightYellow,
                             osi3::TrafficLight *osiLightGreen,
                             const CallbackInterface *callbacks);

    virtual ~ThreeSignalsTrafficLight() = default;

    bool IsValidForLane(OWL::Id laneId) const override;

    bool SetSpecification(RoadSignalInterface *signal, const Position &position) override;

    Primitive::AbsPosition GetReferencePointPosition() const override;

    Primitive::Dimension GetDimension() const override;

    void SetValidForLane(const OWL::Interfaces::Lane& lane, const RoadSignalInterface& specification) override
    {
        osiLightRed->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
        auto logicalLaneAssignmentRed = osiLightRed->mutable_classification()->add_logical_lane_assignment();
        logicalLaneAssignmentRed->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
        logicalLaneAssignmentRed->set_s_position(specification.GetS());
        logicalLaneAssignmentRed->set_t_position(specification.GetT());
        logicalLaneAssignmentRed->set_angle_to_lane(CommonHelper::SetAngleToValidRange(specification.GetHOffset() + (specification.GetOrientation() ? 0 : M_PI)));
        
        osiLightYellow->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
        auto logicalLaneAssignmentYellow = osiLightYellow->mutable_classification()->add_logical_lane_assignment();
        logicalLaneAssignmentYellow->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
        logicalLaneAssignmentYellow->set_s_position(specification.GetS());
        logicalLaneAssignmentYellow->set_t_position(specification.GetT());
        logicalLaneAssignmentYellow->set_angle_to_lane(CommonHelper::SetAngleToValidRange(specification.GetHOffset() + (specification.GetOrientation() ? 0 : M_PI)));
        
        osiLightGreen->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
        auto logicalLaneAssignmentGreen = osiLightGreen->mutable_classification()->add_logical_lane_assignment();
        logicalLaneAssignmentGreen->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
        logicalLaneAssignmentGreen->set_s_position(specification.GetS());
        logicalLaneAssignmentGreen->set_t_position(specification.GetT());
        logicalLaneAssignmentGreen->set_angle_to_lane(CommonHelper::SetAngleToValidRange(specification.GetHOffset() + (specification.GetOrientation() ? 0 : M_PI)));
    }

    virtual void CopyToGroundTruth(osi3::GroundTruth &target) const;

    virtual void SetState(CommonTrafficLight::State newState);

    CommonTrafficLight::Entity GetSpecification(const double relativeDistance) const override;

    virtual CommonTrafficLight::State GetState() const;

    void initDefaultTrafficLight() override;

    // Is need that overloading and overwriting at the same time works
    using TrafficLightBase::initDefaultTrafficLight;

private:
    bool SetSpecificationOnOsiObject(RoadSignalInterface *signal, const Position &position,
                                     osi3::TrafficLight *osiTrafficLightObject);

    osi3::TrafficLight *osiLightRed;
    osi3::TrafficLight *osiLightYellow;
    osi3::TrafficLight *osiLightGreen;
};

//! ASAM TrafficLight 1.000.002 sub -
//! ASAM TrafficLight 1.000.007 sub - (red, green with bike & pedestrian icon)
//! ASAM TrafficLight 1.000.009 sub 10,20,30 (any two conbinations of red yellow or green)
//! ASAM TrafficLight 1.000.013 sub - (red, green with bike icon)
class TwoSignalsTrafficLight : public TrafficLightBase
{
public:

    /// @brief TwoSignalsTrafficLight constructor
    /// @param opendrive_id   OpenDrive ID of the traffic light  
    /// @param osiLightTop    OSI Traffic light at top
    /// @param osiLightBottom OSI Traffic light at bottom
    /// @param callbacks      Pointer to the callbacks
    TwoSignalsTrafficLight(const std::string &opendrive_id, osi3::TrafficLight *osiLightTop,
                           osi3::TrafficLight *osiLightBottom,
                           const CallbackInterface *callbacks);

    virtual ~TwoSignalsTrafficLight() = default;

    bool IsValidForLane(OWL::Id laneId) const override;

    bool SetSpecification(RoadSignalInterface *signal, const Position &position) override;

    Primitive::AbsPosition GetReferencePointPosition() const override;

    Primitive::Dimension GetDimension() const override;

    void SetValidForLane(const OWL::Interfaces::Lane& lane, const RoadSignalInterface& specification) override
    {
        osiLightTop->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
        auto logicalLaneAssignmentTop = osiLightTop->mutable_classification()->add_logical_lane_assignment();
        logicalLaneAssignmentTop->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
        logicalLaneAssignmentTop->set_s_position(specification.GetS());
        logicalLaneAssignmentTop->set_t_position(specification.GetT());
        logicalLaneAssignmentTop->set_angle_to_lane(CommonHelper::SetAngleToValidRange(specification.GetHOffset() + (specification.GetOrientation() ? 0 : M_PI)));
        
        osiLightBottom->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
        auto logicalLaneAssignmentBottom = osiLightBottom->mutable_classification()->add_logical_lane_assignment();
        logicalLaneAssignmentBottom->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
        logicalLaneAssignmentBottom->set_s_position(specification.GetS());
        logicalLaneAssignmentBottom->set_t_position(specification.GetT());
        logicalLaneAssignmentBottom->set_angle_to_lane(CommonHelper::SetAngleToValidRange(specification.GetHOffset() + (specification.GetOrientation() ? 0 : M_PI)));
    }

    virtual void CopyToGroundTruth(osi3::GroundTruth &target) const;

    virtual void SetState(CommonTrafficLight::State newState);

    CommonTrafficLight::Entity GetSpecification(const double relativeDistance) const override;

    virtual CommonTrafficLight::State GetState() const;

    void initDefaultTrafficLight() override;

    // Is need that overloading and overwriting at the same time works
    using TrafficLightBase::initDefaultTrafficLight;

private:
    bool SetSpecificationOnOsiObject(RoadSignalInterface *signal, const Position &position,
                                     osi3::TrafficLight *osiTrafficLightObject, bool isTop);

    void SetTopModeIfColorIsMatching(osi3::TrafficLight::Classification::Color color,
                                     osi3::TrafficLight_Classification_Mode mode);

    void SetBottomModeIfColorIsMatching(osi3::TrafficLight::Classification::Color color,
                                        osi3::TrafficLight_Classification_Mode mode);

    CommonTrafficLight::State GetTopState() const;

    CommonTrafficLight::State GetBottomState() const;

    std::string subtype;

    osi3::TrafficLight *osiLightTop{nullptr};
    osi3::TrafficLight *osiLightBottom{nullptr};
};

//! ASAM TrafficLight 1.000.002 sub 10,20 (red, green with pedestrian icon)
//! ASAM TrafficLight 1.000.007 sub 10,20,30 (red, green with bike & pedestrian icon)
//! ASAM TrafficLight 1.000.013 20,30,40 (red, green with bike icon)
//! ASAM TrafficLight 1.000.020 ALL
//! ASAM TrafficLight 1.000.008 ALL
//! ASAM TrafficLight 1.000.012 ALL
class OneSignalsTrafficLight : public TrafficLightBase
{
public:
    /**
     * @brief ThreeSignalsTrafficLight constructor
     * 
     * @param[in] opendrive_id      OpenDrive ID of the traffic light
     * @param[in] osiTrafficLight   Traffic light
     * @param[in] callbacks         Pointer to the callbacks
     */
    OneSignalsTrafficLight(const std::string &opendrive_id, osi3::TrafficLight *osiTrafficLight,
                           const CallbackInterface *callbacks);

    virtual ~OneSignalsTrafficLight() = default;

    bool IsValidForLane(OWL::Id laneId) const override;

    bool SetSpecification(RoadSignalInterface *signal, const Position &position) override;

    Primitive::AbsPosition GetReferencePointPosition() const override;

    Primitive::Dimension GetDimension() const override;

    void SetValidForLane(const OWL::Interfaces::Lane& lane, const RoadSignalInterface& specification) override
    {
        osiTrafficLight->mutable_classification()->add_assigned_lane_id()->set_value(lane.GetId());
        auto logicalLaneAssignment = osiTrafficLight->mutable_classification()->add_logical_lane_assignment();
        logicalLaneAssignment->mutable_assigned_lane_id()->set_value(lane.GetLogicalLaneId());
        logicalLaneAssignment->set_s_position(specification.GetS());
        logicalLaneAssignment->set_t_position(specification.GetT());
        logicalLaneAssignment->set_angle_to_lane(CommonHelper::SetAngleToValidRange(specification.GetHOffset() + (specification.GetOrientation() ? 0 : M_PI)));

    }

    virtual void CopyToGroundTruth(osi3::GroundTruth &target) const;

    virtual void SetState(CommonTrafficLight::State newState);

    CommonTrafficLight::Entity GetSpecification(const double relativeDistance) const override;

    virtual CommonTrafficLight::State GetState() const;

    void initDefaultTrafficLight() override;

    // Is need that overloading and overwriting at the same time works
    using TrafficLightBase::initDefaultTrafficLight;

private:
    bool SetSpecificationOnOsiObject(RoadSignalInterface *signal, const Position &position,
                                     osi3::TrafficLight *osiTrafficLightObject);

    void SetModeIfColorIsMatching(osi3::TrafficLight::Classification::Color color,
                                  osi3::TrafficLight_Classification_Mode mode);
    std::string subtype;

    osi3::TrafficLight *osiTrafficLight{nullptr};
};
} // namespace OWL::Implementation
#endif // OPENPASS_TRAFFICLIGHT_H
