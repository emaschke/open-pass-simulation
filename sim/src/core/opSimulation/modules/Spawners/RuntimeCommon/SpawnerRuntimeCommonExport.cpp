/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  SpawnerRuntimeCommonExport.cpp */
//-----------------------------------------------------------------------------

#include "SpawnerRuntimeCommonExport.h"

#include "SpawnerRuntimeCommon.h"

/// The version of the current module - has to be incremented manually
const std::string Version = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;

/**
 * @brief dll-function to obtain the version of the current module
 * 
 * @return  Version of the current module
 */
extern "C" SPAWNPOINT_SHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
    return Version;
}

/**
 * @brief dll-function to create an instance of the module
 * 
 * @param   dependencies    Pointer to the spawn point dependencies
 * @param   callbacks       Pointer to the callbacks
 * @return  A pointer to the created module instance
 */
extern "C" SPAWNPOINT_SHARED_EXPORT SpawnPointInterface* OpenPASS_CreateInstance(const SpawnPointDependencies* dependencies,
                                                                                 const CallbackInterface* callbacks)
{
    Callbacks = callbacks;

    try
    {
        return new SpawnerRuntimeCommon(dependencies, callbacks);
    }
    catch(const std::runtime_error &ex)
    {
        if(Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return nullptr;
    }
    catch(...)
    {
        if(Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return nullptr;
    }
}

/**
 * @brief dll-function to create the agents for the spawn points
 * 
 * @param[in]   implementation  Instance of the module
 * @param[in]   time            Current time
 * @return the created agents
 */
extern "C" SPAWNPOINT_SHARED_EXPORT void OpenPASS_Trigger(SpawnPointInterface* implementation, int time, SpawnPointInterface::Agents &agents)
{
    agents = implementation->Trigger(time);
}
