/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/parameter.h"

namespace openpass::sensors
{
/// sensor position information
struct Position
{
    /// name TODO
    std::string name {};
    /// longitudinal position of the sensor
    double longitudinal {};
    /// lateral position of the sensor
    double lateral {};
    /// height of the sensor
    double height {};
    /// pitch of the sensor
    double pitch {};
    /// yaw angle of the sensor
    double yaw {};
    /// roll angle of the sensor
    double roll {};
};

/// profile of sensor
struct Profile
{
    /// name of sensor profile
    std::string name {};
    /// type of sensor profile
    std::string type {};
    /// parameter set level 1
    openpass::parameter::ParameterSetLevel1 parameter {};
};

/// sensor parameters
struct Parameter
{
    /// id 
    int id {};
    /// position of the sensor
    Position position {};
    /// profile of the sensor
    Profile profile {};
};

/// typedef profiles as vector of profiles
using Profiles = std::vector<Profile>;

/// typedef parameters as vector of parameter
using Parameters = std::vector<Parameter>;

} // namespace openpass::sensors
