/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2019-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <optional>
#include <string>
#include <variant>
#include "common/openScenarioDefinitions.h"
#include "include/agentInterface.h"
#include "include/worldInterface.h"
#include "common/opExport.h"

namespace openScenario
{

namespace ConditionEquality
{
//! Maximum difference between two values to be consired equal for OpenSCENARIO rule equal_to
static constexpr double EPSILON = 1e-12;
}

enum class Rule
{
    LessThan = 0,
    EqualTo,
    GreaterThan
};


/// OpenScenario ByEntity Conditions
class OPENPASSCOMMONEXPORT ByEntityCondition
{
public:
    /**
     * @brief Construct a new By Entity Condition object
     * 
     * @param triggeringEntityNames Names of triggering entities
     */
    ByEntityCondition(const std::vector<std::string> &triggeringEntityNames):
        triggeringEntityNames(triggeringEntityNames)
    {}
    ByEntityCondition(const ByEntityCondition&) = default;  ///< copy constructor
    virtual ~ByEntityCondition();

    /*!
     * \brief GetTriggeringAgents 
     *
     * \param[in] world world interface
     * \return          The vector of agent interface
     */
    std::vector<AgentInterface *> GetTriggeringAgents(WorldInterface* const world) const
    {
        std::vector<AgentInterface *> triggeringAgents {};

        if(triggeringEntityNames.empty())
        {
            const auto &agentMap = world->GetAgents();
            std::transform(agentMap.cbegin(),
                           agentMap.cend(),
                           std::back_inserter(triggeringAgents),
                           [] (const auto &agentPair) -> AgentInterface*
                           {
                               return agentPair.second;
                           });
        }
        else
        {
            for (const auto& triggeringEntityName : triggeringEntityNames)
            {
                const auto triggeringAgent = world->GetAgentByName(triggeringEntityName);

                if(triggeringAgent != nullptr)
                {
                    triggeringAgents.emplace_back(triggeringAgent);
                }
            }
        }

        return triggeringAgents;
    }

private:
    std::vector<std::string> triggeringEntityNames;
};

/// OpenScenario Time to collision Conditions
class OPENPASSCOMMONEXPORT TimeToCollisionCondition : public ByEntityCondition
{
public:
    /**
     * @brief Construct a new Time To Collision Condition object
     * 
     * @param triggeringEntityNames Names of triggering entities
     * @param referenceEntityName   Names of reference entities
     * @param targetTTC             Target time to collision
     * @param rule                  Rule to check if the time to collision condition is met
     */
    TimeToCollisionCondition(const std::vector<std::string>& triggeringEntityNames,
                             const std::string& referenceEntityName,
                             const double targetTTC,
                             const Rule rule):
        ByEntityCondition(triggeringEntityNames),
        referenceEntityName(referenceEntityName),
        targetTTC(targetTTC),
        rule(rule)
    {}
    TimeToCollisionCondition(const TimeToCollisionCondition&) = default; ///< copy constructor
    virtual ~TimeToCollisionCondition();

    /**
     * @brief Function to check if the condition is met
     * 
     * @param world             world interface pointer
     * @return AgentInterfaces  list of agent interfaces
     */
    AgentInterfaces IsMet(WorldInterface * const world) const;

private:
    std::string referenceEntityName;
    double targetTTC;
    Rule rule;
};

/// Open scenario time headway condition
class OPENPASSCOMMONEXPORT TimeHeadwayCondition : public ByEntityCondition
{
public:
    /**
     * @brief Construct a new Time Headway Condition object
     * 
     * @param triggeringEntityNames Names of triggering entities
     * @param referenceEntityName   Names of reference entities
     * @param targetTHW             Target time headway condition
     * @param freeSpace             Freespace
     * @param rule                  Rule to check if the condition is met
     */
    TimeHeadwayCondition(const std::vector<std::string>& triggeringEntityNames,
                         const std::string& referenceEntityName,
                         const double targetTHW,
                         const bool freeSpace,
                         const Rule rule):
        ByEntityCondition(triggeringEntityNames),
        referenceEntityName(referenceEntityName),
        targetTHW(targetTHW),
        freeSpace(freeSpace),
        rule(rule)
    {}
    TimeHeadwayCondition(const TimeHeadwayCondition&) = default;    ///< copy constructor
    virtual ~TimeHeadwayCondition();

    /**
     * @brief Function to check if the condition is met
     * 
     * @param world             world interface pointer
     * @return AgentInterfaces  list of agent interfaces
     */
    AgentInterfaces IsMet(WorldInterface * const world) const;

private:
    std::string referenceEntityName;
    double targetTHW;
    bool freeSpace;
    Rule rule;
};

/// open scenario reach position condition
class OPENPASSCOMMONEXPORT ReachPositionCondition : public ByEntityCondition
{
public:
    /**
     * @brief Construct a new Reach Position Condition object
     * 
     * @param triggeringEntityNames Names of triggering entities
     * @param tolerance             Tolerance to reach poisiton
     * @param position              Position to reach
     */
    ReachPositionCondition(const std::vector<std::string>& triggeringEntityNames,
                           const double tolerance,
                           const openScenario::Position position):
        ByEntityCondition(triggeringEntityNames),
        tolerance(tolerance),
        position(position)
    {
        if (tolerance < 0)
        {
            throw std::runtime_error("Reach Position Tolerance must be greater than or equal to 0");
        }

        if (std::holds_alternative<openScenario::RoadPosition>(position))
        {
            const auto roadPosition = std::get<openScenario::RoadPosition>(position);

            if (roadPosition.s < 0)
            {
                throw std::runtime_error("Reach Position Target S Coordinate must be greater than or equal to 0");
            }

        }
    }
    ReachPositionCondition(const ReachPositionCondition&) = default;    ///< copy constructor
    virtual ~ReachPositionCondition();

    /**
     * @brief Function to check if the condition is met
     * 
     * @param world             world interface pointer
     * @return AgentInterfaces  list of agent interfaces
     */
    AgentInterfaces IsMet(WorldInterface * const world) const;

private:
    double tolerance{};
    openScenario::Position position{};
};

/// open scenario relative speed condition
class OPENPASSCOMMONEXPORT RelativeSpeedCondition : public ByEntityCondition
{
public:
    /**
     * @brief Construct a new Relative Speed Condition object
     * 
     * @param triggeringEntityNames Names of triggering entities
     * @param referenceEntityName   Names of reference entities
     * @param value                 Value of the relative speed
     * @param rule                  Rule to check if the condition is met
     */
    RelativeSpeedCondition(const std::vector<std::string> &triggeringEntityNames,
                           const std::string &referenceEntityName,
                           const double value,
                           const Rule rule):
        ByEntityCondition(triggeringEntityNames),
        referenceEntityName(referenceEntityName),
        value(value),
        rule(rule)
    {}
    RelativeSpeedCondition(const RelativeSpeedCondition&) = default;    ///< copy constructor
    virtual ~RelativeSpeedCondition();

    /**
     * @brief Function to check if the condition is met
     * 
     * @param world             world interface pointer
     * @return AgentInterfaces  list of agent interfaces
     */
    AgentInterfaces IsMet(WorldInterface * const world) const;

private:
    std::string referenceEntityName{};
    double value{};
    Rule rule{};
};

/// OpenScenario ByValue Conditions
class OPENPASSCOMMONEXPORT ByValueCondition
{
public:
    /**
     * @brief Construct a new By Value Condition object
     * 
     * @param rule Rule to check if a value condition is met
     */
    ByValueCondition(const Rule rule):
        rule(rule)
    {}
    ByValueCondition(const ByValueCondition&) = default;    ///< copy constructor
    virtual ~ByValueCondition();
protected:
    Rule rule; ///< rule for the condition to be true
};

/// open scenario simulation time condition
class OPENPASSCOMMONEXPORT SimulationTimeCondition : public ByValueCondition
{
public:
    /**
     * @brief Construct a new Simulation Time Condition object
     * 
     * @param rule                  Rule to check if condition is met
     * @param targetValueInSeconds  Seconds of simulation time to meet
     */
    SimulationTimeCondition(const Rule rule,
                            const double targetValueInSeconds):
        ByValueCondition(rule),
        targetValue(static_cast<int>(targetValueInSeconds * 1000.0))
    {}
    SimulationTimeCondition(const SimulationTimeCondition&) = default;  ///< copy constructor
    virtual ~SimulationTimeCondition();

    /**
     * @brief Function to check if the simulation time condition is met
     * 
     * @param value value to reach
     * @return true if the value is reached
     */
    bool IsMet(const int value) const;

    /**
     * @brief Get the Target Value object
     * 
     * @return returns the target simulation time 
     */
    int GetTargetValue() const;

private:
    int targetValue;
};


using Condition = std::variant<ReachPositionCondition,
                               RelativeSpeedCondition,
                               SimulationTimeCondition,
                               TimeToCollisionCondition,
                               TimeHeadwayCondition>;
using ConditionGroup = std::vector<Condition>;

///
/// \brief Event specific information collected from an openSCENARIO story
///
struct OPENPASSCOMMONEXPORT ConditionalEventDetectorInformation
{
    ActorInformation actorInformation{};  ///< Actor information 
    int numberOfExecutions{};             ///< Specifies number of executions. Use -1 for "unrestricted"
    std::string eventName{};              ///< Event name
    ConditionGroup conditions{};          ///< Condition group
};

} // openScenario
