/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/events/basicEvent.h"
#include "include/signalInterface.h"

namespace openpass::events {

//-----------------------------------------------------------------------------
/** This class hold information about a  Component State Change Event
 *
 * \ingroup Event */
//-----------------------------------------------------------------------------

class ComponentStateChangeEvent : public OpenScenarioEvent
{
public:
    static constexpr char *TOPIC {"OpenSCENARIO/UserDefinedAction/CustomCommandAction/SetComponentState"};     ///< Unique topic identification

    /**
     * @brief Construct a new Component State Change Event object
     * 
     * @param time              Current time
     * @param eventName         Name of the event used for identification
     * @param source            Name of the current component
     * @param triggeringAgents  All agents which triggered the event
     * @param actingAgents      All agents which are affected by the event
     * @param componentName     The name of the registered component
     * @param componentState    The state of the registered component
     */
    ComponentStateChangeEvent(int time, std::string eventName, std::string source,
                     openpass::type::TriggeringEntities triggeringAgents, openpass::type::AffectedEntities actingAgents,
                     const std::string componentName, ComponentState componentState) :
        OpenScenarioEvent{time, eventName, source, triggeringAgents, actingAgents},
        componentName{std::move(componentName)},
        componentState{std::move(componentState)}
    {
    }

    const std::string componentName;        ///< Name of the component
    const ComponentState componentState;    ///< State of the component
};

} // namespace openpass::events
