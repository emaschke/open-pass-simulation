/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "commonHelper.h"

#include <map>
#include <optional>
#include <ostream>
#include <string>
#include <variant>
#include <vector>

namespace openScenario {

/// Value of a ParameterDeclaration or ParameterAssignment element
using ParameterValue = std::variant<bool, int, double, std::string>;

/// Content of a ParameterDeclarations or ParameterAssignments element
using Parameters = std::map<std::string, ParameterValue>;

/// Attribute in a catalog that is not yet resolved
template <typename T>
struct ParameterizedAttribute
{
    std::string name; ///< Name of the parameter in OpenSCENARIO
    T defaultValue;   ///< Value defined in the catalog (may be overwritten in the CatalogReference later)

    ParameterizedAttribute() = default;

    /**
     * @brief Construct a new Parameterized Attribute object
     * 
     * @param value TODO
     */
    ParameterizedAttribute(const T& value) :
        name{""},
        defaultValue{value} {}

    /**
     * @brief Construct a new Parameterized Attribute object
     * 
     * @param name          TODO
     * @param defaultValue 
     */
    ParameterizedAttribute(const std::string& name, const T& defaultValue) :
        name{name},
        defaultValue{defaultValue} {}
};

enum class Shape
{
    Linear = 0,
    Step
};

enum class SpeedTargetValueType
{
    Delta = 0,
    Factor
};

/// This is a custom solution and not according to the openScenario standard
struct StochasticAttribute
{
    /// value
    double value = -999;
    /// mean
    double mean = -999;
    /// standard deviation
    double stdDeviation = -999;
    /// lower boundary
    double lowerBoundary = -999;
    /// upper boundary
    double upperBoundary = -999;
};

/// @brief  Information of the given actor
struct ActorInformation
{
    /// is actor triggering entity
    bool actorIsTriggeringEntity{false};
    /// list of actors
    std::optional<std::vector<std::string>> actors{};
};

/// OSC Orientation
enum class OrientationType
{
    Undefined = 0,
    Relative,
    Absolute
};

/// OSC Orientation
struct Orientation
{
    /// orientation type
    std::optional<OrientationType> type{};
    /// h type
    std::optional<double> h{};
    /// p type
    std::optional<double> p{};
    /// r type
    std::optional<double> r{};
};

/// OSC Position
struct LanePosition
{
    /// orientation of lane
    std::optional<Orientation> orientation{};
    /// id of the road
    std::string roadId{};
    /// id of the lane
    int laneId{};
    /// offset of the lane
    std::optional<double> offset{};
    /// s position
    double s{};

    /// stochastic offset
    std::optional<StochasticAttribute> stochasticOffset;
    /// stochastic s position
    std::optional<StochasticAttribute> stochasticS;
};

/// OSC RelativeLanePosition
struct RelativeLanePosition
{
    /// reference entity
    std::string entityRef{};
    /// d lane
    int dLane{};
    /// ds
    double ds{};
    /// offset of relative lane
    std::optional<double> offset{};
    /// orientation of relative lane
    std::optional<Orientation> orientation{};
};

/// OSC RoadPosition
struct RoadPosition
{
    /// orientation of road
    std::optional<Orientation> orientation{};
    /// id of the road
    std::string roadId{};
    /// s position
    double s{};
    /// t position
    double t{};
};

/// OSC WorldPosition
struct WorldPosition
{
    /// x coordinate
    double x{};
    /// y coordinate
    double y{};
    /// z coordinate
    std::optional<double> z;
    /// h value
    std::optional<double> h;
    /// p value
    std::optional<double> p;
    /// r value
    std::optional<double> r;
};

/// OSC RelativeObjectPosition
struct RelativeObjectPosition {

    /// orientation of relative object
    std::optional<Orientation> orientation{};
    /// reference entity
    std::string entityRef{};
    /// dx
    double dx{};
    /// dy
    double dy{};
    /// dz
    std::optional<double> dz{};
};

/// OSC RelativeWorldPosition
struct RelativeWorldPosition {
    /// orientation of world relatively
    std::optional<Orientation> orientation{};
    /// reference entity
    std::string entityRef{};
    /// relative x position
    double dx{};
    /// relative y position
    double dy{};
    /// relative z position
    std::optional<double> dz{};
};

/// One of different variants of Position in OpenSCENARIO
using Position = std::variant<LanePosition,
                              RelativeLanePosition,
                              RoadPosition,
                              WorldPosition,
                              RelativeObjectPosition,
                              RelativeWorldPosition>;

/// Information about traffic signal controller phase
struct TrafficSignalControllerPhase
{
    /// name of the traffic signal controller phase
    std::string name;
    /// duration
    double duration;
    /// state of the traffic signal controller phase
    std::map<std::string, std::string> states;
};

/// Information about traffic signal controller
struct TrafficSignalController
{
    /// name of the traffic signal controller
    std::string name;
    /// delay
    double delay;
    /// phases of the traffic signal controller
    std::vector<TrafficSignalControllerPhase> phases;
};

// Action
/// GlobalAction
enum EntityActionType
{
    Delete = 0,
    Add
};

/// @brief Action of given entity
struct EntityAction
{
    /// reference entity
    std::string entityRef{};
    /// type of the entity
    EntityActionType type{};
};

/// OpenSCENARIO Sun
struct Sun
{
    /// intensity of sun
    double intensity{0.};
    /// azimuth angle of sun
    double azimuth{0.};
    /// elevation angle of sun
    double elevation{0.};
};

/// OpenSCENARIO Fog
struct Fog
{
    /// visual range
    double visualRange{0.};
};

/// OpenSCENARIO Precipitation
struct Precipitation
{
    /// OSC precipitation type
    enum Type
    {
        dry,
        rain,
        snow
    } type;     ///< type of the OSC precipitation

    /// intensity of the precipitation
    double intensity;
};

//! OpenSCENARIO Wind
struct Wind
{
    double direction{0.};       ///< Direction of the wind flow
    double speed{0.};           ///< Speed of the wind flow
};

//! OpenSCENARIO Weather
struct Weather
{
    /// state of the cloud
    enum CloudState
    {
        skyOff,
        free,
        cloudy,
        overcast,
        rainy
    } cloudState;    ///< state of the cloud

    /// sun
    Sun sun;
    /// fog
    Fog fog;
    /// precipitation
    Precipitation precipitation;
    Wind wind;                                      ///< Wind flow
    std::optional<double> temperature{};            ///< Temperator in Fahrenheit
    std::optional<double> atmosphericPressure{};    ///< Atmospheric pressure in pascal
};

/// OpenSCENARIO EnvironmentAction
struct EnvironmentAction
{
    /// Weather
    Weather weather;
};

using GlobalAction = std::variant<EntityAction, EnvironmentAction>;

/// PrivateAction
struct TrajectoryPoint
{
    /// time
    double time{};
    /// x position of trajectory point
    double x{};
    /// y position of trajectory point
    double y{};
    /// yaw angle
    double yaw{};

    /**
     * @brief operator == to compare with trajectory point
     * 
     * @param other Another trajectory point
     * @return true if two trajectory points are same 
     */
    bool operator== (const TrajectoryPoint& other) const
    {
        return CommonHelper::DoubleEquality(other.time, time)
                && CommonHelper::DoubleEquality(other.x, x)
                && CommonHelper::DoubleEquality(other.y, y)
                && CommonHelper::DoubleEquality(other.yaw, yaw);
    }

    /**
     * @brief operator << overload to compare with other trajectory point
     * 
     * @param os        TODO
     * @param point 
     * @return std::ostream& 
     */
    friend std::ostream& operator<<(std::ostream& os, const TrajectoryPoint& point)
    {
        os << "t = " << point.time
           << " : (" << point.x
           << ", " << point.y
           << ") yaw = " << point.yaw;

        return os;
    }
};

/// OSC TimeReference (for FollowTrajectoryAction)
struct TrajectoryTimeReference
{
    /// domain absolute relative
    std::string domainAbsoluteRelative;
    /// scale
    double scale;
    /// offset
    double offset;
};

/// OSC Trajectory
struct Trajectory
{
    /// trajectory point
    std::vector<TrajectoryPoint> points;
    /// trajectory name
    std::string name;
    /// trajectory time reference
    std::optional<TrajectoryTimeReference> timeReference;

    /// operator << overload to compare with other trajectory
    /// @param os         the output stream
    /// @param trajectory the trajectory
    friend std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory)
    {
        os << "Trajectory \"" << trajectory.name << "\"\n";
        for (const auto& point : trajectory.points)
        {
            os << point << "\n";
        }

        return os;
    }
};

/// OSC AssignRouteAction
using AssignRouteAction = std::vector<RoadPosition>;

/// OSC FollowTrajectoryAction
struct FollowTrajectoryAction
{
    /// trajectory for FollowTrajectoryAction
    Trajectory trajectory{};
};

/// OSC AcquirePositionAction
struct AcquirePositionAction
{
    /// position
    Position position{};
};

/// Some variant of a RoutingAction in OpenSCENARIO
using RoutingAction = std::variant<AssignRouteAction, FollowTrajectoryAction, AcquirePositionAction>;

/// visibility action
struct VisibilityAction
{
    /// graphics
    bool graphics {false};
    /// traffic
    bool traffic {false};
    /// sensors
    bool sensors {false};
};

/// LaneChangeParameter
struct LaneChangeParameter
{
    /// different types of LaneChangeParameter
    enum class Type
    {
        Absolute,
        Relative
    };

    /// different types of DynamicsType
    enum class DynamicsType
    {
        Time,
        Distance
    };

    /// Whether the target is absolute or relative
    Type type{}; 
    /// Value of the target element
    int value{};    
    /// Name of the reference object if relative
    std::string object{}; 
    /// Time or distance defined by Dynamics element
    double dynamicsTarget{}; 
    /// Whether time or distance was defined by Dynamics element
    DynamicsType dynamicsType{};
};

/// @brief Lane change action
struct LaneChangeAction
{
    /// LaneChangeParameter
    LaneChangeParameter laneChangeParameter{};
};

using LateralAction = std::variant<LaneChangeAction>;

/// @brief Information on transition dynamics
struct TransitionDynamics
{
    /// shape
    Shape shape{};
    /// value
    double value{};
    /// dimension
    std::string dimension{};
};

/// @brief absolute target speed
struct AbsoluteTargetSpeed
{
    /// value of absolute target speed
    double value{};
};

/// @brief relative target speed
struct RelativeTargetSpeed
{
    /// reference entity
    std::string entityRef{};
    /// value of relative target speed
    double value{};
    /// speed target value type
    SpeedTargetValueType speedTargetValueType{};
    /// is it continuous
    bool continuous{};
};

/// using speed action target as a variant of absolute, relative target speed
using SpeedActionTarget = std::variant<AbsoluteTargetSpeed, RelativeTargetSpeed>;

/// OSC SpeedAction
struct SpeedAction
{
    /// transition dynamics
    TransitionDynamics transitionDynamics {};
    /// speed action target
    SpeedActionTarget target {};

    /// stochastic value
    std::optional<StochasticAttribute> stochasticValue {};
    /// stochastic dynamics
    std::optional<StochasticAttribute> stochasticDynamics {};
};


/// typedef longitudina action as a variant of speed action
using LongitudinalAction = std::variant<SpeedAction>;

/// typedef teleport action as position
using TeleportAction = Position;

/// typedef private action as a variant of lateral, routing, teleport, visibilitiy or longitudinal action
using PrivateAction = std::variant<LateralAction, LongitudinalAction, RoutingAction, TeleportAction, VisibilityAction>;

/// UserDefinedAction
struct CustomCommandAction
{
    /// command
    std::string command;
};

/// type def user defined action as a variant of custom command action
using UserDefinedAction = std::variant<CustomCommandAction>;

/// using action as one of the variant of global, private, user defined actions
using Action = std::variant<GlobalAction, PrivateAction, UserDefinedAction>;

/// Information of the manipulator
struct ManipulatorInformation
{
    /**
     * @brief Construct a new Manipulator Information object
     * 
     * @param action     Action
     * @param eventName  Name of the event
     */
    ManipulatorInformation(const Action action, const std::string eventName):
        action(action),
        eventName(eventName)
    {}

    /// action type
    const Action action;
    /// event name
    const std::string eventName;
};
} // namespace openScenario
