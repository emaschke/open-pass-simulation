/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2020 HLRS, University of Stuttgart
 *               2016-2021 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  globalDefinitions.h
//! @brief This file contains several classes for global purposes
//-----------------------------------------------------------------------------

#pragma once

#include "common/opMath.h"
#include <array>
#include <list>
#include <map>
#include <string>
#include <tuple>
#include <vector>

// the following is a temporary workaround until the contribution is merged into osi
#if defined(_WIN32) && !defined(NODLL)
#define OSIIMPORT __declspec(dllimport)
#define OSIEXPORT __declspec(dllexport)

#elif (defined(__GNUC__) && __GNUC__ >= 4 || defined(__clang__))
#define OSIEXPORT __attribute__((visibility("default")))
#define OSIIMPORT OSIEXPORT

#else
#define OSIIMPORT   ///< TODO
#define OSIEXPORT   ///< TODO
#endif


/// TODO
#if defined(open_simulation_interface_EXPORTS)
#define OSI_EXPORT OSIEXPORT
#else
#define OSI_EXPORT OSIIMPORT
#endif

//-----------------------------------------------------------------------------
//! @brief Containing the three possible states regarding lane change
//-----------------------------------------------------------------------------
enum class LaneChangeState
{
    NoLaneChange = 0,
    LaneChangeLeft,
    LaneChangeRight
};

//-----------------------------------------------------------------------------
//! weekday type
//-----------------------------------------------------------------------------
enum class Weekday
{
    Undefined = 0,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
};

//-----------------------------------------------------------------------------
//! Agent category classification
//-----------------------------------------------------------------------------
enum class AgentCategory
{
    Ego = 0,
    Scenario,
    Common,
    Any
};

namespace openpass::utils {

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 4> AgentCategoryMapping{
    "Ego",
    "Scenario",
    "Common",
    "Any"};

/**
 * @brief Convert AgentCategory to cstr (constexpr)
 * 
 * @param agentCategory Agent category classification
 * @return Returns category of the agent as string representation 
 */
constexpr const char *to_cstr(AgentCategory agentCategory)
{
    return AgentCategoryMapping[static_cast<std::size_t>(agentCategory)];
}

/**
 * @brief Convert AgentCategory to std::string
 * 
 * @param agentCategory Agent category classification
 * @return Returns category of the agent as string
 */
inline std::string to_string(AgentCategory agentCategory) noexcept
{
    return std::string(to_cstr(agentCategory));
}

} // namespace util

//-----------------------------------------------------------------------------
//! Agent type classification
//-----------------------------------------------------------------------------
enum class AgentVehicleType
{
    NONE = -2,
    Undefined = -1,
    Car = 0,
    Pedestrian = 1,
    Motorbike = 2,
    Bicycle = 3,
    Truck = 4
};

namespace openpass::utils {

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 7> AgentVehicleTypeMapping{
    "NONE",
    "Undefined",
    "Car",
    "Pedestrian",
    "Motorbike",
    "Bicycle",
    "Truck"};

/**
 * @brief Convert AgentVehicleType to cstr (constexpr)
 * 
 * @param agentVehicleType  Agent type classification
 * @return Returns type of the agent as string representation
 */
constexpr const char *to_cstr(AgentVehicleType agentVehicleType)
{
    return AgentVehicleTypeMapping[static_cast<std::size_t>(agentVehicleType) -
                                   static_cast<std::size_t>(AgentVehicleType::NONE)];
}

/**
 * @brief Convert AgentVehicleType to std::string
 * 
 * @param agentVehicleType  Agent type classification
 * @return Returns type of the agent as string 
 */
inline std::string to_string(AgentVehicleType agentVehicleType) noexcept
{
    return std::string(to_cstr(agentVehicleType));
}

} // namespace util


/**
 * @brief // @brief convert a vehicle type name to VehicleType enum
 * 
 * @param strVehicleType vehicle type
 * @return returns agentVehicleType 
 */
inline AgentVehicleType GetAgentVehicleType(const std::string &strVehicleType)
{
    if (0 == strVehicleType.compare("Car"))
    {
        return AgentVehicleType::Car;
    }
    else if (0 == strVehicleType.compare("Pedestrian"))
    {
        return AgentVehicleType::Pedestrian;
    }
    else if (0 == strVehicleType.compare("Motorbike"))
    {
        return AgentVehicleType::Motorbike;
    }
    else if (0 == strVehicleType.compare("Bicycle"))
    {
        return AgentVehicleType::Bicycle;
    }
    else if (0 == strVehicleType.compare("Truck"))
    {
        return AgentVehicleType::Truck;
    }
    return AgentVehicleType::Undefined;
}

/**
 * @brief convert a AgentVehicleType to VehicleType string
 * 
 * @param vehicleType Type of vehicle
 * @return the string of vehicle type
 */
inline std::string GetAgentVehicleTypeStr(const AgentVehicleType &vehicleType)
{
    return (vehicleType == AgentVehicleType::Car) ? "Car" : (vehicleType == AgentVehicleType::Pedestrian) ? "Pedestrian" : (vehicleType == AgentVehicleType::Motorbike) ? "Motorbike" : (vehicleType == AgentVehicleType::Bicycle) ? "Bicycle" : (vehicleType == AgentVehicleType::Truck) ? "Truck" : "unknown type";
}

/**
 * @brief convert a string of type code to VehicleType string
 * 
 * @param vehicleTypeCode Code to vehicletype
 * @return returns a string of type code to VehicleType string
 */
inline std::string GetAgentVehicleTypeStr(const std::string &vehicleTypeCode)
{
    try
    {
        AgentVehicleType vehicleType = static_cast<AgentVehicleType>(std::stoi(vehicleTypeCode));
        return GetAgentVehicleTypeStr(vehicleType);
    }
    catch (...)
    {
        return "unknown type";
    }
}

/// State of indicator lever
enum class IndicatorLever
{
    IndicatorLever_Off = 0,
    IndicatorLever_Left = 1,
    IndicatorLever_Right = -1
};

/// state of the indicator
enum class IndicatorState
{
    IndicatorState_Off = 0,
    IndicatorState_Left = 1,
    IndicatorState_Right = 2,
    IndicatorState_Warn = 3
};

/// @brief Position of the object
struct Position
{
    Position()
    {
    }
    
    /**
     * @brief Construct a new Position object
     * 
     * @param x             x coordinate
     * @param y             y coordinate
     * @param yaw           yaw angle
     * @param curvature     curvature
     */
    Position(double x,
             double y,
             double yaw,
             double curvature) :
        xPos(x),
        yPos(y),
        yawAngle(yaw),
        curvature(curvature)
    {
    }

    /// x coordinate of the position
    double xPos{0};
    /// y coordinate of the position
    double yPos{0};
    /// yaw angle
    double yawAngle{0};
    /// curvature
    double curvature{0};
};

//! Enum of potential types of marks.
enum class MarkType
{
    NONE,
    CONTINUOUS,
    INTERRUPTED_LONG,
    INTERRUPTED_SHORT,
    ROADSIDE,
    NumberOfMarkTypes
};

//! Enum of potential types of objects.
enum class ObjectType
{
    NONE,
    OBJECT,
    VIEWOBJECT,
    NumberOfObjectTypes
};

/// OSI object type
enum ObjectTypeOSI : int
{
    None = 0x00, // default at initialization
    Vehicle = 0x01,
    Object = 0x02,
    Any = Vehicle | Object
};

/// typedef collision partner as a pair of type of OSI object and the id associated to that object
using CollisionPartner = std::pair<ObjectTypeOSI, int>;

/// state of the light
enum class LightState
{
    Off = 0,
    LowBeam,
    FogLight,
    HighBeam,
    Flash,
    NumberOfLightStates
};

//! Possibile direction of view angle with agent in center.
enum class AgentViewDirection
{
    none,
    front,
    left,
    back,
    right,
    NumberOfCarViewDirections
};

/// side whether left or right
enum class Side
{
    Left,
    Right
};

/// @brief Parameters of a given vehicle model
struct VehicleModelParameters
{
    /// vehicle type of the agent
    AgentVehicleType vehicleType;

    struct BoundingBoxCenter        ///< Center position of bounding box of vehicle
    {
        /// x coordinate of bounding box center
        double x;       
        /// y coordinate of bounding box center
        double y;  
        /// z coordinate of bounding box center    
        double z;       
    } boundingBoxCenter; ///< bounding box center

    struct BoundingBoxDimensions    ///< Dimensions of bounding box of vehicle
    {
        /// width coordinate of bounding box
        double width;   
        /// length coordinate of bounding box
        double length;  
        /// height coordinate of bounding box
        double height;  
    } boundingBoxDimensions; ///< bounding box dimensions

    
    struct Performance  ///< Performance of the vehicle
    {
        /// maximum speed of vehicle
        double maxSpeed;   
        /// maximum acceleration of vehicle     
        double maxAcceleration; 
        /// maximum deceleration of vehicle
        double maxDeceleration;
    } performance; ///< performance of the vehicle

    /// axle parameters
    struct Axle
    {
        /// maximum steering of the axle  
        double maxSteering;     
        /// wheel diameter
        double wheelDiameter;   
        /// track width
        double trackWidth;      
        /// X position of axle
        double positionX;       
        /// Z position of axle
        double positionZ;       
    };
    /// fron axle of the vehicle
    Axle frontAxle;             
    /// rear axle of vehicle
    Axle rearAxle;              

    /// properties of the vehicle
    std::map<std::string, double> properties;   
};

/// Type of the ADAS system
enum class AdasType
{
    Safety = 0,
    Comfort,
    Undefined
};

/// map adas type to the string
const std::map<AdasType, std::string> adasTypeToString = {{AdasType::Safety, "Safety"},
                                                          {AdasType::Comfort, "Comfort"},
                                                          {AdasType::Undefined, "Undefined"}};

/// Type of the component
enum class ComponentType
{
    Driver = 0,
    TrajectoryFollower,
    VehicleComponent,
    Undefined
};

/// category for lanes
enum class LaneCategory
{
    Undefined = 0,
    RegularLane,
    RightMostLane
};

//! Defines which traffic rules are in effect
struct TrafficRules
{
    /// maximum allowed speed if not restricted by signs
    double openSpeedLimit; 
     /// maximum allowed speed for trucks if not restricted by signs
    double openSpeedLimitTrucks;
    /// maximum allowed speed for buses if not restricted by signs
    double openSpeedLimitBuses; 
    /// if true, vehicles must use the outermost free lane for driving
    bool keepToOuterLanes;
    /// if true, it is prohibited to overtake another vehicle, that is driving further left (or right for lefthand traffic) 
    bool dontOvertakeOnOuterLanes; 
    /// if true, vehicles driving in a traffic jam must form a corridor for emergency vehicles
    bool formRescueLane; 
    /// if true all merging shall be performed using zipper merge
    bool zipperMerge; 
};

//! Defines the weight of a path for randomized route generation
struct TurningRate
{
    /// id of the incoming road
    std::string incoming {}; 
    /// id of the outgoing road (i.e. connector)
    std::string outgoing {}; 
    /// weight relative to all paths with the same incoming road
    double weight {1.}; 
};

/// list of turning rate
using TurningRates = std::vector<TurningRate>;

//-----------------------------------------------------------------------------
//! Representation of an agent as defined in the run configuration.
//-----------------------------------------------------------------------------
class AgentSpawnItem
{
public:
    /**
     * @brief Construct a new Agent Spawn Item object
     * 
     * @param id        Id of the agent spawn item
     * @param reference Reference of the agent spawn item
     */
    AgentSpawnItem(int id, int reference) :
        id(id),
        reference(reference)
    {
    }
    AgentSpawnItem(const AgentSpawnItem &) = delete;
    AgentSpawnItem(AgentSpawnItem &&) = delete;
    AgentSpawnItem &operator=(const AgentSpawnItem &) = delete;
    AgentSpawnItem &operator=(AgentSpawnItem &&) = delete;
    virtual ~AgentSpawnItem() = default;

    /*!
     * \brief GetId function to get id of the agent
     *
     * \return Id of the agent spawn item
     */
    int GetId() const
    {
        return id;
    }

    /*!
     * \brief GetReference function to get reference id of the agent
     *
     * \return Reference of the agent spawn item
     */
    int GetReference() const
    {
        return reference;
    }

    /*!
     * \brief GetVehicleType function to get vehicle type
     *
     * \return Vehicle type of the agent
     */
    AgentVehicleType GetVehicleType() const
    {
        return vehicleType;
    }

    /*!
     * \brief GetWidth function to get width
     *
     * \return width of the agent
     */
    double GetWidth() const
    {
        return width;
    }

    /*!
     * \brief GetLength function to get length
     *
     * \return length of the agent
     */
    double GetLength() const
    {
        return length;
    }

    /*!
     * \brief GetDistanceCOGtoFrontAxle function to get distance from COG to front axle
     *
     * \return distance of center of gravity to front axle of the agent
     */
    double GetDistanceCOGtoFrontAxle() const
    {
        return distanceCOGtoFrontAxle;
    }

    /*!
     * \brief GetWeight function to get width
     *
     * \return width of the agent
     */
    double GetWeight() const
    {
        return weight;
    }

    /*!
     * \brief GetHeightCOG function to get height
     *
     * \return height of COG
     */
    double GetHeightCOG() const
    {
        return heightCOG;
    }

    /*!
     * \brief GetWheelbase function to get wheel base
     *
     * \return wheelbase of the agent
     */
    double GetWheelbase() const
    {
        return wheelbase;
    }

    /*!
     * \brief GetMomentInertiaRoll function to get rolling moment of inertia
     *
     * \return rolling moment of inertia of the agent
     */
    double GetMomentInertiaRoll() const
    {
        return momentInertiaRoll;
    }

    /*!
     * \brief GetMomentInertiaPitch function to get pitching moment of inertia
     *
     * \return moment of inertia during pitching of the agent
     */
    double GetMomentInertiaPitch() const
    {
        return momentInertiaPitch;
    }

    /*!
     * \brief GetMomentInertiaYaw function to get yaw moment of inertia
     *
     * \return yaw moment of inertia of the agent
     */
    double GetMomentInertiaYaw() const
    {
        return momentInertiaYaw;
    }

    /*!
     * \brief GetFrictionCoeff function to get coefficient of friction
     *
     * \return coefficient of friction
     */
    double GetFrictionCoeff() const
    {
        return frictionCoeff;
    }

    /*!
     * \brief GetTrackWidth function to get track width
     *
     * \return track width of the agent
     */
    double GetTrackWidth() const
    {
        return trackWidth;
    }

    /*!
     * \brief GetDistanceCOGtoLeadingEdge function to get distance from COG to LeadingEdge
     *
     * \return distance of COG to LeadingEdge of the agent
     */
    double GetDistanceCOGtoLeadingEdge() const
    {
        return distanceCOGtoLeadingEdge;
    }

    /*!
     * \brief SetVehicleType function to set vehicle type
     *
     * \param vehicleType type of the agent
     */
    void SetVehicleType(AgentVehicleType vehicleType)
    {
        this->vehicleType = vehicleType;
    }

    /*!
     * \brief SetWidth function to set width
     *
     * \param width width of the agent
     */
    void SetWidth(double width)
    {
        this->width = width;
    }

    /*!
     * \brief SetLength function to length vehicle type
     *
     * \param length length of the agent
     */
    void SetLength(double length)
    {
        this->length = length;
    }

    /*!
     * \brief SetDistanceCOGtoFrontAxle function to set distance from COG to FrontAxle type
     *
     * \param distanceCOGtoFrontAxle distance from COG to FrontAxle of the agent
     */
    void SetDistanceCOGtoFrontAxle(double distanceCOGtoFrontAxle)
    {
        this->distanceCOGtoFrontAxle = distanceCOGtoFrontAxle;
    }

    /*!
     * \brief SetWeight function to set weight
     *
     * \param weight of the agent
     */
    void SetWeight(double weight)
    {
        this->weight = weight;
    }

    /*!
     * \brief SetHeightCOG function to set height of COG
     *
     * \param heightCOG height of COG
     */
    void SetHeightCOG(double heightCOG)
    {
        this->heightCOG = heightCOG;
    }

    /*!
     * \brief SetWheelbase function to set wheel base
     *
     * \param wheelbase wheel base of the agent
     */
    void SetWheelbase(double wheelbase)
    {
        this->wheelbase = wheelbase;
    }

    /*!
     * \brief SetMomentInertiaRoll function to set rolling moment of inertia
     *
     * \param momentInertiaRoll rolling moment of inertia of the agent
     */
    void SetMomentInertiaRoll(double momentInertiaRoll)
    {
        this->momentInertiaRoll = momentInertiaRoll;
    }

    /*!
     * \brief SetMomentInertiaPitch function to set pitch moment of inertia
     *
     * \param momentInertiaPitch pitch moment of inertia
     */
    void SetMomentInertiaPitch(double momentInertiaPitch)
    {
        this->momentInertiaPitch = momentInertiaPitch;
    }

    /*!
     * \brief SetMomentInertiaYaw function to set yaw moment of inertia
     *
     * \param momentInertiaYaw yaw moment of inertia of the agent
     */
    void SetMomentInertiaYaw(double momentInertiaYaw)
    {
        this->momentInertiaYaw = momentInertiaYaw;
    }

    /*!
     * \brief SetFrictionCoeff function to set coefficient of friction
     *
     * \param frictionCoeff coefficient of friction
     */
    void SetFrictionCoeff(double frictionCoeff)
    {
        this->frictionCoeff = frictionCoeff;
    }

    /*!
     * \brief SetTrackWidth function to set track width
     *
     * \param trackWidth track width of the agent
     */
    void SetTrackWidth(double trackWidth)
    {
        this->trackWidth = trackWidth;
    }

    /*!
     * \brief SetDistanceCOGtoLeadingEdge function to set distance of COG to leading edge
     *
     * \param distanceCOGtoLeadingEdge distance of COG to the leading edge of the agent
     */
    void SetDistanceCOGtoLeadingEdge(double distanceCOGtoLeadingEdge)
    {
        this->distanceCOGtoLeadingEdge = distanceCOGtoLeadingEdge;
    }

private:
    int id;
    int reference;
    AgentVehicleType vehicleType;
    double positionX;
    double positionY;
    double width;
    double length;
    double velocityX;
    double velocityY;
    double distanceCOGtoFrontAxle;
    double weight;
    double heightCOG;
    double wheelbase;
    double momentInertiaRoll;
    double momentInertiaPitch;
    double momentInertiaYaw;
    double frictionCoeff;
    double trackWidth;
    double distanceCOGtoLeadingEdge;
    double accelerationX;
    double accelerationY;
    double yawAngle;
};

struct PostCrashVelocity    ///< Post crach velocity parameters
{
    /// activity flag
    bool isActive = false; 
    /// post crash velocity, absolute [m/s]
    double velocityAbsolute = 0.0;
    /// post crash velocity direction [rad]
    double velocityDirection = 0.0;
    /// post crash yaw velocity [rad/s]
    double yawVelocity = 0.0;
};

/*!
 * For definitions see http://indexsmart.mirasmart.com/26esv/PDFfiles/26ESV-000177.pdf
 */
struct CollisionAngles{
    /// opponent yaw angle
    double OYA = 0.0; 
    /// original host collision point angle
    double HCPAo = 0.0; 
    /// original opponent collision point angle
    double OCPAo = 0.0; 
    /// transformed host collision point angle
    double HCPA = 0.0; 
    /// transformed opponent collision point angle
    double OCPA = 0.0; 
};

class AgentInterface;
/// list of agent interface pointers
using AgentInterfaces = std::vector<const AgentInterface*>;
