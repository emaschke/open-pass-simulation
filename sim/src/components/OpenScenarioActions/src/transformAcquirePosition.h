/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "src/common/acquirePositionSignal.h"
#include "src/common/events/acquirePositionEvent.h"

#include "transformerBase.h"

namespace openScenario::transformation {

/// Struct representing the acquire position signal
struct AcquirePosition : public TransformerBase<AcquirePosition, AcquirePositionSignal, openpass::events::AcquirePositionEvent>
{
    /**
     * @brief Convert to signal
     * 
     * @param event     Acquire position action
     * @param world     Pointer to the world interface
     * @param agent     Pointer to agent instance
     * @param cycleTime Corresponds to "cycleTime" of "Component"
     * @return TODO
     */
    static std::shared_ptr<AcquirePositionSignal> ConvertToSignal(const openpass::events::AcquirePositionEvent &event,
                                                                  WorldInterface *world,
                                                                  AgentInterface *agent,
                                                                  int cycleTime);
};

} // namespace openScenario::transformation
