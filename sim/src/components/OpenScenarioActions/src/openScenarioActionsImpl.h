/********************************************************************************
 * Copyright (c) 2020 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2020-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <map>

#include "include/modelInterface.h"
#include "transformAcquirePosition.h"
#include "transformDefaultCustomCommandAction.h"
#include "transformLaneChange.h"
#include "transformSpeedAction.h"
#include "transformTrajectory.h"
/**
* \brief Relays triggered OpenScenario actions as signals to other components
*
* When used in conjunction with autoregistering transformers (\sa actionTransformRepository),
* the linkIdMapping has to be provided via the parameter interface.
*
* That means that when a transformer for e.g. event X is autoregistered,
* a parameter for mapping X::Topic to a linkId needs to be defined.
*
* Syntax:
* @cond
* <parameter>
*     <id>THE_TOPIC</id>
*     <type>int</type>
*     <unit/>
*     <value>THE_LINK_ID</value>
* </parameter>
* @endcond
*
* \ingroup OpenScenarioActions
*/
class OpenScenarioActionsImplementation : public UnrestrictedEventModelInterface
{
public:
    /// Name of this component
    static constexpr char *COMPONENTNAME{"OpenScenarioActions"};

    /**
     * @brief Construct a new Open Scenario Actions Implementation object
     * 
     * @param[in]     componentName  Name of the component
     * @param[in]     isInit         Corresponds to "init" of "Component"
     * @param[in]     priority       Corresponds to "priority" of "Component"
     * @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
     * @param[in]     responseTime   Corresponds to "responseTime" of "Component"
     * @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
     * @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
     * @param[in]     world          Pointer to the world interface
     * @param[in]     parameters     Pointer to the parameters of the module
     * @param[in]     publisher      Pointer to the publisher instance
     * @param[in]     callbacks      Pointer to the callbacks
     * @param[in]     agent          Pointer to agent instance
     * @param[in]     eventNetwork   Pointer to event network
     */ 
    OpenScenarioActionsImplementation(std::string componentName,
                                      bool isInit,
                                      int priority,
                                      int offsetTime,
                                      int responseTime,
                                      int cycleTime,
                                      StochasticsInterface *stochastics,
                                      WorldInterface *world,
                                      const ParameterInterface *parameters,
                                      PublisherInterface *const publisher,
                                      const CallbackInterface *callbacks,
                                      AgentInterface *agent,
                                      core::EventNetworkInterface *const eventNetwork);

    /**
     * @brief Update Inputs
     *
     * Function is called by framework when another component delivers a signal over
     * a channel to this component (scheduler calls update taks of other component).
     *
     * Refer to module description for input channels and input ids.
     *
     * @param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
     * @param[in]     data           Referenced signal (copied by sending component)
     * @param[in]     time           Current scheduling time
     */
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;
    
    /**
     * @brief Update outputs.
     *
     * Function is called by framework when this component has to deliver a signal over
     * a channel to another component (scheduler calls update task of this component).
     *
     * Refer to module description for output channels and output ids.
     *
     * @param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
     * @param[out]    data           Referenced signal (copied by this component)
     * @param[in]     time           Current scheduling time
     */
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;
    
    /**
     * @brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component
     * 
     * @param time  Current scheduling time
     */
    void Trigger(int time) override;

    //! Indicates linkId 0 as a TrajectorySignalLinkId value
    using TrajectorySignalLinkId = std::integral_constant<LinkId, 0>;
    //! Indicates linkId 3 as a SpeedActionSignalLinkId value
    using SpeedActionSignalLinkId = std::integral_constant<LinkId, 3>;
    //! Indicates linkId 4 as a AcquirePositionSignalLinkId value
    using AcquirePositionSignalLinkId = std::integral_constant<LinkId, 4>;
    //! Indicates linkId 5 as a StringSignalLinkId value
    using StringSignalLinkId = std::integral_constant<LinkId, 5>;

private:
    [[noreturn]] void ThrowUnregisteredIdentifier(const std::string &identifier);
    [[noreturn]] void ThrowOnTooManySignals(LinkId localLinkId);
    [[noreturn]] void ThrowOnInvalidLinkId(LinkId localLinkId);

    TransformResults pendingSignals;

    inline static std::vector<bool> registeredActions{
        ActionTransformRepository::Register(openScenario::transformation::Trajectory::Transform),
        ActionTransformRepository::Register(openScenario::transformation::LaneChange::Transform),
        ActionTransformRepository::Register(openScenario::transformation::SpeedAction::Transform),
        ActionTransformRepository::Register(openScenario::transformation::AcquirePosition::Transform),
        ActionTransformRepository::Register(openScenario::transformation::DefaultCustomCommandAction::Transform)};

    std::map<const std::string, LinkId> linkIdMapping{
        {openpass::events::TrajectoryEvent::TOPIC, TrajectorySignalLinkId::value},
        {openpass::events::LaneChangeEvent::TOPIC, TrajectorySignalLinkId::value},
        {openpass::events::SpeedActionEvent::TOPIC, SpeedActionSignalLinkId::value},
        {openpass::events::AcquirePositionEvent::TOPIC, AcquirePositionSignalLinkId::value},
        {openpass::events::DefaultCustomCommandActionEvent::TOPIC, StringSignalLinkId::value}};
};
