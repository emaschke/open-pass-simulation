/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef VEHICLESIMPLETT_H
#define VEHICLESIMPLETT_H

#include <array>
#include <vector>

#include <QtGlobal>

#include "common/vector2d.h"
#include "dynamics_twotrack_tire.h"
#define NUMBER_OF_WHEELS 4

class Tire;

//! Simple STATIC two-track vehicle model
class VehicleSimpleTT
{
public:
    VehicleSimpleTT();
    ~VehicleSimpleTT();

    /**
     * @brief Initialize tire characteristics
     * 
     * @param weight        Mass of the car
     * @param P_engine      Maximal engine power
     * @param T_brakeLimit  Brake force limit
     */
    void InitSetEngine(double weight,
                       double P_engine, double T_brakeLimit);
    /**
     * @brief Initialize car's physics
     * 
     * @param x_wheelbase   Wheel base of the vehicle
     * @param x_COG         Height of the COG (center-of-gravity)
     * @param y_track       Track width
     * @param y_COG         Longitudinal position of the COG (center-of-gravity)
     */
    void InitSetGeometry(double x_wheelbase, double x_COG, double y_track, double y_COG);
    /**
     * @brief Initialize car's velocity
     * 
     * @param vel               Initial velocity
     * @param mu_tire_max       Peak tire force
     * @param mu_tire_slide     Force at full slide
     * @param s_max             Slip corresponding to peak tire force
     * @param r_tire            Radius of the tire
     * @param frictionScaleRoll Road/tire friction coefficient
     */
    void InitSetTire(double vel,
                     double mu_tire_max, double mu_tire_slide,
                     double s_max, double r_tire, double frictionScaleRoll);

    /**
     * Initialize car's velocity
     */
    void UpdatePosition(double);
    /**
     * @brief Refresh car's velocity
     */
    void SetVelocity(Common::Vector2d, const double);
    
    /**
     * @brief Calculate local tire torques
     * 
     * @param throttlePedal     Throttle pedal position in the range [0...1]
     * @param brakePedal        Brake pedal position in the range [0...1]
     * @param brakeSuperpose    Brake position for each tire
     */
    void DriveTrain(double throttlePedal, double brakePedal, std::array<double, NUMBER_OF_WHEELS> brakeSuperpose);
    
    /**
     * @brief Local forces and moments transferred onto road
     * 
     * @param timeStep
     * @param forceVertical 
     */
    void ForceLocal(double timeStep, double, std::vector<double> forceVertical);
    
    /**
     * @brief Global force and moment
     * 
     */
    void ForceGlobal();

    /**
     * @brief Get the Tire Force object
     * 
     * @param tireNumber 
     * @return double 
     */
    double GetTireForce(int tireNumber);

    /**
     * @brief Get the Force Tire Vertical Static object
     * 
     * @param tireNumber 
     * @return double 
     */
    double GetForceTireVerticalStatic(int tireNumber);
    /// Total force on vehicle's CoM
    Common::Vector2d forceTotalXY;
    /// Total momentum on the vehicle around the z-axes
    double momentTotalZ;

    /// force on tire in vertical direction
    std::array<double, NUMBER_OF_WHEELS> forceTireVerticalStatic;

private:

    //! Inertial moment of tires [kg*m^2]
    std::array<double, NUMBER_OF_WHEELS> inertiaTireX;

    //! Maximal engine power [W]
    double powerEngineLimit;
    //! Brake force limit [N]
    double torqueBrakeLimit;

    //! Mass of the car [kg]
    double massTotal;
    //! Tire positions in car CS [m]
    std::array<Common::Vector2d, NUMBER_OF_WHEELS> positionTire;

    //! Drag coefficient (Asbo from http://rc.opelgt.org/indexcw.php) []
    const double coeffDrag = 0.34;
    //! Face area (Asbo from http://rc.opelgt.org/indexcw.php) [m^2]
    const double areaFace = 1.94;
    //! Air density [kg/m^3]
    const double densityAir = 1.29;
    //! Earth's gravitation acceleration
    const double accelVerticalEarth = -9.81;
    //! Toe-in/-out
    const double anglePreSet = 0.0;//0.003;
    //! Brake balance
    const double brakeBalance = 0.67;
    //! Max. engine moment
    const double torqueEngineLimit = 10000.0;
    // Dynamics to remember
    std::array<double, NUMBER_OF_WHEELS> rotationVelocityTireX;
    std::array<double, NUMBER_OF_WHEELS> rotationVelocityGradTireX;
    double yawVelocity;
    Common::Vector2d velocityCar;
    std::array<Common::Vector2d, NUMBER_OF_WHEELS> forceTire;
    std::array<Common::Vector2d, NUMBER_OF_WHEELS> slipTire;
    std::array<double, NUMBER_OF_WHEELS> torqueTireXthrottle;
    std::array<double, NUMBER_OF_WHEELS> torqueTireXbrake;
    std::array<double, NUMBER_OF_WHEELS> momentTireZ;
    std::vector<Tire *> tires;

};

#endif // VEHICLESIMPLETT_H
