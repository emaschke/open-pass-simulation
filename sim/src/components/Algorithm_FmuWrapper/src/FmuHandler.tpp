#include "SignalInterface/SignalTranslator.h"
#include "common/driverWarning.h"
#include "common/agentCompToCompCtrlSignal.h"
#include "FmuCalculations.h"

/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*               2019-2021 in-tech GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/


template <size_t FMI>
FmuHandler<FMI>::FmuHandler(std::string componentName,
                            fmu_check_data_t& cdata,
                            FmuVariables& fmuVariables,
                            std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                            const ParameterInterface *parameters,
                            WorldInterface *world,
                            AgentInterface *agent,
                            const CallbackInterface *callbacks) :
        FmuHandlerInterface(componentName, cdata, agent, callbacks),
        fmiVersionEnum{cdata.version},
        fmuVariables(fmuVariables),
        fmuVariableValues(fmuVariableValues),
        world(world),
        parameters(parameters),
        bb_center_offset_x(agent->GetVehicleModelParameters().boundingBoxCenter.x),
        agentIdString(FmuFileHelper::CreateAgentIDString(agent->GetId()))
{
    fmuCommunications = std::make_shared<FmuCommunication<FMI>>(componentName,
            cdata,
            fmuVariableValues,
            parameters,
            world,
            agent,
            callbacks);
}
template <size_t FMI>
FmuVariables FmuHandler<FMI>::GetFmuVariables()
{
    return fmuCommunications->GetFmuVariables();
}


template <size_t FMI>
void FmuHandler<FMI>::InitOsiVariables(const ParameterInterface *parameters)
{
    if(cdata.FMUPath != nullptr)
    {
        std::filesystem::path fmuPath = cdata.FMUPath;
        fmuName = fmuPath.filename().replace_extension().string();
    }
    else
    {
        LOGWARN("FmuHandler: FMU cdata has no FMU path.");
    }


    for (const auto& [key, value] : parameters->GetParametersString())
    {
        const auto pos = key.find('_');
        const auto type = key.substr(0, pos);
        const auto variableName = key.substr(pos+1);
        if (type == "Input" || type == "Output" || type == "Init")
        {
            const auto findResult = variableMapping.at(type).find(value);
            if (findResult != variableMapping.at(type).cend())
            {
                findResult->second = variableName;
            }
        }
    }

    auto writeSensorDataFlag = parameters->GetParametersBool().find("WriteJson_SensorData");
    if (writeSensorDataFlag != parameters->GetParametersBool().end())
    {
        writeSensorData = writeSensorDataFlag->second;
    }
    auto writeTraceSensorDataFlag = parameters->GetParametersBool().find("WriteTrace_SensorData");
    if (writeTraceSensorDataFlag != parameters->GetParametersBool().end())
    {
        writeTraceSensorData = writeTraceSensorDataFlag->second;
    }
    auto writeSensorViewFlag = parameters->GetParametersBool().find("WriteJson_SensorView");
    if (writeSensorViewFlag != parameters->GetParametersBool().end())
    {
        writeSensorView = writeSensorViewFlag->second;
    }
    auto writeTraceSensorViewFlag = parameters->GetParametersBool().find("WriteTrace_SensorView");
    if (writeTraceSensorViewFlag != parameters->GetParametersBool().end())
    {
        writeTraceSensorView = writeTraceSensorViewFlag->second;
    }
    auto writeSensorViewConfigFlag = parameters->GetParametersBool().find("WriteJson_SensorViewConfig");
    if (writeSensorViewConfigFlag != parameters->GetParametersBool().end())
    {
        writeSensorViewConfig = writeSensorViewConfigFlag->second;
    }
    auto writeTraceSensorViewConfigFlag = parameters->GetParametersBool().find("WriteTrace_SensorViewConfig");
    if (writeTraceSensorViewConfigFlag != parameters->GetParametersBool().end())
    {
        writeTraceSensorViewConfig = writeTraceSensorViewConfigFlag->second;
    }
    auto writeSensorViewConfigRequestFlag = parameters->GetParametersBool().find("WriteJson_SensorViewConfigRequest");
    if (writeSensorViewConfigRequestFlag != parameters->GetParametersBool().end())
    {
        writeSensorViewConfigRequest = writeSensorViewConfigRequestFlag->second;
    }
    auto writeTraceSensorViewConfigRequestFlag = parameters->GetParametersBool().find("WriteTrace_SensorViewConfigRequest");
    if (writeTraceSensorViewConfigRequestFlag != parameters->GetParametersBool().end())
    {
        writeTraceSensorViewConfigRequest = writeTraceSensorViewConfigRequestFlag->second;
    }
    auto writeGroundTruthFlag = parameters->GetParametersBool().find("WriteJson_GroundTruth");
    if (writeGroundTruthFlag != parameters->GetParametersBool().end())
    {
        writeGroundTruth = writeGroundTruthFlag->second;
    }
    auto writeTraceGroundTruthFlag = parameters->GetParametersBool().find("WriteTrace_GroundTruth");
    if (writeTraceGroundTruthFlag != parameters->GetParametersBool().end())
    {
        writeTraceGroundTruth = writeTraceGroundTruthFlag->second;
    }
    auto writeTrafficCommandFlag = parameters->GetParametersBool().find("WriteJson_TrafficCommand");
    if (writeTrafficCommandFlag != parameters->GetParametersBool().end())
    {
        writeTrafficCommand = writeTrafficCommandFlag->second;
    }
    auto writeTraceTrafficCommandFlag = parameters->GetParametersBool().find("WriteTrace_TrafficCommand");
    if (writeTraceTrafficCommandFlag != parameters->GetParametersBool().end())
    {
        writeTraceTrafficCommand = writeTraceTrafficCommandFlag->second;
    }
    auto writeTrafficUpdateFlag = parameters->GetParametersBool().find("WriteJson_TrafficUpdate");
    if (writeTrafficUpdateFlag != parameters->GetParametersBool().end())
    {
        writeTrafficUpdate = writeTrafficUpdateFlag->second;
    }
    auto writeTraceTrafficUpdateFlag = parameters->GetParametersBool().find("WriteTrace_TrafficUpdate");
    if (writeTraceTrafficUpdateFlag != parameters->GetParametersBool().end())
    {
        writeTraceTrafficUpdate = writeTraceTrafficUpdateFlag->second;
    }
    auto writeHostVehicleDataFlag = parameters->GetParametersBool().find("WriteJson_HostVehicleData");
    if (writeHostVehicleDataFlag != parameters->GetParametersBool().end())
    {
        writeHostVehicleData = writeTrafficUpdateFlag->second;
    }
    auto writeTraceHostVehicleDataFlag = parameters->GetParametersBool().find("WriteTrace_HostVehicleData");
    if (writeTraceHostVehicleDataFlag != parameters->GetParametersBool().end())
    {
        writeTraceHostVehicleData = writeTraceTrafficUpdateFlag->second;
    }
#ifdef USE_EXTENDED_OSI
    auto writeMotionCommandFlag = parameters->GetParametersBool().find("WriteJson_MotionCommand");
    if (writeMotionCommandFlag != parameters->GetParametersBool().end())
    {
        writeMotionCommand = writeMotionCommandFlag->second;
    }
    auto writeTraceMotionCommandFlag = parameters->GetParametersBool().find("WriteTrace_MotionCommand");
    if (writeTraceMotionCommandFlag != parameters->GetParametersBool().end())
    {
        writeTraceMotionCommand = writeTraceMotionCommandFlag->second;
    }
    auto writeVehicleCommunicationDataFlag = parameters->GetParametersBool().find("WriteJson_VehicleCommunicationData");
    if (writeVehicleCommunicationDataFlag != parameters->GetParametersBool().end())
    {
        writeVehicleCommunicationData = writeVehicleCommunicationDataFlag->second;
    }
    auto writeTraceVehicleCommunicationDataFlag = parameters->GetParametersBool().find("WriteTrace_VehicleCommunicationData");
    if (writeTraceVehicleCommunicationDataFlag != parameters->GetParametersBool().end())
    {
        writeTraceVehicleCommunicationData = writeTraceVehicleCommunicationDataFlag->second;
    }

    bool writeJsonOutput = writeSensorData || writeSensorView || writeSensorViewConfig || writeSensorViewConfigRequest || writeTrafficCommand || writeTrafficUpdate || writeMotionCommand || writeVehicleCommunicationData || writeGroundTruth || writeHostVehicleData;
    bool writeTraceOutput = writeTraceSensorData || writeTraceSensorView || writeTraceSensorViewConfig || writeTraceSensorViewConfigRequest || writeTraceTrafficCommand || writeTraceTrafficUpdate || writeTraceMotionCommand || writeTraceVehicleCommunicationData || writeTraceGroundTruth || writeTraceHostVehicleData;
#else
    bool writeJsonOutput = writeSensorData || writeSensorView || writeSensorViewConfig || writeSensorViewConfigRequest || writeTrafficCommand || writeTrafficUpdate || writeGroundTruth || writeHostVehicleData;
    bool writeTraceOutput = writeTraceSensorData || writeTraceSensorView || writeTraceSensorViewConfig || writeTraceSensorViewConfigRequest || writeTraceTrafficCommand || writeTraceTrafficUpdate || writeTraceGroundTruth || writeTraceHostVehicleData;
#endif

    if (writeJsonOutput)
    {
        const std::string runtimeOutputDir = parameters->GetRuntimeInformation().directories.output;
        outputDir = FmuFileHelper::CreateFmuJsonOutputDir(fmuName, agent->GetId(), runtimeOutputDir);

        QDir directory{outputDir};
        if (!directory.exists())
        {
            directory.mkpath(outputDir);
        }
    }

    if (writeTraceOutput)
    {
        const std::string runtimeOutputDir = parameters->GetRuntimeInformation().directories.output;
        outputDir = FmuFileHelper::CreateFmuTraceOutputDir(fmuName, agent->GetId(), runtimeOutputDir);

        QDir directory{traceOutputDir};
        if (!directory.exists())
        {
            directory.mkpath(traceOutputDir);
        }
    }

    auto enforceDoubleBufferingFlag = parameters->GetParametersBool().find("EnforceDoubleBuffering");
    if (enforceDoubleBufferingFlag != parameters->GetParametersBool().end())
    {
        enforceDoubleBuffering = enforceDoubleBufferingFlag->second;
    }

    ParseFmuParameters(parameters);
}

template <size_t FMI>
FmuHandler<FMI>::~FmuHandler()
{
    FmuFileHelper::WriteTracesToFile(traceOutputDir, fileToOutputTracesMap);
}

template <size_t FMI>
void FmuHandler<FMI>::PrepareInit()
{
    this->fmuVariables = GetFmuVariables();

    const auto sensorList = parameters->GetParameterLists().find("SensorLinks");
    if (sensorList != parameters->GetParameterLists().cend())
    {
        for (const auto& sensorLink : sensorList->second)
        {
            if (sensorLink->GetParametersString().at("InputId") == "Camera")
            {
                sensors.push_back(sensorLink->GetParametersInt().at("SensorId"));
            }
        }
    }
    ParseChannelDefinitions(parameters, fmuVariables);
    InitOsiVariables(parameters);
}

template <size_t FMI>
void FmuHandler<FMI>::Init()
{
    if (groundTruthVariable.has_value())
    {
        SetGroundTruth();

        if (writeGroundTruth)
        {
            FmuFileHelper::WriteJson(groundTruth, "GroundTruth.json", this->outputDir);
        }
        if (writeTraceGroundTruth)
        {
            FmuHelper::AppendMessages(appendedSerializedGroundTruth, serializedGroundTruth);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedGroundTruth, "GroundTruth", QString::fromStdString(fmuName),
                                            1000/cycleTime, "gt", fileToOutputTracesMap);
        }
    }
    if (sensorViewConfigRequestVariable.has_value())
    {
        SetSensorViewConfigRequest();
        SetSensorViewConfig();

        if (writeSensorViewConfig)
        {
            FmuFileHelper::WriteJson(sensorViewConfig, "SensorViewConfig.json", this->outputDir);
        }
        if (writeTraceSensorViewConfig)
        {
            FmuHelper::AppendMessages(appendedSerializedSensorViewConfig, serializedSensorViewConfig);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorViewConfig, "SensorViewConfig", QString::fromStdString(fmuName),
                                            1000/cycleTime, "sv", fileToOutputTracesMap);
        }
        if (writeSensorViewConfigRequest)
        {
            FmuFileHelper::WriteJson(sensorViewConfigRequest, "SensorViewConfigRequest.json", this->outputDir);
        }
        if (writeTraceSensorViewConfigRequest)
        {
            FmuHelper::AppendMessages(appendedSerializedSensorViewConfigRequest, serializedSensorViewConfigRequest);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorViewConfigRequest, "SensorViewConfigRequest", QString::fromStdString(fmuName),
                                            1000/cycleTime, "sv", fileToOutputTracesMap);
        }
    }
    else
    {
        sensorViewConfig = FmuHelper::GenerateDefaultSensorViewConfiguration();
    }

    isInitialized = true;
}

template <size_t FMI>
void FmuHandler<FMI>::UpdateInput(int localLinkId,
                                  const std::shared_ptr<SignalInterface const> &data,
                                  [[maybe_unused]] int time)
{
    if(auto inputSignalTranslator = InputSignalTranslatorFactory::build(localLinkId, *world, *agent, *this->callbacks))
    {
        const google::protobuf::Message *message;
        switch(localLinkId)
        {
            case 2:
                message = inputSignalTranslator.value()->translate(data, nullptr);
                sensorDataIn = *dynamic_cast<const osi3::SensorData*>(message);
                break;
            case 10:
            case 11:
            case 12:
            case 13:

                if (!trafficCommand.has_timestamp() || osi3::utils::GetTimestampInMilliseconds(trafficCommand) != time)
                {
                    const auto newTrafficCommand = new osi3::TrafficCommand;
                    newTrafficCommand->CopyFrom(trafficCommand);
                    osi3::utils::SetTimestamp(*newTrafficCommand, time);
                    osi3::utils::SetVersion(*newTrafficCommand);
                    auto messageIn = static_cast<const google::protobuf::Message*>(newTrafficCommand);
                    message = inputSignalTranslator.value()->translate(data, messageIn);
                }
                else
                {
                    auto messageIn = static_cast<const google::protobuf::Message*>(&trafficCommand);
                    message = inputSignalTranslator.value()->translate(data, messageIn);
                }
                trafficCommand = *dynamic_cast<const osi3::TrafficCommand*>(message);
                break;
        }
    }

    if (sensorViewConfigRequestVariable.has_value())
    {
        SetSensorViewConfigRequest();

        if (previousSerializedSensorViewConfigRequest != serializedSensorViewConfigRequest)
        {
            SetSensorViewConfig();

            if (writeSensorViewConfig)
            {
                FmuFileHelper::WriteJson(sensorViewConfig, "SensorViewConfig-" + QString::number(time) + ".json", this->outputDir);
            }
            if (writeSensorViewConfigRequest)
            {
                FmuFileHelper::WriteJson(sensorViewConfigRequest, "SensorViewConfigRequest-" + QString::number(time) + ".json", this->outputDir);
            }
        }
    }
}

template <size_t FMI>
void FmuHandler<FMI>::UpdateOutput(int localLinkId, std::shared_ptr<const SignalInterface> &data, int time)
{
    Q_UNUSED(time)
    LOGDEBUG("UpdateOutput started");

    try
    {
        auto componentStateValueReference = std::get<FMI>(fmuOutputs).find(SignalValue::ComponentState);
        ComponentState componentState = ComponentState::Acting;

        if (componentStateValueReference != std::get<FMI>(fmuOutputs).end())
        {
            // FMI version 1 enums needs to be shifted -1
            if (FMI == FMI1)
            {
                componentState = static_cast<ComponentState>(GetFmuSignalValue(SignalValue::ComponentState, VariableType::Enum).intValue - 1);
            }
            else
            {
                componentState = static_cast<ComponentState>(GetFmuSignalValue(SignalValue::ComponentState, VariableType::Enum).intValue);
            }
        }


        const google::protobuf::Message *messageIn;
        if(auto outputSignalTranslator = OutputSignalTranslatorFactory::build(localLinkId, *world, *agent, *this->callbacks))
        {
            switch(localLinkId)
            {
                case 0:

                    if (trafficUpdateVariable.has_value() && trafficUpdate.update_size() > 0)
                    {
                        messageIn = static_cast<const google::protobuf::Message*>(&trafficUpdate);
                        data = outputSignalTranslator.value()->translate(messageIn);
                    }
#ifdef USE_EXTENDED_OSI
                    else if (motionCommandVariable.has_value())
                    {
                        messageIn = static_cast<const google::protobuf::Message*>(&motionCommand);
                        data = outputSignalTranslator.value()->translate(messageIn);
                    }
#endif
                    break;
                case 6:
                    messageIn = static_cast<const google::protobuf::Message*>(&sensorDataOut);
                    data = outputSignalTranslator.value()->translate(messageIn);
                    break;
            }
        }

        // TODO Put this code from GenericFmuHandler into SignalTranslator
        if (localLinkId == 0)
        {

            if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::DynamicsSignal) != outputSignals.cend())
            {
                double acceleration = GetFmuSignalValue(SignalValue::DynamicsSignal_Acceleration, VariableType::Double).realValue;
                double velocity = GetFmuSignalValue(SignalValue::DynamicsSignal_Velocity, VariableType::Double).realValue;
                double positionX = GetFmuSignalValue(SignalValue::DynamicsSignal_PositionX, VariableType::Double).realValue;
                double positionY = GetFmuSignalValue(SignalValue::DynamicsSignal_PositionY, VariableType::Double).realValue;
                double yaw = GetFmuSignalValue(SignalValue::DynamicsSignal_Yaw, VariableType::Double).realValue;
                double yawRate = GetFmuSignalValue(SignalValue::DynamicsSignal_YawRate, VariableType::Double).realValue;
                double yawAcceleration = GetFmuSignalValue(SignalValue::DynamicsSignal_YawAcceleration, VariableType::Double).realValue;
                double steeringWheelAngle = GetFmuSignalValue(SignalValue::DynamicsSignal_SteeringWheelAngle, VariableType::Double).realValue;
                double centripetalAcceleration = GetFmuSignalValue(SignalValue::DynamicsSignal_CentripetalAcceleration, VariableType::Double).realValue;
                double travelDistance = GetFmuSignalValue(SignalValue::DynamicsSignal_TravelDistance, VariableType::Double).realValue;

                data = std::make_shared<DynamicsSignal const>(ComponentState::Acting,
                                                              acceleration,
                                                              velocity * std::cos(yaw),
                                                              velocity * std::sin(yaw),
                                                              positionX,
                                                              positionY,
                                                              yaw,
                                                              yawRate,
                                                              yawAcceleration,
                                                              0.0,
                                                              steeringWheelAngle,
                                                              centripetalAcceleration,
                                                              travelDistance);
            }
#ifdef USE_EXTENDED_OSI
            else if (!(trafficUpdateVariable.has_value() && trafficUpdate.update_size() > 0) && !(motionCommandVariable.has_value()))
#else
            else if (!(trafficUpdateVariable.has_value() && trafficUpdate.update_size() > 0))
#endif
            {
                //TODO throw error or use empty dynamic signal?
                data = std::make_shared<DynamicsSignal const>(ComponentState::Disabled, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
                //LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Cannot provide DynamicsSignal, as neither TrafficUpdate, MotionCommand nor DynamicSignal are connected");
            }
        }
        else if (localLinkId == 1)
        {
            if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::AccelerationSignal) != outputSignals.cend())
            {
                auto acceleration = GetFmuSignalValue(SignalValue::AccelerationSignal_Acceleration, VariableType::Double).realValue;
                data = std::make_shared<AccelerationSignal const>(componentState, acceleration);
            }
            else
            {
                data = std::make_shared<AccelerationSignal const>(ComponentState::Disabled, 0.0);
            }
        }
        else if (localLinkId == 2)
        {
            if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::LongitudinalSignal) != outputSignals.cend())
            {
                auto accPedalPos = GetFmuSignalValue(SignalValue::LongitudinalSignal_AccPedalPos, VariableType::Double).realValue;
                auto brakePedalPos = GetFmuSignalValue(SignalValue::LongitudinalSignal_BrakePedalPos, VariableType::Double).realValue;
                auto gear = GetFmuSignalValue(SignalValue::LongitudinalSignal_Gear, VariableType::Int).intValue;
                data = std::make_shared<LongitudinalSignal const>(componentState, accPedalPos, brakePedalPos, gear);
            }
            else
            {
                data = std::make_shared<LongitudinalSignal const>(ComponentState::Disabled, 0.0, 0.0, 0.0);
            }
        }
        else if (localLinkId == 3)
        {
            if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::SteeringSignal) != outputSignals.cend())
            {
                auto steeringWheelAngle = GetFmuSignalValue(SignalValue::SteeringSignal_SteeringWheelAngle, VariableType::Double).realValue;
                data = std::make_shared<SteeringSignal const>(componentState, steeringWheelAngle);
            }
            else
            {
                data = std::make_shared<SteeringSignal const>(ComponentState::Disabled, 0.0);
            }
        }
        else if (localLinkId == 4)
        {
            if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::CompCtrlSignal) != outputSignals.cend())
            {
                MovementDomain movementDomain = MovementDomain::Undefined;
                std::vector<ComponentWarningInformation> warnings;
                bool withWarningDirection = std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::CompCtrlSignalWarningDirection) != outputSignals.cend();

                // FMI version 1 enums needs to be shifted -1
                if(FMI == FMI1)
                {
                    movementDomain = static_cast<MovementDomain>(GetFmuSignalValue(SignalValue::CompCtrlSignal_MovementDomain, VariableType::Enum).intValue - 1);
                    bool warningActivity = GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningActivity, VariableType::Bool).boolValue;
                    ComponentWarningLevel warningLevel = static_cast<ComponentWarningLevel>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningLevel, VariableType::Enum).intValue - 1);
                    ComponentWarningType warningType = static_cast<ComponentWarningType>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningType, VariableType::Enum).intValue - 1);
                    ComponentWarningIntensity warningIntensity = static_cast<ComponentWarningIntensity>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningIntensity, VariableType::Enum).intValue - 1);
                    auto direction = withWarningDirection ? std::make_optional(static_cast<AreaOfInterest>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningDirection, VariableType::Enum).intValue - 1)) : std::nullopt;
                    warnings.push_back(ComponentWarningInformation{warningActivity, warningLevel, warningType, warningIntensity, direction});
                }
                else
                {
                    movementDomain = static_cast<MovementDomain>(GetFmuSignalValue(SignalValue::CompCtrlSignal_MovementDomain, VariableType::Enum).intValue);
                    bool warningActivity = GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningActivity, VariableType::Bool).boolValue;
                    ComponentWarningLevel warningLevel = static_cast<ComponentWarningLevel>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningLevel, VariableType::Enum).intValue);
                    ComponentWarningType warningType = static_cast<ComponentWarningType>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningType, VariableType::Enum).intValue);
                    ComponentWarningIntensity warningIntensity = static_cast<ComponentWarningIntensity>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningIntensity, VariableType::Enum).intValue);
                    auto direction = withWarningDirection ? std::make_optional(static_cast<AreaOfInterest>(GetFmuSignalValue(SignalValue::CompCtrlSignal_WarningDirection, VariableType::Enum).intValue)) : std::nullopt;
                    warnings.push_back(ComponentWarningInformation{warningActivity, warningLevel, warningType, warningIntensity, direction});
                }

                data = std::make_shared<VehicleCompToCompCtrlSignal const>(ComponentType::VehicleComponent,
                                                                           "FMU",
                                                                           componentState,
                                                                           movementDomain,
                                                                           warnings,
                                                                           AdasType::Safety);
            }
            else
            {
                data = std::make_shared<VehicleCompToCompCtrlSignal const>(ComponentType::VehicleComponent,
                                                                           "FMU",
                                                                           ComponentState::Disabled,
                                                                           MovementDomain::Undefined,
                                                                           std::vector<ComponentWarningInformation>{},
                                                                           AdasType::Safety);
            }
        }

    }
    catch (const std::bad_alloc& e)
    {
        throw std::runtime_error("Could not instantiate output signal: " + std::string(e.what()));
    }

    LOGDEBUG("UpdateOutput finished");
}

template <size_t FMI>
void FmuHandler<FMI>::PreStep([[maybe_unused]]int time)
{
    LOGDEBUG("Start of PreStep");

    const auto& objectsInRange = agent->GetEgoAgent().GetObjectsInRange(0, std::numeric_limits<double>::max(), 0);
    const auto* frontObject = objectsInRange.empty() ? nullptr : objectsInRange.at(0);
    const auto* frontfrontObject = (objectsInRange.size() < 2) ? nullptr : objectsInRange.at(1);
    auto sensorFusionInfo = FmuCalculations::CalculateSensorFusionInfo(sensorDataIn, world, agent);

    size_t i = 0;
    std::vector<int> realValueReferencesVec;
    std::vector<FmuValue> realDataVec;
    for (const auto& fmuInput : std::get<FMI>(fmuRealInputs))
    {
        double dataToAdd = 0;
        realValueReferencesVec.resize(i+1);
        realValueReferencesVec[i] = fmuInput.valueReference;
        switch (fmuInput.type)
        {
            case FmuInputType::VelocityEgo:
                dataToAdd = agent->GetVelocity().Length();
                break;
            case FmuInputType::AccelerationEgo:
                dataToAdd = agent->GetAcceleration().Projection(agent->GetYaw());
                break;
            case FmuInputType::CentripetalAccelerationEgo:
                dataToAdd = agent->GetCentripetalAcceleration();
                break;
            case FmuInputType::SteeringWheelEgo:
                dataToAdd = agent->GetSteeringWheelAngle();
                break;
            case FmuInputType::AccelerationPedalPositionEgo:
                dataToAdd = agent->GetEffAccelPedal();
                break;
            case FmuInputType::BrakePedalPositionEgo:
                dataToAdd = agent->GetEffBrakePedal();
                break;
            case FmuInputType::DistanceRefToFrontEdgeEgo:
                dataToAdd = agent->GetDistanceReferencePointToLeadingEdge();
                break;
            case FmuInputType::PositionXEgo:
                dataToAdd = agent->GetPositionX();
                break;
            case FmuInputType::PositionYEgo:
                dataToAdd = agent->GetPositionY();
                break;
            case FmuInputType::YawEgo:
                dataToAdd = agent->GetYaw();
                break;
            case FmuInputType::PositionSEgo:
                dataToAdd = agent->GetEgoAgent().GetReferencePointPosition().value_or(GlobalRoadPosition{}).roadPosition.s;
                break;
            case FmuInputType::PositionTEgo:
                dataToAdd = agent->GetEgoAgent().GetReferencePointPosition().value_or(GlobalRoadPosition{}).roadPosition.t;
                break;
            case FmuInputType::PositionXFront:
                dataToAdd = frontObject ? frontObject->GetPositionX() : 0.0;
                break;
            case FmuInputType::PositionYFront:
                dataToAdd = frontObject ? frontObject->GetPositionY() : 0.0;
                break;
            case FmuInputType::YawFront:
                dataToAdd = frontObject ? frontObject->GetYaw() : 0.0;
                break;
            case FmuInputType::PositionSFront:
                dataToAdd = frontObject ? frontObject->GetRoadPosition(ObjectPointPredefined::Reference).at(agent->GetEgoAgent().GetRoadId()).roadPosition.s : 0.0;
                break;
            case FmuInputType::PositionTFront:
                dataToAdd = frontObject ? frontObject->GetRoadPosition(ObjectPointPredefined::Reference).at(agent->GetEgoAgent().GetRoadId()).roadPosition.t : 0.0;
                break;
            case FmuInputType::RelativeDistanceFront:
                dataToAdd = frontObject ? agent->GetEgoAgent().GetNetDistance(frontObject).value() : 0.0;
                break;
            case FmuInputType::WidthFront:
                dataToAdd = frontObject ? frontObject->GetWidth() : 0.0;
                break;
            case FmuInputType::LengthFront:
                dataToAdd = frontObject ? frontObject->GetLength() : 0.0;
                break;
            case FmuInputType::DistanceRefToFrontEdgeFront:
                dataToAdd = frontObject ? frontObject->GetDistanceReferencePointToLeadingEdge() : 0.0;
                break;
            case FmuInputType::VelocityFront:
                dataToAdd = frontObject ? frontObject->GetVelocity().Length() : 0.0;
                break;
            case FmuInputType::PositionXFrontFront:
                dataToAdd = frontfrontObject ? frontfrontObject->GetPositionX() : 0.0;
                break;
            case FmuInputType::PositionYFrontFront:
                dataToAdd = frontfrontObject ? frontfrontObject->GetPositionY() : 0.0;
                break;
            case FmuInputType::VelocityFrontFront:
                dataToAdd = frontfrontObject ? frontfrontObject->GetVelocity().Length() : 0.0;
                break;
            case FmuInputType::RelativeDistanceFrontFront:
                dataToAdd = frontfrontObject ? agent->GetEgoAgent().GetNetDistance(frontfrontObject).value() : 0.0;
                break;
            case FmuInputType::SpeedLimit:
                dataToAdd = FmuCalculations::CalculateSpeedLimit(std::get<double>(fmuInput.additionalParameter), agent);
                break;
            case FmuInputType::RoadCurvature:
                dataToAdd = agent->GetEgoAgent().GetLaneCurvature(std::get<double>(fmuInput.additionalParameter)).value_or(0.0);
                break;
            case FmuInputType::SensorFusionRelativeS:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].s;
                break;
            case FmuInputType::SensorFusionRelativeNetS:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].net_s;
                break;
            case FmuInputType::SensorFusionRelativeT:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].t;
                break;
            case FmuInputType::SensorFusionRelativeNetLeft:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].t_left;
                break;
            case FmuInputType::SensorFusionRelativeNetRight:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].t_right;
                break;
            case FmuInputType::SensorFusionRelativeNetX:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].net_x;
                break;
            case FmuInputType::SensorFusionRelativeNetY:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].net_y;
                break;
            case FmuInputType::SensorFusionVelocity:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].velocity;
                break;
            case FmuInputType::SensorFusionVelocityX:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].velocity_x;
                break;
            case FmuInputType::SensorFusionVelocityY:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].velocity_y;
                break;
            case FmuInputType::SensorFusionYaw:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].yaw;
                break;
            default:
                LOGERRORANDTHROW("Input can not be mapped to variable of type Real");
        }
        realDataVec.resize(i+1);
        FmuValue fmuValue;
        fmuValue.realValue = dataToAdd;
        realDataVec[i] = fmuValue;
        ++i;
    }


    std::vector<int> integerValueReferencesVec;
    std::vector<FmuValue> integerDataVec;
    i = 0;
    for (const auto& fmuInput : std::get<FMI>(fmuIntegerInputs))
    {
        int dataToAdd = 0;
        integerValueReferencesVec.resize(i+1);
        integerValueReferencesVec[i] = fmuInput.valueReference;
        switch (fmuInput.type)
        {
            case FmuInputType::LaneEgo:
                dataToAdd = agent->GetEgoAgent().HasValidRoute() ? agent->GetEgoAgent().GetMainLocatePosition().value().laneId : 0;
                break;
            case FmuInputType::LaneFront:
                dataToAdd = frontObject ? frontObject->GetRoadPosition(ObjectPointPredefined::Reference).at(agent->GetEgoAgent().GetRoadId()).laneId : 0.0;
                break;
            case FmuInputType::LaneFrontFront:
                dataToAdd = frontfrontObject ? frontfrontObject->GetRoadPosition(ObjectPointPredefined::Reference).at(agent->GetEgoAgent().GetRoadId()).laneId : 0.0;
                break;
            case FmuInputType::LaneCountLeft:
                dataToAdd = FmuCalculations::CalculateLaneCount(Side::Left, agent);
                break;
            case FmuInputType::LaneCountRight:
                dataToAdd = FmuCalculations::CalculateLaneCount(Side::Right, agent);
                break;
            case FmuInputType::SensorFusionObjectId:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? -1 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].id;
                break;
            case FmuInputType::SensorFusionNumberOfDetectingSensors:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? -1 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].numberOfSensors;
                break;
            case FmuInputType::SensorFusionLane:
                dataToAdd = sensorFusionInfo.size() <= std::get<size_t>(fmuInput.additionalParameter) ? 0 : sensorFusionInfo[std::get<size_t>(fmuInput.additionalParameter)].lane;
                break;
            default:
                LOGERRORANDTHROW("Input can not be mapped to variable of type Integer");
        }
        integerDataVec.resize(i+1);
        FmuValue fmuValue;
        fmuValue.intValue = dataToAdd;
        integerDataVec[i] = fmuValue;
        ++i;
    }

    std::vector<int> booleanValueReferencesVec;
    std::vector<FmuValue> booleanDataVec;
    i = 0;
    for (const auto& fmuInput : std::get<FMI>(fmuBooleanInputs))
    {
        int dataToAdd = 0;
        booleanValueReferencesVec.resize(i+1);
        booleanValueReferencesVec[i] = fmuInput.valueReference;
        switch (fmuInput.type)
        {
            case FmuInputType::ExistenceFront:
                dataToAdd = static_cast<bool>(frontObject);
                break;
            case FmuInputType::ExistenceFrontFront:
                dataToAdd = static_cast<bool>(frontfrontObject);
                break;
            default:
                LOGERRORANDTHROW("Only ExistenceFront and ExistenceFrontFront can be mapped to variable of type Boolean");
        }
        booleanDataVec.resize(i+1);
        FmuValue fmuValue;
        fmuValue.boolValue = dataToAdd;
        booleanDataVec[i] = fmuValue;
        ++i;
    }
    SetFmuValues(realValueReferencesVec, realDataVec, VariableType::Double);
    SetFmuValues(integerValueReferencesVec, integerDataVec, VariableType::Int);
    SetFmuValues(booleanValueReferencesVec, booleanDataVec, VariableType::Bool);

    if (sensorViewVariable)
    {
        auto worldData = static_cast<OWL::Interfaces::WorldData*>(world->GetWorldData());
        auto sensorView = worldData->GetSensorView(sensorViewConfig, agent->GetId(), time);

        SetSensorViewInput(*sensorView);
        if (writeSensorView)
        {
            FmuFileHelper::WriteJson(*sensorView, "SensorView-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceSensorView)
        {
            FmuHelper::AppendMessages(appendedSerializedSensorView, serializedSensorView);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorView, "SensorView", QString::fromStdString(fmuName),
                                            time, "sv", fileToOutputTracesMap);
        }
    }
    if (sensorDataInVariable)
    {
        SetSensorDataInput(sensorDataIn);
        if (writeSensorData)
        {
            FmuFileHelper::WriteJson(sensorDataIn, "SensorDataIn-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceSensorData)
        {
            FmuHelper::AppendMessages(appendedSerializedSensorDataIn, serializedSensorDataIn);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorDataIn, "SensorData", QString::fromStdString(fmuName),
                                            time, "sd", fileToOutputTracesMap);
        }
    }
    if (trafficCommandVariable)
    {

        SetTrafficCommandInput(trafficCommand);
        if (writeTrafficCommand)
        {
            FmuFileHelper::WriteJson(trafficCommand, "TrafficCommand-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceTrafficCommand)
        {
            FmuHelper::AppendMessages(appendedSerializedTrafficCommand, serializedTrafficCommand);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedTrafficCommand, "TrafficCommand", QString::fromStdString(fmuName),
                                            time, "tc", fileToOutputTracesMap);
        }

    }
    if (hostVehicleDataVariable)
    {
        SetHostVehicleDataInput(hostVehicleData);
        if (writeHostVehicleData)
        {
            FmuFileHelper::WriteJson(hostVehicleData, "HostVehicleData-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceHostVehicleData)
        {
            FmuHelper::AppendMessages(appendedSerializedHostVehicleData, serializedHostVehicleData);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedHostVehicleData, "HostVehicleData", QString::fromStdString(fmuName),
                                            time, "tc", fileToOutputTracesMap);
        }
    }
#ifdef USE_EXTENDED_OSI
    if (vehicleCommunicationDataVariable)
    {
        auto hostVehicleData = vehicleCommunicationData.mutable_host_vehicle_data();
        hostVehicleData->mutable_location()->CopyFrom(sensorDataIn.host_vehicle_location());
        hostVehicleData->mutable_location_rmse()->CopyFrom(sensorDataIn.host_vehicle_location_rmse());

        SetVehicleCommunicationDataInput(vehicleCommunicationData);
        if (writeVehicleCommunicationData)
        {
            FmuFileHelper::WriteJson(vehicleCommunicationData, "VehicleCommunicationData-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceVehicleCommunicationData)
        {
            FmuHelper::AppendMessages(appendedSerializedVehicleCommunicationData, serializedVehicleCommunicationData);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedVehicleCommunicationData, "VehicleCommunicationData",
                                            QString::fromStdString(fmuName), time, "vc", fileToOutputTracesMap);
        }
    }
#endif

    LOGDEBUG("End of PreStep");
}

template <size_t FMI>
void FmuHandler<FMI>::PostStep([[maybe_unused]]int time)
{
    LOGDEBUG("Start of PostStep");

    if (sensorDataOutVariable)
    {
        GetSensorData();
        if (writeSensorData)
        {
            FmuFileHelper::WriteJson(sensorDataOut, "SensorDataOut-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceSensorData)
        {
            int valueReference;
            valueReference = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value()+".size").valueReference;
            //valueReference.emplace<FMI>((std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value()+".size").valueReference));
            std::string serializedSensorDataOut{static_cast<const char*>(previousSensorDataOut), static_cast<std::string::size_type>(GetValue(valueReference, VariableType::Int).intValue)};
            FmuHelper::AppendMessages(appendedSerializedSensorDataOut, serializedSensorDataOut);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedSensorDataOut, "SensorData",
                                            QString::fromStdString(fmuName), time, "sd", fileToOutputTracesMap);
        }
    }
#ifdef USE_EXTENDED_OSI
    if (motionCommandVariable)
    {
        GetMotionCommand();
        SetMotionCommandDataInput(motionCommand);
        if (writeMotionCommand)
        {
            FmuFileHelper::WriteJson(motionCommand, "MotionCommand-" + QString::number(time) + ".json", this->outputDir);
        }
        if (writeTraceMotionCommand)
        {
            FmuHelper::AppendMessages(appendedSerializedMotionCommand, serializedMotionCommand);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedMotionCommand, "MotionCommand",
                                            QString::fromStdString(fmuName), time, "mc", fileToOutputTracesMap);
        }
    }
#endif
    if (trafficUpdateVariable)
    {
        GetTrafficUpdate();
        if (writeTrafficUpdate)
        {
            FmuFileHelper::WriteJson(trafficUpdate, "TrafficUpdate-" + QString::number(time) + ".json", this->outputDir);
        }
        if(writeTraceTrafficUpdate)
        {
            FmuHelper::AppendMessages(appendedSerializedTrafficUpdate, serializedTrafficUpdate);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedTrafficUpdate, "TrafficUpdate",
                                            QString::fromStdString(fmuName), time, "tu", fileToOutputTracesMap);
        }
    }

    if (hostVehicleDataVariable)
    {
        GetHostVehicleData();
        if (writeHostVehicleData)
        {
            FmuFileHelper::WriteJson(hostVehicleData, "HostVehicleData-" + QString::number(time) + ".json", this->outputDir);
        }
        if(writeTraceHostVehicleData)
        {
            FmuHelper::AppendMessages(appendedSerializedHostVehicleData, serializedHostVehicleData);
            FmuFileHelper::WriteBinaryTrace(appendedSerializedHostVehicleData, "HostVehicleData",
                                            QString::fromStdString(fmuName), time, "hv", fileToOutputTracesMap);
        }
    }

    LOGDEBUG("End of PostStep");
}

template <size_t FMI>
void FmuHandler<FMI>::ParseChannelDefinitions(const ParameterInterface* parameters, const FmuVariables& fmuVariables)
{
    ChannelDefinitionParser<FMI> channelDefinitionParser(fmuVariables, cdata.version);

    for (const auto& [paramKey, paramValue] : parameters->GetParametersString())
    {
        const auto pos = paramKey.find('_');
        const auto channelCausality = paramKey.substr(0, pos);
        const auto channelType = paramKey.substr(pos + 1);

        if (channelCausality == "Output")
        {
            if (!channelDefinitionParser.AddOutputChannel(paramValue, channelType))
            {
                LOGDEBUG("Unable to parse output channel definition \"" + channelType + "\"");
            }
        }
        else if (channelCausality == "Input")
        {
            if (!channelDefinitionParser.AddInputChannel(paramValue, channelType))
            {
                LOGDEBUG("Unable to parse input channel definition \"" + channelType + "\"");
            }
        }
        else if (channelCausality == "Parameter")
        {
            if (!channelDefinitionParser.AddParameter(paramValue, channelType))
            {
                LOGERRORANDTHROW("Unable to parse parameter definition \"" + channelType + "\"");
            }
        }
    }

    for (const auto& [paramKey, paramValue] : parameters->GetParametersInt())
    {
        const auto pos = paramKey.find('_');
        const auto channelCausality = paramKey.substr(0, pos);
        const auto channelType = paramKey.substr(pos + 1);

        if (channelCausality == "Parameter")
        {
            if (!channelDefinitionParser.AddParameter(paramValue, channelType))
            {
                LOGERRORANDTHROW("Unable to parse parameter definition \"" + channelType + "\"");
            }
        }
    }

    for (const auto& [paramKey, paramValue] : parameters->GetParametersDouble())
    {
        const auto pos = paramKey.find('_');
        const auto channelCausality = paramKey.substr(0, pos);
        const auto channelType = paramKey.substr(pos + 1);

        if (channelCausality == "Parameter")
        {
            if (!channelDefinitionParser.AddParameter(paramValue, channelType))
            {
                LOGERRORANDTHROW("Unable to parse parameter definition \"" + channelType + "\"");
            }
        }
    }

    for (const auto& [paramKey, paramValue] : parameters->GetParametersBool())
    {
        const auto pos = paramKey.find('_');
        const auto channelCausality = paramKey.substr(0, pos);
        const auto channelType = paramKey.substr(pos + 1);

        if (channelCausality == "Parameter")
        {
            if (!channelDefinitionParser.AddParameter(paramValue, channelType))
            {
                LOGERRORANDTHROW("Unable to parse parameter definition \"" + channelType + "\"");
            }
        }
    }

    if (!channelDefinitionParser.ParseOutputSignalTypes())
    {
        LOGERRORANDTHROW("Output signal for FMU incomplete");
    }

    fmuOutputs = channelDefinitionParser.GetFmuOutputs();
    outputSignals = channelDefinitionParser.GetOutputSignals();
    fmuRealInputs = channelDefinitionParser.GetFmuRealInputs();
    fmuIntegerInputs = channelDefinitionParser.GetFmuIntegerInputs();
    fmuBooleanInputs = channelDefinitionParser.GetFmuBooleanInputs();
    fmuStringInputs = channelDefinitionParser.GetFmuStringInputs();
    fmuStringParameters = channelDefinitionParser.GetFmuStringParameters();
    fmuIntegerParameters = channelDefinitionParser.GetFmuIntegerParameters();
    fmuDoubleParameters = channelDefinitionParser.GetFmuDoubleParameters();
    fmuBoolParameters = channelDefinitionParser.GetFmuBoolParameters();
}

template <size_t FMI>
void FmuHandler<FMI>::ParseFmuParameters(const ParameterInterface *parameters)
{
    ParseFmuParametersByType(&ParameterInterface::GetParametersInt, parameters, &FmuHandler::fmuIntegerParameters, TypeContainer<int>{VariableType::Int});
    ParseFmuParametersByType(&ParameterInterface::GetParametersBool, parameters, &FmuHandler::fmuBoolParameters, TypeContainer<bool>{VariableType::Bool});
    ParseFmuParametersByType(&ParameterInterface::GetParametersDouble, parameters, &FmuHandler::fmuDoubleParameters, TypeContainer<double>{VariableType::Double});
    ParseFmuParametersByType(&ParameterInterface::GetParametersString, parameters, &FmuHandler::fmuStringParameters, TypeContainer<std::string>{VariableType::String});
}

template <size_t FMI>
template <typename GetParametersType, typename FmuTypeParameters, typename UnderlyingType>
void FmuHandler<FMI>::ParseFmuParametersByType(const GetParametersType getParametersType,
                                               const ParameterInterface *parameterInterface,
                                               const FmuTypeParameters fmuTypeParameters,
                                               const TypeContainer<UnderlyingType> typeContainer)
{
    for (const auto &[key, value] : std::invoke(getParametersType, parameterInterface))
    {
        if (key.substr(0, 10) != "Parameter_")
            continue;
        auto variableName = key.substr(10);
        UnderlyingType val = value;
        if (std::smatch transformationMatch; std::regex_search(variableName, transformationMatch, transformationRegex))
        {
            const std::string transformationType = transformationMatch[1];
            if (parameterTransformations.find(transformationType) == end(parameterTransformations))
                LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Unknown transformation type: " + transformationType);
            if (transformationType == "TransformList" && typeContainer.variableType != VariableType::String)
                LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "'TransformList' is only applicable to strings");
            const std::string transformationRule = transformationMatch[2];
            auto mappingEntry = parameterTransformationMappings.find(transformationRule);
            if (mappingEntry == end(parameterTransformationMappings))
                LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Unknown mapping rule: " + transformationRule);
            variableName = transformationMatch[3];

            //TODO: support mapping to different types
            auto mapping = std::any_cast<std::function<UnderlyingType(const UnderlyingType &)>>(mappingEntry->second);
            if (transformationType == "Transform")
            {
                val = mapping(value);
            }
            if constexpr (std::is_same_v<UnderlyingType, std::string>)
            {
                if (transformationType == "TransformList")
                {
                    std::string s = std::string(value);
                    size_t pos = 0;
                    std::string token;
                    std::vector<UnderlyingType> transformedList;
                    while ((pos = s.find(',')) != std::string::npos)
                    {
                        auto mappedValue = mapping(s.substr(0, pos));
                        transformedList.emplace_back(mappedValue);
                        s.erase(0, pos + 1);
                    }
                    transformedList.emplace_back(mapping(s));
                    val = std::accumulate(++cbegin(transformedList), cend(transformedList), *cbegin(transformedList), [](const std::string &left, const std::string &right) { return left + "," + right; });
                }
            }
        }
        const auto foundVariable = std::get<FMI>(fmuVariables).find(variableName);
        if (foundVariable == std::get<FMI>(fmuVariables).cend())
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "No variable with name \"" + variableName + "\" found in the FMU");
        if (foundVariable->second.variableType != typeContainer.variableType)
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Variable \"" + variableName + "\" has different type in FMU");
        std::get<FMI>(std::invoke(fmuTypeParameters, this)).emplace_back(val, foundVariable->second.valueReference);
    }
}

template <size_t FMI>
bool FmuHandler<FMI>::IsDefinedAsParameter(int desiredValRef, VariableType desiredType)
{
    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuStringParameters))
    {
        if(desiredValRef == valueReference && desiredType == VariableType::String)
        {
            return true;
        }
    }
    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuIntegerParameters))
    {
        if(desiredValRef == valueReference && desiredType == VariableType::Int || desiredType == VariableType::Enum)
        {
            return true;
        }
    }
    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuDoubleParameters))
    {
        if(desiredValRef == valueReference && desiredType == VariableType::Double)
        {
            return true;
        }
    }
    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuBoolParameters))
    {
        if(desiredValRef == valueReference && desiredType == VariableType::Bool)
        {
            return true;
        }
    }
    return false;
}

template <size_t FMI>
std::variant<FmuVariable1*, FmuVariable2*>* FmuHandler<FMI>::GetFmuVariable(int desiredValRef, VariableType desiredType)
{
    std::shared_ptr<std::variant<FmuVariable1*, FmuVariable2*>> returnVariable;

    for (auto& fmuVariable : std::get<FMI>(fmuVariables))
    {
        returnVariable = std::make_shared<std::variant<FmuVariable1*, FmuVariable2*>>(&fmuVariable.second);
        ValueReferenceAndType valRefAndType = fmuVariable.second.GetValueReferenceAndType();
        int valueRef = valRefAndType.first;
        auto type = valRefAndType.second;

        if(desiredValRef == valueRef && desiredType == type)
        {
            return returnVariable.get();
        }
    }
    return nullptr;
}

template <size_t FMI>
void FmuHandler<FMI>::SyncFmuVariablesAndParameters()
{
    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuStringParameters))
    {
        std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::String);
        if(fmuVariable)
        {
            ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
            int valueRef = valRefAndType.first;
            auto dataType = valRefAndType.second;

            if(dataType == VariableType::String)
            {
                fmuVariableValues.at(valRefAndType).stringValue = fmuParameterValue.data();
            }
        }
    }

    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuIntegerParameters))
    {
        std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Int);

        if(!fmuVariable)
        {
            std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Enum);
        }

        if(fmuVariable)
        {
            ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
            int valueRef = valRefAndType.first;
            auto dataType = valRefAndType.second;

            if(dataType == VariableType::Int || dataType == VariableType::Enum)
            {
                fmuVariableValues.at(valRefAndType).intValue = fmuParameterValue;
            }
        }
    }

    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuDoubleParameters))
    {
        std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Double);

        if(fmuVariable)
        {
            ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
            int valueRef = valRefAndType.first;
            auto dataType = valRefAndType.second;

            if(dataType == VariableType::Double)
            {
                fmuVariableValues.at(valRefAndType).realValue = fmuParameterValue;
            }
        }
    }

    for (const auto& [fmuParameterValue, valueReference] : std::get<FMI>(fmuBoolParameters))
    {
        std::variant<FmuVariable1*, FmuVariable2*>* fmuVariable = GetFmuVariable(valueReference, VariableType::Bool);

        if(fmuVariable)
        {
            ValueReferenceAndType valRefAndType = std::get<FMI>(*fmuVariable)->GetValueReferenceAndType();
            int valueRef = valRefAndType.first;
            auto dataType = valRefAndType.second;

            if(dataType == VariableType::Bool)
            {
                fmuVariableValues.at(valRefAndType).boolValue = fmuParameterValue;
            }
        }
    }
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorDataInput(const osi3::SensorData& data)
{
    std::swap(serializedSensorDataIn, previousSerializedSensorDataIn);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(sensorDataInVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(sensorDataInVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(sensorDataInVariable.value()+".size").valueReference;


    data.SerializeToString(&serializedSensorDataIn);
    FmuHelper::encode_pointer_to_integer<FMI>(serializedSensorDataIn.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);
    fmuInputValuesVec[2].emplace<FMI>(serializedSensorDataIn.length());

    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorViewInput(const osi3::SensorView& data)
{
    std::swap(serializedSensorView, previousSerializedSensorView);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(sensorViewVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(sensorViewVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(sensorViewVariable.value()+".size").valueReference;

    data.SerializeToString(&serializedSensorView);
    FmuHelper::encode_pointer_to_integer<FMI>(serializedSensorView.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);
    fmuInputValuesVec[2].emplace<FMI>(serializedSensorView.length());

    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorViewConfigRequest()
{
    int valueReferenceHi = std::get<FMI>(fmuVariables).at(sensorViewConfigRequestVariable.value()+".base.hi").valueReference;
    int valueReferenceLo = std::get<FMI>(fmuVariables).at(sensorViewConfigRequestVariable.value()+".base.lo").valueReference;
    int valueReferenceSize = std::get<FMI>(fmuVariables).at(sensorViewConfigRequestVariable.value()+".size").valueReference;

    fmi_integer_t fmiIntHi;
    fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
    fmi_integer_t fmiIntLo;
    fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
    int intSize;
    intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

    void *buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);
    const auto size = static_cast<std::string::size_type>(intSize);

    previousSerializedSensorViewConfigRequest = serializedSensorViewConfigRequest;

    serializedSensorViewConfigRequest = {static_cast<char *>(buffer), size};
    sensorViewConfigRequest.ParseFromString(serializedSensorViewConfigRequest);
}

template <size_t FMI>
void FmuHandler<FMI>::SetSensorViewConfig()
{
    // Apply requested config structure from FMU to sensorViewConfig in OpenPASS, which will be sent back to FMU
    serializedSensorViewConfig = serializedSensorViewConfigRequest;
    sensorViewConfig.ParseFromString(serializedSensorViewConfig);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(sensorViewConfigVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(sensorViewConfigVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(sensorViewConfigVariable.value()+".size").valueReference;

    FmuHelper::encode_pointer_to_integer<FMI>(serializedSensorViewConfig.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);
    fmuInputValuesVec[2].emplace<FMI>(serializedSensorViewConfig.length());


    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::GetSensorData()
{
    int valueReferenceHi = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value()+".base.hi").valueReference;
    int valueReferenceLo = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value()+".base.lo").valueReference;
    int valueReferenceSize = std::get<FMI>(fmuVariables).at(sensorDataOutVariable.value()+".size").valueReference;

    fmi_integer_t fmiIntHi;
    fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
    fmi_integer_t fmiIntLo;
    fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
    int intSize;
    intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

    void* buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

    if (enforceDoubleBuffering && buffer != nullptr && buffer == previousSensorDataOut)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
    }

    previousSensorDataOut = buffer;
    sensorDataOut.ParseFromArray(buffer, intSize);
}


#ifdef USE_EXTENDED_OSI
template <size_t FMI>
void FmuHandler<FMI>::SetVehicleCommunicationDataInput(const setlevel4to5::VehicleCommunicationData& data)
{
    std::swap(serializedVehicleCommunicationData, previousSerializedVehicleCommunicationData);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(vehicleCommunicationDataVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(vehicleCommunicationDataVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(vehicleCommunicationDataVariable.value()+".size").valueReference;

    data.SerializeToString(&serializedVehicleCommunicationData);
    FmuHelper::encode_pointer_to_integer<FMI>(serializedVehicleCommunicationData.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);
    fmuInputValuesVec[2].emplace<FMI>(serializedVehicleCommunicationData.length());

    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::GetMotionCommand()
{
    int valueReferenceHi = std::get<FMI>(fmuVariables).at(motionCommandVariable.value()+".base.hi").valueReference;
    int valueReferenceLo = std::get<FMI>(fmuVariables).at(motionCommandVariable.value()+".base.lo").valueReference;
    int valueReferenceSize = std::get<FMI>(fmuVariables).at(motionCommandVariable.value()+".size").valueReference;

    fmi_integer_t fmiIntHi;
    fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
    fmi_integer_t fmiIntLo;
    fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
    int intSize;
    intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

    void* buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

    if (enforceDoubleBuffering && buffer != nullptr && buffer == previousMotionCommand)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
    }

    previousMotionCommand = buffer;
    motionCommand.ParseFromArray(buffer, intSize);
}

template <size_t FMI>
void FmuHandler<FMI>::SetMotionCommandDataInput(const setlevel4to5::MotionCommand &data)
{
    std::swap(serializedMotionCommand, previousSerializedMotionCommand);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(motionCommandVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(motionCommandVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(motionCommandVariable.value()+".size").valueReference;

    data.SerializeToString(&serializedMotionCommand);
    FmuHelper::encode_pointer_to_integer<FMI>(serializedMotionCommand.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);
    fmuInputValuesVec[2].emplace<FMI>(serializedMotionCommand.length());


    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}
#endif

template <size_t FMI>
void FmuHandler<FMI>::GetTrafficUpdate()
{
    int valueReferenceHi = std::get<FMI>(fmuVariables).at(trafficUpdateVariable.value()+".base.hi").valueReference;
    int valueReferenceLo = std::get<FMI>(fmuVariables).at(trafficUpdateVariable.value()+".base.lo").valueReference;
    int valueReferenceSize = std::get<FMI>(fmuVariables).at(trafficUpdateVariable.value()+".size").valueReference;

    fmi_integer_t fmiIntHi;
    fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
    fmi_integer_t fmiIntLo;
    fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
    int intSize;
    intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

    void *buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

    if (enforceDoubleBuffering && buffer != nullptr && buffer == previousTrafficUpdate)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
    }

    previousTrafficUpdate = buffer;
    trafficUpdate.ParseFromArray(buffer, intSize);
    trafficUpdate.SerializeToString(&serializedTrafficUpdate);
}

template <size_t FMI>
void FmuHandler<FMI>::GetHostVehicleData()
{
    int valueReferenceHi = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value()+".base.hi").valueReference;
    int valueReferenceLo = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value()+".base.lo").valueReference;
    int valueReferenceSize = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value()+".size").valueReference;

    fmi_integer_t fmiIntHi;
    fmiIntHi.emplace<FMI>(GetValue(valueReferenceHi, VariableType::Int).intValue);
    fmi_integer_t fmiIntLo;
    fmiIntLo.emplace<FMI>(GetValue(valueReferenceLo, VariableType::Int).intValue);
    int intSize;
    intSize = GetValue(valueReferenceSize, VariableType::Int).intValue;

    void *buffer = FmuHelper::decode_integer_to_pointer<FMI>(fmiIntHi, fmiIntLo);

    if (enforceDoubleBuffering && buffer != nullptr && buffer == previousHostVehicleData)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "FMU has no double buffering");
    }

    previousHostVehicleData = buffer;
    hostVehicleData.ParseFromArray(buffer, intSize);
    hostVehicleData.SerializeToString(&serializedHostVehicleData);
}

template <size_t FMI>
void FmuHandler<FMI>::SetTrafficCommandInput(const osi3::TrafficCommand& data)
{
    std::swap(serializedTrafficCommand, previousSerializedTrafficCommand);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(trafficCommandVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(trafficCommandVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(trafficCommandVariable.value()+".size").valueReference;

    data.SerializeToString(&serializedTrafficCommand);

    FmuHelper::encode_pointer_to_integer<FMI>(serializedTrafficCommand.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);

    auto length = serializedTrafficCommand.length();

    if(FMI == FMI1)
    {
        if (length > std::numeric_limits<fmi1_integer_t>::max())
        {
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
        }
    }
    else if(FMI == FMI2)
    {
        if (length > std::numeric_limits<fmi2_integer_t>::max())
        {
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
        }
    }

    fmuInputValuesVec[2].emplace<FMI>(length);

    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetHostVehicleDataInput(const osi3::HostVehicleData &data)
{
    std::swap(serializedHostVehicleData, previousSerializedHostVehicleData);

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(hostVehicleDataVariable.value()+".size").valueReference;

    data.SerializeToString(&serializedHostVehicleData);

    FmuHelper::encode_pointer_to_integer<FMI>(serializedHostVehicleData.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);

    auto length = serializedHostVehicleData.length();

    if(FMI == FMI1)
    {
        if (length > std::numeric_limits<fmi1_integer_t>::max())
        {
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
        }
    }
    else if(FMI == FMI2)
    {
        if (length > std::numeric_limits<fmi2_integer_t>::max())
        {
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + "Serialized buffer length of osi::TrafficCommand exceeds fmi integer size");
        }
    }

    fmuInputValuesVec[2].emplace<FMI>(length);

    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}

template <size_t FMI>
void FmuHandler<FMI>::SetGroundTruth()
{
    auto* worldData = static_cast<OWL::Interfaces::WorldData*>(world->GetWorldData());
    groundTruth = worldData->GetOsiGroundTruth();

    std::vector<int> valueReferencesVec;
    std::vector<fmi_integer_t> fmuInputValuesVec;

    valueReferencesVec.resize(3);
    fmuInputValuesVec.resize(3);

    valueReferencesVec[0] = std::get<FMI>(fmuVariables).at(groundTruthVariable.value()+".base.lo").valueReference;
    valueReferencesVec[1] = std::get<FMI>(fmuVariables).at(groundTruthVariable.value()+".base.hi").valueReference;
    valueReferencesVec[2] = std::get<FMI>(fmuVariables).at(groundTruthVariable.value()+".size").valueReference;


    groundTruth.SerializeToString(&serializedGroundTruth);
    FmuHelper::encode_pointer_to_integer<FMI>(serializedGroundTruth.data(),
                                              fmuInputValuesVec[1],
                                              fmuInputValuesVec[0]);
    fmuInputValuesVec[2].emplace<FMI>(serializedGroundTruth.length());

    fmuCommunications->SetIntegerFMIFromIntRef(valueReferencesVec, fmuInputValuesVec, 3);
}


template <size_t FMI>
FmuValue& FmuHandler<FMI>::GetFmuSignalValue(SignalValue signalValue, VariableType variableType)
{
    int valueReference = std::get<FMI>(fmuOutputs).at(signalValue);
    ValueReferenceAndType valueReferenceAndType;
    valueReferenceAndType.first = valueReference;
    valueReferenceAndType.second = variableType;
    return fmuVariableValues.at(valueReferenceAndType);
}

template <size_t FMI>
FmuValue& FmuHandler<FMI>::GetValue(int valueReference, VariableType variableType) const
{
    ValueReferenceAndType valueReferenceAndType;
    valueReferenceAndType.first = valueReference;
    valueReferenceAndType.second = variableType;
    return fmuVariableValues.at(valueReferenceAndType);
}

template <size_t FMI>
void FmuHandler<FMI>::WriteValues()
{
    for (auto [fmuVarName, fmuVariable] : std::get<FMI>(fmuVariables))
    {
        if(!isInitialized && fmuVariable.WriteAtInit() ||
            isInitialized && fmuVariable.WriteAtTrigger())
        {
            ValueReferenceAndType valRefAndType = fmuVariable.GetValueReferenceAndType();
            int valueRef = valRefAndType.first;
            auto dataType = valRefAndType.second;

            //TODO: Remove this tmp fix for parameter overriding as soon as possible
            if(fmuVariable.IsParameter() && !IsDefinedAsParameter(valueRef, dataType))
            {
                LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + "'" + fmuVarName + "': Not Written because not defined as parameter");
                continue;
            }

            if(fmuVariableValues.count(valRefAndType) <= 0)
                continue;
            FmuValue value = fmuVariableValues.at(valRefAndType);
            if(dataType == VariableType::String)
            {
                value.stringValue = ((std::string)(fmuVariableValues.at(valRefAndType).stringValue)).data();
            }

            SetFmuValue(valueRef, value, dataType);
            LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + "" + FmuHelper::GenerateString("write", fmuVarName, dataType, value));
        }
        else
        {
            LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) +"'"
                     + fmuVarName + "': Not written (variablity: " + FmuHelper::VariabilityToStringMap(fmuVariable.variability) +
                     ", causality: " + FmuHelper::CausalityToStringMap(fmuVariable.causality) + ")");
        }
    }
}

template <size_t FMI>
void FmuHandler<FMI>::ReadValues()
{
    for (auto [fmuVarName, fmuVariable] : std::get<FMI>(fmuVariables))
    {
        if(!isInitialized && fmuVariable.ReadAtInit() ||
           isInitialized && fmuVariable.ReadAtTrigger())
        {
            ValueReferenceAndType valRefAndType = fmuVariable.GetValueReferenceAndType();
            int valueRef = valRefAndType.first;
            auto dataType = valRefAndType.second;

            fmi_t dataOut;
            auto& value = fmuVariableValues[valRefAndType];
            GetFmuValue(valueRef, value, dataType);
            LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) + "" + FmuHelper::GenerateString("read", fmuVarName, dataType, value));

        }
        else
        {
            LOGDEBUG(FmuHelper::log_prefix(this->agentIdString, this->componentName) +"'"
                     + fmuVarName + "': Not read (variablity: " + FmuHelper::VariabilityToStringMap(fmuVariable.variability) +
                     ", causality: " + FmuHelper::CausalityToStringMap(fmuVariable.causality) + ")");
        }
    }
}

template <size_t FMI>
void FmuHandler<FMI>::SetFmuValue(int valueReference, FmuValue fmuValueIn, VariableType dataType)
{
    std::vector<int> valueReferences;
    valueReferences.resize(1);
    valueReferences[0] = valueReference;
    std::vector<FmuValue> fmuValuesIn;
    fmuValuesIn.resize(1);
    fmuValuesIn[0] = fmuValueIn;
    fmuCommunications->SetFMI(valueReferences, fmuValuesIn, 1, dataType);
}

template <size_t FMI>
void FmuHandler<FMI>::SetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue> fmuValuesIn, VariableType dataType)
{
    /*
    if(!valueReferences.empty())
        fmuCommunications->SetFMI(valueReferences, fmuValuesIn, valueReferences.size(), dataType);
    */
    // TODO The above code has issues in prestep method (storage allocation issue). Fix the issue and use the code above instead if the one below
    for(int i = 0; i < valueReferences.size(); i++)
    {
        SetFmuValue(valueReferences[i], fmuValuesIn[i], dataType);
    }
}

template <size_t FMI>
void FmuHandler<FMI>::GetFmuValue(int valueReference, FmuValue& fmuValueOut, VariableType dataType)
{
    std::vector<int> valueReferences;
    valueReferences.resize(1);
    valueReferences[0] = valueReference;
    std::vector<FmuValue> fmuValuesOut;
    fmuValuesOut.resize(1);
    fmuCommunications->GetFMI(valueReferences, fmuValuesOut, 1, dataType);
    fmuValueOut = fmuValuesOut[0];
}

template <size_t FMI>
void FmuHandler<FMI>::GetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue>& fmuValuesOut, VariableType dataType)
{
    fmuCommunications->GetFMI(valueReferences, fmuValuesOut, valueReferences.size(), dataType);
}

template <size_t FMI>
void FmuHandler<FMI>::HandleFmiStatus(jm_status_enu_t status, std::string logPrefix)
{
    return fmuCommunications->HandleFmiStatus(status, logPrefix);
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::PrepareFmuInit()
{
    return fmuCommunications->PrepareFmuInit();
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::FmiEndHandling()
{
    return fmuCommunications->FmiEndHandling();
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::FmiSimulateStep(double time)
{
    return fmuCommunications->FmiSimulateStep(time);
}

template <size_t FMI>
jm_status_enu_t FmuHandler<FMI>::FmiPrepSimulate()
{
    return fmuCommunications->FmiPrepSimulate();
}