/*******************************************************************************
* Copyright (c) 2021, 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#pragma once

#include <google/protobuf/message.h>
#include <memory>
#include <osi3/osi_trafficcommand.pb.h>

#include "include/agentInterface.h"
#include "include/signalInterface.h"
#include "include/callbackInterface.h"
#include "common/openScenarioDefinitions.h"

void AddTrafficCommandActionFromOpenScenarioTrajectory(osi3::TrafficAction *trafficAction, const openScenario::Trajectory &trajectory);

void AddTrafficCommandActionFromOpenScenarioPosition(osi3::TrafficAction *trafficAction,
                                                     const openScenario::Position &position,
                                                     WorldInterface *const worldInterface,
                                                     const std::function<void(const std::string &)> &errorCallback);


/// @brief Structure representing a translator for all input signals
struct InputSignalTranslator
{
    WorldInterface& world;                      ///< Reference to the World interface
    AgentInterface& agent;                      ///< Reference to the agent interface
    const CallbackInterface& callbackInterface; ///< Reference to the callback interface

    /// @brief Method to initialize a input signal translator
    /// @param world                Reference to the World interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the callback interface
    InputSignalTranslator(WorldInterface& world, AgentInterface& agent, const CallbackInterface& callbackInterface);

    /// @brief Function to translate a signal message to a protobuf message
    /// @param message Signal message
    /// @return Translated protobuf message
    virtual const google::protobuf::Message * translate(std::shared_ptr<const SignalInterface>, const google::protobuf::Message *const message) = 0;
};

/// @brief Structure representing a translator for all output signals
struct OutputSignalTranslator
{
    WorldInterface& world;                      ///< Reference to the World interface
    AgentInterface& agent;                      ///< Reference to the agent interface
    const CallbackInterface& callbackInterface; ///< Reference to the callback interface

    /// @brief Method to initialize a input signal translator
    /// @param world                Reference to the World interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the callback interface
    OutputSignalTranslator(WorldInterface& world, AgentInterface& agent, const CallbackInterface& callbackInterface);

    /// @brief Translate a protobuf message to the signal message
    /// @return Signal message
    virtual std::shared_ptr<const SignalInterface> translate(const google::protobuf::Message *const) = 0;
};

/// @brief Structure representing a factory of translator for all input signals 
struct InputSignalTranslatorFactory
{
    /// @brief Method to build an appropriate signal message for the given local link
    /// @param localLinkId          Local link id value
    /// @param world                Reference to the World interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the callback interface
    /// @return A pointer to the corresponding input signal translator
    [[nodiscard]] static std::optional<std::shared_ptr<InputSignalTranslator>> build(int localLinkId,
                                                                                     WorldInterface& world,
                                                                                     AgentInterface& agent,
                                                                                     const CallbackInterface& callbackInterface);
};

/// @brief Structure representing a factory of translators for all output signals
struct OutputSignalTranslatorFactory
{

    /// @brief Method to build an appropriate signal message for the given local link
    /// @param localLinkId          Local link id value
    /// @param world                Reference to the World interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the callback interface
    /// @return A pointer to the corresponding output signal translator
    [[nodiscard]] static std::optional<std::shared_ptr<OutputSignalTranslator>> build(int localLinkId,
                                                                                      WorldInterface& world,
                                                                                      AgentInterface& agent,
                                                                                      const CallbackInterface& callbackInterface);
};

