/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*               2019-2021 in-tech GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/

#pragma once

#include <memory>
#include <qglobal.h>
#include <string>
#include <sstream>
#include <cstdint>
#include <osi3/osi_trafficcommand.pb.h>

#include "variant_visitor.h"

#include "common/commonTools.h"
#include "common/accelerationSignal.h"
#include "common/longitudinalSignal.h"
#include "common/steeringSignal.h"
#include "common/dynamicsSignal.h"
#include "common/sensorDataSignal.h"
#include "common/osiUtils.h"
#include "common/speedActionSignal.h"
#include "common/stringSignal.h"
#include "common/trajectorySignal.h"

#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"
#include "include/fmuHandlerInterface.h"

#include "core/opSimulation/modules/World_OSI/WorldData.h"

#include "google/protobuf/util/json_util.h"

#include "osi3/osi_trafficupdate.pb.h"

extern "C"
{
#include "fmilib.h"
#include "fmuChecker.h"
}

/*
struct fmi1String{ fmi1_string_t string; };
struct fmi2String{ fmi2_string_t string; };
struct fmi1Real{ fmi1_real_t real; };
struct fmi2Real{ fmi2_real_t real; };
struct fmi1Integer{ fmi1_integer_t integer; };
struct fmi2Integer{ fmi2_integer_t integer; };
struct fmi1Boolean{ fmi1_boolean_t boolean; };
struct fmi2Boolean{ fmi2_boolean_t boolean; };
using fmi_string_t_s = std::variant<fmi1String, fmi2String>;
using fmi_real_t_s = std::variant<fmi1Real, fmi2Real>;
using fmi_integer_t_s = std::variant<fmi1Integer, fmi2Integer>;
using fmi_boolean_t_s = std::variant<fmi1Boolean, fmi2Boolean>;
*/

using fmi_string_t = std::variant<fmi1_string_t, fmi2_string_t>;
using fmi_real_t = std::variant<fmi1_real_t, fmi2_real_t>;
using fmi_integer_t = std::variant<fmi1_integer_t, fmi2_integer_t>;
using fmi_boolean_t = std::variant<fmi1_boolean_t, fmi2_boolean_t>;
using fmi_string_t_data = std::variant<fmi1_string_t*, fmi2_string_t*>;
using fmi_real_t_data = std::variant<fmi1_real_t*, fmi2_real_t*>;
using fmi_integer_t_data = std::variant<fmi1_integer_t*, fmi2_integer_t*>;
using fmi_boolean_t_data = std::variant<fmi1_boolean_t*, fmi2_boolean_t*>;

using fmi_t = std::variant<fmi_string_t, fmi_real_t, fmi_integer_t, fmi_boolean_t>;
using fmi_t_data = std::variant<std::vector<fmi_string_t>, std::vector<fmi_real_t>, std::vector<fmi_integer_t>, std::vector<fmi_boolean_t>>;
//using fmi_t_data = std::variant<fmi_string_t, fmi_real_t, fmi_integer_t, fmi_boolean_t>;

/// Structure representing an object containing information on sensor fusion
struct SensorFusionObjectInfo
{
    int id{-1};                 ///< ID f the sensor fusion object
    int numberOfSensors{0};     ///< Number of sensors
    double t{0};                ///< T positoin
    double t_left{0};           ///< T left position      
    double t_right{0};          ///< T right position
    double s{0};                ///< s coordinate
    double net_s{0};            ///< Net s coordinate of all sensors
    double net_x{0};            ///< Net x coordinate
    double net_y{0};            ///< Net y coordinate
    int lane{0};                ///< Lane
    double velocity{0};         ///< Velocity of the agent
    double velocity_x{0};       ///< Velocity of the agent in x coordiinate
    double velocity_y{0};       ///< Velocity of the agent in y coordinate
    double yaw{0};              ///< Yaw angle of the agent
};

/// @brief Class representing an FMU helper
class FmuHelper
{
public:

    /// @brief TODO
    /// @param agentIdString 
    /// @return 
    static std::string log_prefix(const std::string &agentIdString);
    
    /// @brief TODO
    /// @param agentIdString 
    /// @param componentName 
    /// @return 
    static std::string log_prefix(const std::string &agentIdString, const std::string &componentName);

    /// @brief Add a traffic command action from open scenario position
    /// @param trafficAction    Pointer to OSI traffic action
    /// @param position         Openscenario position
    /// @param worldInterface   Pointer to the world interface
    /// @param errorCallback    Function of error callback
    static void AddTrafficCommandActionFromOpenScenarioPosition(osi3::TrafficAction *trafficAction,
                                                                          const openScenario::Position &position,
                                                                          WorldInterface *const worldInterface,
                                                                          const std::function<void(const std::string &)> &errorCallback);

    /// @brief Add an traffic command action from a open scenario trajectory
    /// @param trafficAction    Pointer to OSI traffic action
    /// @param trajectory       Open scenario trajectory
    static void AddTrafficCommandActionFromOpenScenarioTrajectory(osi3::TrafficAction *trafficAction, const openScenario::Trajectory& trajectory);

    /// @brief Function to decode integer to pointers
    /// @tparam FMI 
    /// @param hi 
    /// @param lo 
    /// @return 
    template <size_t FMI>
    static void* decode_integer_to_pointer(fmi_integer_t hi, fmi_integer_t lo);
    
    /// @brief Function to encode pointer to integer
    /// @tparam FMI 
    /// @param ptr 
    /// @param hi 
    /// @param lo 
    template <size_t FMI>
    static void encode_pointer_to_integer(const void* ptr,fmi_integer_t& hi,fmi_integer_t& lo);

    /// @brief Function to append message
    /// @param appendedMessage message to append
    /// @param message         existing message
    static void AppendMessages(std::string& appendedMessage, std::string& message);
    
    /// @brief Function to convert an integer to bytes
    /// @param paramInt Integer value
    /// @return list of converted bytes
    static std::vector<unsigned char> intToBytes(int paramInt);
    
    /// @brief Generate a default sensor view configuration
    /// @return Default OSI sensor view configuration
    static osi3::SensorViewConfiguration GenerateDefaultSensorViewConfiguration();

    /// @brief Function to get references from list of FMI value reference
    /// @tparam FMI 
    /// @param valueReferencesVec   List of references of the value
    /// @param s                    Size
    /// @return FMI value reference
    template <size_t FMI>
    static fmi_value_references_t GetReferencesFromVector(std::vector<fmi_value_reference_t> valueReferencesVec, size_t s);

    /// @brief Function to get references from integer vector
    /// @tparam FMI 
    /// @param valueReferencesVec   List of references of the value
    /// @param size                 Size
    /// @return FMI value reference
    template <size_t FMI>
    static fmi_value_references_t GetReferencesFromIntVector(std::vector<int> valueReferencesVec, size_t size);

    /// @brief FUnction to convert FMI data from string
    /// @tparam FMI 
    /// @param dataVec  list of fmi string
    /// @param size     size of the FMI library
    /// @return FMI integer
    template <size_t FMI>
    static fmi_string_t_data FmiDataFromString(std::vector<fmi_string_t> dataVec, size_t size);
    
    /// @brief FUnction to convert FMI data from real
    /// @tparam FMI 
    /// @param dataVec  list of fmi real
    /// @param size     size of the FMI library
    /// @return FMI integer
    template <size_t FMI>
    static fmi_real_t_data FmiDataFromReal(std::vector<fmi_real_t> dataVec, size_t size);
    
    /// @brief FUnction to convert FMI data from integer
    /// @tparam FMI 
    /// @param dataVec  list of fmi boolean
    /// @param size     size of the FMI library
    /// @return FMI integer
    template <size_t FMI>
    static fmi_integer_t_data FmiDataFromInteger(std::vector<fmi_integer_t> dataVec, size_t size);
    
    /// @brief FUnction to convert from boolean to FMI data 
    /// @tparam FMI 
    /// @param dataVec  list of fmi boolean
    /// @param size     size of the FMI library
    /// @return FMI data
    template <size_t FMI>
    static fmi_boolean_t_data FmiDataFromBoolean(std::vector<fmi_boolean_t> dataVec, size_t size);

    /// @brief Function to generate a string 
    /// @param operation    Operation
    /// @param name         Name
    /// @param datatype     
    /// @param value 
    /// @return 
    static std::string GenerateString(std::string operation, std::string name, VariableType datatype, FmuValue value);

    /// @brief Function to convert a given datatype to the string
    /// @param datatype Given variable data type
    /// @return data type in string
    static std::string VariableTypeToStringMap(VariableType datatype)
    {
        static std::map<VariableType, std::string> s
                {
                        {VariableType::Bool, "Bool"},
                        {VariableType::Int, "Int"},
                        {VariableType::Double, "Double"},
                        {VariableType::String, "String"},
                        {VariableType::Enum, "Enum"}
                };

        return s[datatype];
    };

    /// @brief Function to convert the given cauality to string
    /// @param causality Causality of FMI variable
    /// @return string of the given causality
    static std::string CausalityToStringMap(fmi_causality_enu_t causality)
    {
        static std::map<fmi_causality_enu_t , std::string> s
                {
                        {fmi1_causality_enu_input, "input"},
                        {fmi1_causality_enu_output, "output"},
                        {fmi2_causality_enu_input, "output"},
                        {fmi2_causality_enu_output, "output"},
                        {fmi2_causality_enu_parameter, "parameter"},
                        {fmi2_causality_enu_calculated_parameter, "calculated parameter"}
                };

        return s[causality];
    };

    /// @brief Function to convet fmi variability to string
    /// @param variability FMU variablities
    /// @return string of the given variablity
    static std::string VariabilityToStringMap(fmi_variability_enu_t variability)
    {
        static std::map<fmi_variability_enu_t , std::string> s
                {
                        {fmi1_variability_enu_discrete, "discrete"},
                        {fmi1_variability_enu_parameter, "fixed"},
                        {fmi1_variability_enu_continuous, "continuous"},
                        {fmi1_variability_enu_constant, "constant"},
                        {fmi1_variability_enu_unknown, "unknown"},

                        {fmi2_variability_enu_discrete, "discrete"},
                        {fmi2_variability_enu_fixed, "fixed"},
                        {fmi2_variability_enu_continuous, "continuous"},
                        {fmi2_variability_enu_constant, "constant"},
                        {fmi2_variability_enu_tunable, "tunable"},
                        {fmi2_variability_enu_unknown, "unknown"},
                };

        return s[variability];
    };

};

template <size_t FMI>
fmi_value_references_t FmuHelper::GetReferencesFromVector(std::vector<fmi_value_reference_t> valueReferencesVec, size_t size)
{
    fmi_value_references_t valueReferences;

    if(FMI == FMI1)
        valueReferences.emplace<FMI>(new fmi1_value_reference_t[size]);
    else if(FMI == FMI2)
        valueReferences.emplace<FMI>(new fmi2_value_reference_t[size]);

    for(int i = 0; i < size; i++)
    {
        std::get<FMI>(valueReferences)[i] = std::get<FMI>(valueReferencesVec[i]);
    }
    return valueReferences;
}

template <size_t FMI>
fmi_value_references_t FmuHelper::GetReferencesFromIntVector(std::vector<int> valueReferencesVec, size_t size)
{
    fmi_value_references_t valueReferences;

    if(FMI == FMI1)
        valueReferences.emplace<FMI>(new fmi1_value_reference_t[size]);
    else if(FMI == FMI2)
        valueReferences.emplace<FMI>(new fmi2_value_reference_t[size]);

    for(int i = 0; i < size; i++)
    {
        std::get<FMI>(valueReferences)[i] = valueReferencesVec[i];
    }
    return valueReferences;
}

template <size_t FMI>
fmi_string_t_data FmuHelper::FmiDataFromString(std::vector<fmi_string_t> dataVec, size_t size)
{
    fmi_string_t_data data;

    if(FMI == FMI1)
        data.emplace<FMI>(new fmi1_string_t[size]);
    else if(FMI == FMI2)
        data.emplace<FMI>(new fmi2_string_t[size]);

    for(int i = 0; i < size; i++)
    {
        std::get<FMI>(data)[i] = std::get<FMI>(dataVec[i]);
    }
    return data;
}
template <size_t FMI>
fmi_real_t_data FmuHelper::FmiDataFromReal(std::vector<fmi_real_t> dataVec, size_t size)
{
    fmi_real_t_data data;

    if(FMI == FMI1)
        data.emplace<FMI>(new fmi1_real_t[size]);
    else if(FMI == FMI2)
        data.emplace<FMI>(new fmi2_real_t[size]);

    for(int i = 0; i < size; i++)
    {
        std::get<FMI>(data)[i] = std::get<FMI>(dataVec[i]);
    }
    return data;
}
template <size_t FMI>
fmi_integer_t_data FmuHelper::FmiDataFromInteger(std::vector<fmi_integer_t> dataVec, size_t size)
{
    fmi_integer_t_data data;

    if(FMI == FMI1)
        data.emplace<FMI>(new fmi1_integer_t[size]);
    else if(FMI == FMI2)
        data.emplace<FMI>(new fmi2_integer_t[size]);

    for(int i = 0; i < size; i++)
    {
        std::get<FMI>(data)[i] = std::get<FMI>(dataVec[i]);
    }
    return data;
}
template <size_t FMI>
fmi_boolean_t_data FmuHelper::FmiDataFromBoolean(std::vector<fmi_boolean_t> dataVec, size_t size)
{
    fmi_boolean_t_data data;

    if(FMI == FMI1)
        data = new fmi1_boolean_t[size];
    else if(FMI == FMI2)
        data = new fmi2_boolean_t[size];

    for(int i = 0; i < size; i++)
    {
        std::get<FMI>(data)[i] = std::get<FMI>(dataVec[i]);
    }
    return data;
}

template <size_t FMI>
void FmuHelper::encode_pointer_to_integer(const void* ptr,fmi_integer_t& hi,fmi_integer_t& lo)
{
#if PTRDIFF_MAX == INT64_MAX
    union addrconv {
        struct {
            int lo;
            int hi;
        } base;
        unsigned long long address;
    } myaddr;
    myaddr.address=reinterpret_cast<unsigned long long>(ptr);
    hi.emplace<FMI>(myaddr.base.hi);
    lo.emplace<FMI>(myaddr.base.lo);
#elif PTRDIFF_MAX == INT32_MAX
    hi.emplace<FMI>(0);
    lo.emplace<FMI>(reinterpret_cast<int>(ptr));
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}

template <size_t FMI>
void* FmuHelper::decode_integer_to_pointer(fmi_integer_t hi, fmi_integer_t lo)
{
#if PTRDIFF_MAX == INT64_MAX
    union addrconv {
        struct {
            int lo;
            int hi;
        } base;
        unsigned long long address;
    } myaddr;
    myaddr.base.lo=std::get<FMI>(lo);
    myaddr.base.hi=std::get<FMI>(hi);
    return reinterpret_cast<void*>(myaddr.address);
#elif PTRDIFF_MAX == INT32_MAX
    return reinterpret_cast<void*>(get<FMI>(lo));
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}
