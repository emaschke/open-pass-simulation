/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/

#pragma once

#include <cstddef>
#include <vector>
#include "include/fmuHandlerInterface.h"
#include "FmuHelper.h"

template <size_t FMI>
class FmuCommunication  ///< Class representing a FMU communication
{
public:

    /// @brief Constructor for FMU Communication with FMI size type
    /// @param componentName        Name of the component
    /// @param cdata                FMU check data
    /// @param fmuVariableValues    Map of FMU value reference type and its value
    /// @param parameters           Reference to the parameter interface
    /// @param world                Pointer to the world interface
    /// @param agent                Pointer to the agent interface
    /// @param callbacks            Pointer to the callbacks interface
    FmuCommunication<FMI>(std::string componentName,
                          fmu_check_data_t& cdata,
                          std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                          const ParameterInterface *parameters,
                          WorldInterface *world,
                          AgentInterface *agent,
                          const CallbackInterface *callbacks);

    /// @brief Function to set FMI
    /// @param valueReferencesVec   List of value referemces
    /// @param fmuValuesIn          List of input fmu values
    /// @param size                 size of the FMI
    /// @param dataType             Type of the variable
    void SetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue> fmuValuesIn, size_t size, VariableType dataType);
    
    /// @brief Function to get GMI
    /// @param valueReferencesVec   List of value referemces
    /// @param fmuValuesOut         List of input fmu values
    /// @param size                 size of the FMI
    /// @param dataType             Type of the variable
    void GetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue>& fmuValuesOut, size_t size, VariableType dataType);

    /// @brief Set string FMI from integer reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetStringFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_string_t> dataVecIn, size_t size);
    
    /// @brief Set real FMI from integer reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetRealFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_real_t> dataVecIn, size_t size);

    /// @brief Set integer FMI from integer reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetIntegerFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_integer_t> dataVecIn, size_t size);
    
    /// @brief Set boolean FMI from integer reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetBooleanFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_boolean_t> dataVecIn, size_t size);

    /// @brief Set string FMI
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetStringFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_string_t> dataVecIn, size_t size);

    /// @brief Set real FMI
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetRealFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_real_t> dataVecIn, size_t size);

    /// @brief Set integer FMI
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetIntegerFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_integer_t> dataVecIn, size_t size);

    /// @brief Set boolean FMI
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecIn            list of fmi strings
    /// @param size                 size of the FMI
    void SetBooleanFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_boolean_t> dataVecIn, size_t size);

    /// @brief Function to get string FMI from int reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetStringFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_string_t>& dataVecOut, size_t size);

    /// @brief Function to get real FMI from int reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetRealFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_real_t>& dataVecOut, size_t size);
    
    /// @brief Function to get integer FMI from int reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetIntegerFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_integer_t>& dataVecOut, size_t size);
    
    /// @brief Function to get boolean FMI from int reference
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetBooleanFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_boolean_t>& dataVecOut, size_t size);

    /// @brief Function to get string fmi
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetStringFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_string_t>& dataVecOut, size_t size);
    
    /// @brief Function to get real fmi
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetRealFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_real_t>& dataVecOut, size_t size);
    
    /// @brief Function to integer fmi
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetIntegerFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_integer_t>& dataVecOut, size_t size);
    
    /// @brief Function to boolean fmi
    /// @param valueReferencesVec   List of value referemces
    /// @param dataVecOut           List of input fmu values
    /// @param size                 size of the FMI
    void GetBooleanFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_boolean_t>& dataVecOut, size_t size);

    /*!
    * \brief Calls a FMIL function and handles the return status.
    *
    * \param[in]   fmiStatus   FMI Status
    * \param[in]   logPrefix   Prefix to prepend to log messages
    *
    * \note Calls \c fmi1_end_handling with \c cdata as argument on FMI error.
    */
    void HandleFmiStatus(const jm_status_enu_t &fmiStatus, const std::string &logPrefix);

    /// @return TODO
    jm_status_enu_t PrepareFmuInit();

    /// @return TODO
    jm_status_enu_t FmiEndHandling();

    /// @brief TODO
    /// @param time 
    /// @return 
    jm_status_enu_t FmiSimulateStep(double time);

    /// @return TODO
    jm_status_enu_t FmiPrepSimulate();

    /*!
    * \return Retrieve available variables from FMU
    */
    FmuVariables GetFmuVariables();

    /// @brief Log the information of FMI communication
    /// @param logLevel Level of log
    /// @param file     File to log
    /// @param line     Line
    /// @param message  Message to log
    void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message);

    const CallbackInterface* callbacks;     ///< Pointer to the callback interface
private:
    const std::string agentIdString;
    struct fmu_check_data_t& cdata;
    std::string componentName;

    fmi_status_t SetStringFMI1(fmi1_value_reference_t valueReferences[], fmi1_string_t inData[], size_t size);
    fmi_status_t SetStringFMI2(fmi2_value_reference_t valueReferences[], fmi2_string_t inData[], size_t size);
    fmi_status_t SetRealFMI1(fmi1_value_reference_t valueReferences[], fmi1_real_t inData[], size_t size);
    fmi_status_t SetRealFMI2(fmi2_value_reference_t valueReferences[], fmi2_real_t inData[], size_t size);
    fmi_status_t SetIntegerFMI1(fmi1_value_reference_t valueReferences[], fmi1_integer_t inData[], size_t size);
    fmi_status_t SetIntegerFMI2(fmi2_value_reference_t valueReferences[], fmi2_integer_t inData[], size_t size);
    fmi_status_t SetBooleanFMI1(fmi1_value_reference_t valueReferences[], fmi1_boolean_t inData[], size_t size);
    fmi_status_t SetBooleanFMI2(fmi2_value_reference_t valueReferences[], fmi2_boolean_t inData[], size_t size);

    fmi_status_t GetStringFMI1(fmi1_value_reference_t valueReferences[], fmi1_string_t outData[], size_t size);
    fmi_status_t GetStringFMI2(fmi2_value_reference_t valueReferences[], fmi2_string_t outData[], size_t size);
    fmi_status_t GetRealFMI1(fmi1_value_reference_t valueReferences[], fmi1_real_t outData[], size_t size);
    fmi_status_t GetRealFMI2(fmi2_value_reference_t valueReferences[], fmi2_real_t outData[], size_t size);
    fmi_status_t GetIntegerFMI1(fmi1_value_reference_t valueReferences[], fmi1_integer_t outData[], size_t size);
    fmi_status_t GetIntegerFMI2(fmi2_value_reference_t valueReferences[], fmi2_integer_t outData[], size_t size);
    fmi_status_t GetBooleanFMI1(fmi1_value_reference_t valueReferences[], fmi1_boolean_t outData[], size_t size);
    fmi_status_t GetBooleanFMI2(fmi2_value_reference_t valueReferences[], fmi2_boolean_t outData[], size_t size);

    /*!
    * \brief Converts the FMU variable type to a unique C++ type id.
    *
    * \param[in]   fmiType     FMILibrary specific type specifier
    *
    * \return  Hash code of the associated C++ type id
     */
    VariableType FmiTypeToCType(const fmi1_base_type_enu_t fmiType);
    VariableType FmiTypeToCType(const fmi2_base_type_enu_t fmiType);

};

#include "FmuCommunication.tpp"