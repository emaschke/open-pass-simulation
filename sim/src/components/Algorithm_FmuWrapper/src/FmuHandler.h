/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*               2019-2021 in-tech GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/

#pragma once

#include <map>
#include <memory>
#include <unordered_map>
#include <set>
#include <QString>
#include <any>
#include <regex>
#include <fstream>
#include <filesystem>
#include <cmath>
#include <QDir>

#include "ChannelDefinitionParser.h"
#include "fmuFileHelper.h"
#include "FmuHelper.h"
#include "FmuCommunication.h"

#include "common/globalDefinitions.h"
#include "common/openScenarioDefinitions.h"
#include "common/acquirePositionSignal.h" //<src/...>

#include "include/signalInterface.h"
#include "include/fmuHandlerInterface.h"
#include "include/parameterInterface.h"
#include "include/worldInterface.h"
#include "include/agentInterface.h"
#include "include/parameterInterface.h"
#include "include/signalInterface.h"
#include "include/worldInterface.h"

#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_trafficcommand.pb.h>
#include <osi3/osi_trafficupdate.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>

#ifdef USE_EXTENDED_OSI
#include <osi3/sl45_motioncommand.pb.h>
#include <osi3/sl45_vehiclecommunicationdata.pb.h>
#endif


class CallbackInterface;
class AgentInterface;

template <size_t FMI>
class FmuHandler : public  FmuHandlerInterface      ///< Class representing a fmu handler
{
private:
    template <typename T>
    struct TypeContainer;

    const std::map<std::string, std::map<std::string, std::optional<std::string>&>> variableMapping{
        {"Input",
         {{"SensorView", sensorViewVariable},
          {"SensorViewConfig", sensorViewConfigVariable},
          {"SensorData", sensorDataInVariable},
          {"TrafficCommand", trafficCommandVariable},
#ifdef USE_EXTENDED_OSI
          {"VehicleCommunicationData", vehicleCommunicationDataVariable}
#endif
         }},
        {"Output",
         {{"SensorData", sensorDataOutVariable},
          {"SensorViewConfigRequest", sensorViewConfigRequestVariable},
          {"TrafficUpdate", trafficUpdateVariable},
          {"HostVehicleData", hostVehicleDataVariable},
#ifdef USE_EXTENDED_OSI
          {"MotionCommand", motionCommandVariable}
#endif
         }},
        {"Init",
         {{"GroundTruth", groundTruthVariable}}}};

    template <typename T>
    struct TypeContainer
    {
        using type = T;
        VariableType variableType;
    };

    std::regex transformationRegex{R"((\w+)\[(\w+>\w+)\]_(\w+))"};

    const std::set<std::string> parameterTransformations{"Transform", "TransformList"};

    /// TODO: storing with std::any works fine but poses potential problems when any-casting to const/volatile/reference/pointer mismatch
    const std::map<std::string, std::any> parameterTransformationMappings{
        {"ScenarioName>Id", static_cast<std::function<std::string(const std::string &)>>([this](const std::string &agentScenarioName) {
             const auto agent = world->GetAgentByName(agentScenarioName);
             if (agent == nullptr)
                 LOGERRORANDTHROW("Agent '" + agentScenarioName + "' not found in world.");
             return std::to_string(agent->GetId());
         })}};

public:

    /// @brief Functio to construct FMU handler
    /// @param componentName        Name of the component
    /// @param cdata                FMU check data
    /// @param fmuVariables         FMU variables
    /// @param fmuVariableValues    Values of FMU variables
    /// @param parameters           Pointer to the parameter interface
    /// @param world                Pointer to the world interface
    /// @param agent                Pointer to the agent interface
    /// @param callbacks            POinter to the callback interface
    FmuHandler<FMI>(std::string componentName,
                    fmu_check_data_t& cdata,
                    FmuVariables& fmuVariables,
                    std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                    const ParameterInterface *parameters,
                    WorldInterface *world,
                    AgentInterface *agent,
                    const CallbackInterface *callbacks);

    ~FmuHandler() override;

    void PrepareInit() override;

    /**
     * @brief initialize the component
     * 
     */
    void Init() override;

    void PreStep(int time) override;
    void PostStep(int time) override;

    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const>& data, int time) override;
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const>& data, int time) override;

    /*!
     * \brief Parses the channel definitions from FMU wrapper parameters
     * \param parameters    parameters defined in the config
     * \param fmuVariables  variables of the fmu
     *
     * Map from output link id to FMI value reference and type hash code
     */
    void ParseChannelDefinitions(const ParameterInterface* parameters, const FmuVariables& fmuVariables);

    ///! Sets the parameters in the FMU
    void SetFmuParameters();

    /// @brief Synchronize FMU variables and its paramters
    void SyncFmuVariablesAndParameters() override;

    ///! Public for testing
    ///! Reads an output value of the FMU
    ///! @param valueReferenc    value to the reference
    ///! @param variableType     Type of the variable
    ///! @return FMU value
    FmuValue& GetValue(int valueReferenc, VariableType variableType) const;

    /// @brief Function to read FMU variable values
    void ReadValues() override;

    /// @brief Function to write FMU variable values
    void WriteValues() override;

    /// @brief Function to set FMU values
    /// @param valueReference   value of a reference
    /// @param fmuValueIn       Input FMU value
    /// @param dataType         Type of the variable
    void SetFmuValue(int valueReference, FmuValue fmuValueIn, VariableType dataType) override;

    /// @brief Function to set list of FMU values
    /// @param valueReferences   list of values of a reference
    /// @param fmuValuesIn       list of Input FMU value
    /// @param dataType          Type of the variable
    void SetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue> fmuValuesIn, VariableType dataType) override;
    
    /// @brief Function to get FMU values
    /// @param valueReference   value of a reference
    /// @param fmuValueOut      Input FMU value
    /// @param dataType         Type of the variable
    void GetFmuValue(int valueReference, FmuValue& fmuValueOut, VariableType dataType) override;

    /// @brief Function to get list of FMU values
    /// @param valueReferences   list of values of a reference
    /// @param fmuValuesOut      list of Input FMU value
    /// @param dataType          Type of the variable
    void GetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue>& fmuValuesOut, VariableType dataType) override;

    /// @return Function to get FMU variables
    FmuVariables GetFmuVariables() override;

    /// @brief Function to handle FMI status
    /// @param status       Status of 
    /// @param logPrefix    Log prefix
    void HandleFmiStatus(jm_status_enu_t status, std::string logPrefix) override;
    
    /// @brief Prepare FMU initialization
    /// @return status of FMI Library
    jm_status_enu_t PrepareFmuInit() override;

    /// @brief End handling of FMI library
    /// @return status of FMI Library
    jm_status_enu_t FmiEndHandling() override;

    /// @brief Simulate step of FMI library
    /// @param time Time of the simulation
    /// @return status of FMI Library
    jm_status_enu_t FmiSimulateStep(double time) override;

    /// @brief Prepare simulation for FMI
    /// @return status of FMI Library
    jm_status_enu_t FmiPrepSimulate() override;

private:
    void InitOsiVariables(const ParameterInterface *parameters);
    FmuValue& GetFmuSignalValue(SignalValue signalValue, VariableType variableType);

    bool IsDefinedAsParameter(int desiredValRef, VariableType desiredType);
    std::variant<FmuVariable1*, FmuVariable2*>* GetFmuVariable(int desiredValRef, VariableType desiredType);

    template <typename GetFunction, typename TargetParameterField, typename UnderlyingType>
    void ParseFmuParametersByType(GetFunction getParametersType,
                                  const ParameterInterface *parameterInterface,
                                  TargetParameterField fmuTypeParameters,
                                  TypeContainer<UnderlyingType> typeContainer);

    //! Reads the parameters in the profile that should be forwarded to the FMU
    void ParseFmuParameters(const ParameterInterface *parameters);

    //! Sets the SensorView as input for the FMU
    void SetSensorViewInput(const osi3::SensorView &data);

    //! Sets the SensorData as input for the FMU
    void SetSensorDataInput(const osi3::SensorData& data);

    //! Reads the SensorData from the FMU
    void GetSensorData();

    //! Sets the SensorViewConfig as input for the FMU
    void SetSensorViewConfig();

    //! Sets the SensorViewConfigRequest
    void SetSensorViewConfigRequest();

    //! Sets the TrafficCommand as input for the FMU
    void SetTrafficCommandInput(const osi3::TrafficCommand &data);

    //! Sets the TrafficCommand as input for the FMU
    void SetHostVehicleDataInput(const osi3::HostVehicleData &data);

    //! Reads the TrafficUpdate from the FMU
    void GetTrafficUpdate();

    //! Reads the TrafficUpdate from the FMU
    void GetHostVehicleData();

    //! Sets GroundTruth
    void SetGroundTruth();

#ifdef USE_EXTENDED_OSI
    //! Reads the MotionCommand from the FMU
    void GetMotionCommand();

    //! Sets the VehicleCommunicationData as input for the FMU
    void SetVehicleCommunicationDataInput(const setlevel4to5::VehicleCommunicationData &data);

    //! Sets the MotionCommand as input for the FMU (makes it also available for json output, this needs refactoring)
    void SetMotionCommandDataInput(const setlevel4to5::MotionCommand &data);

#endif

private:
    fmi_version_enu_t fmiVersionEnum;

    //! Mapping from FMI value reference and C++ type id to FmuWrapper value (union). Provided by FmuWrapper on construction.
    std::map<ValueReferenceAndType, FmuValue>& fmuVariableValues;

    FmuOutputs fmuOutputs;                      //!< Mapping from component output link id to to FMI value reference and C++ type id
    std::set<SignalType> outputSignals;

    FmuInputs fmuRealInputs;
    FmuInputs fmuIntegerInputs;
    FmuInputs fmuBooleanInputs;
    FmuInputs fmuStringInputs;
    FmuParameters<int> fmuIntegerParameters;
    FmuParameters<double> fmuDoubleParameters;
    FmuParameters<bool> fmuBoolParameters;
    FmuParameters<std::string> fmuStringParameters;

    std::vector<int> sensors;
    WorldInterface* world;
    const ParameterInterface* parameters;

    FmuVariables& fmuVariables;
    const std::string agentIdString;
    std::string fmuName;

    std::string serializedSensorDataIn;
    std::string appendedSerializedSensorDataIn;
    std::string appendedSerializedSensorDataOut;
    std::string previousSerializedSensorDataIn;
    std::string serializedSensorView;
    std::string previousSerializedSensorView;
    std::string appendedSerializedSensorView;
    void* previousSensorDataOut{nullptr};
    osi3::SensorViewConfiguration sensorViewConfig;
    osi3::SensorViewConfiguration sensorViewConfigRequest;
    osi3::GroundTruth groundTruth;
    std::string serializedSensorViewConfig;
    std::string appendedSerializedSensorViewConfig;
    std::string serializedSensorViewConfigRequest;
    std::string appendedSerializedSensorViewConfigRequest;
    std::string previousSerializedSensorViewConfigRequest;
    osi3::SensorData sensorDataIn;
    osi3::SensorData sensorDataOut;
    std::string serializedGroundTruth;
    std::string appendedSerializedGroundTruth;
    std::string serializedTrafficCommand;
    std::string appendedSerializedTrafficCommand;
    std::string previousSerializedTrafficCommand;
    std::string serializedHostVehicleData;
    std::string appendedSerializedHostVehicleData;
    std::string previousSerializedHostVehicleData;
    osi3::TrafficUpdate trafficUpdate;
    std::string serializedTrafficUpdate;
    std::string appendedSerializedTrafficUpdate;
    void* previousTrafficUpdate{nullptr};
    void* previousHostVehicleData{nullptr};
    osi3::TrafficCommand trafficCommand;
    osi3::HostVehicleData hostVehicleData;


    QString outputDir{};
    QString traceOutputDir{};
    std::string oldFileName{};

    std::map<std::string, FmuFileHelper::TraceEntry> fileToOutputTracesMap{};

    Common::Vector2d previousPosition{0.0,0.0};
    std::ofstream traceOutputFile;

    double bb_center_offset_x{0.0};    //!< Offset of bounding box center to agent reference point (rear axle)

    //Use existing methods in FmuHandler if possible, instead of using FmuCommunication directly
    std::shared_ptr<FmuCommunication<FMI>> fmuCommunications;
    bool fmuVariablesParsed {false};

    bool isInitialized {false};

#ifdef USE_EXTENDED_OSI
    std::string serializedVehicleCommunicationData;
    std::string appendedSerializedVehicleCommunicationData;
    std::string previousSerializedVehicleCommunicationData;
    setlevel4to5::VehicleCommunicationData vehicleCommunicationData;
    setlevel4to5::MotionCommand motionCommand;
    std::string previousSerializedMotionCommand;
    std::string serializedMotionCommand;
    std::string appendedSerializedMotionCommand;
    void* previousMotionCommand{nullptr};
#endif
    std::optional<std::string> sensorViewVariable;
    std::optional<std::string> sensorViewConfigVariable;
    std::optional<std::string> sensorViewConfigRequestVariable;
    std::optional<std::string> sensorDataInVariable;
    std::optional<std::string> sensorDataOutVariable;
    std::optional<std::string> groundTruthVariable;
    std::optional<std::string> trafficCommandVariable;
    std::optional<std::string> trafficUpdateVariable;
    std::optional<std::string> hostVehicleDataVariable;

#ifdef USE_EXTENDED_OSI
    std::optional<std::string> motionCommandVariable;
    std::optional<std::string> vehicleCommunicationDataVariable;
#endif

    bool writeSensorView{false};
    bool writeSensorViewConfig{false};
    bool writeSensorViewConfigRequest{false};
    bool writeSensorData{false};
    bool writeGroundTruth{false};
    bool writeTrafficCommand{false};
    bool writeTrafficUpdate{false};
    bool writeHostVehicleData{false};

    bool writeTraceSensorView{false};
    bool writeTraceSensorViewConfig{false};
    bool writeTraceSensorViewConfigRequest{false};
    bool writeTraceSensorData{false};
    bool writeTraceGroundTruth{false};
    bool writeTraceTrafficCommand{false};
    bool writeTraceTrafficUpdate{false};
    bool writeTraceHostVehicleData{false};

#ifdef USE_EXTENDED_OSI
    bool writeMotionCommand{false};
    bool writeVehicleCommunicationData{false};

    bool writeTraceMotionCommand{false};
    bool writeTraceVehicleCommunicationData{false};
#endif

    //! check for double buffering of OSI messages allocated by FMU
    bool enforceDoubleBuffering{false};
};

#include "FmuHandler.tpp"