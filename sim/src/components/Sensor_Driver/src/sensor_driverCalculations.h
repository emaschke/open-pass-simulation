/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once
#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"

//! Utility class for the sensor driver
class SensorDriverCalculations
{
public:

    /**
     * @brief Construct a new Sensor Driver Calculations object
     * 
     * @param egoAgent Ego agent
     */
    SensorDriverCalculations(const EgoAgentInterface& egoAgent) :
        egoAgent(egoAgent)
    {}

    /**
     * @brief Get the Lateral Distance To Object object
     * 
     * @param roadId        TODO
     * @param otherObject 
     * @return double 
     */
    double GetLateralDistanceToObject(const std::string& roadId, const AgentInterface *otherObject);

private:
    const EgoAgentInterface& egoAgent;
};
