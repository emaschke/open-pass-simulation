/********************************************************************************
 * Copyright (c) 2018 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  AlgorithmCar2XSenderImplementation.cpp */
//-----------------------------------------------------------------------------

#include <memory>
#include <QtGlobal>
#include <QCoreApplication>

#include "include/observationInterface.h"
#include "include/worldInterface.h"
#include "include/parameterInterface.h"
#include "AlgorithmCar2XSenderImplementation.h"

AlgorithmCar2XSenderImplementation::AlgorithmCar2XSenderImplementation(std::string componentName,
        bool isInit,
        int priority,
        int offsetTime,
        int responseTime,
        int cycleTime,
        StochasticsInterface *stochastics,
        WorldInterface *world,
        const ParameterInterface *parameters,
        PublisherInterface * const publisher,
        const CallbackInterface *callbacks,
        AgentInterface *agent) :
    UnrestrictedModelInterface(
        componentName,
        isInit,
        priority,
        offsetTime,
        responseTime,
        cycleTime,
        stochastics,
        world,
        parameters,
        publisher,
        callbacks,
        agent)
 {
    // read parameters
    try
    {
        signalStrength = parameters->GetParametersDouble().at("SignalStrength");
        if(parameters->GetParametersBool().at("SendPositionXEnabled"))
        {
            informationToSend.push_back(SensorInformationType::PositionX);
        }
        if(parameters->GetParametersBool().at("SendPositionYEnabled"))
        {
            informationToSend.push_back(SensorInformationType::PositionY);
        }
        if(parameters->GetParametersBool().at("SendVelocityEnabled"))
        {
            informationToSend.push_back(SensorInformationType::Velocity);
        }
        if(parameters->GetParametersBool().at("SendAccelerationEnabled"))
        {
            informationToSend.push_back(SensorInformationType::Acceleration);
        }
        if(parameters->GetParametersBool().at("SendYawEnabled"))
        {
            informationToSend.push_back(SensorInformationType::Yaw);
        }
    }
    catch(...)
    {
        const std::string msg = COMPONENTNAME + " could not init parameters";
        LOG(CbkLogLevel::Error, msg);
        throw std::runtime_error(msg);
    }

    try
    {
        if (GetPublisher() == nullptr)    throw std::runtime_error("");
    }
    catch(...)
    {
        const std::string msg = COMPONENTNAME + " invalid observation module setup";
        LOG(CbkLogLevel::Error, msg);
        throw std::runtime_error(msg);
    }
}

void AlgorithmCar2XSenderImplementation::UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{
    Q_UNUSED(localLinkId);
    Q_UNUSED(data);
    Q_UNUSED(time);
}

void AlgorithmCar2XSenderImplementation::UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time)
{
    Q_UNUSED(localLinkId);
    Q_UNUSED(data);
    Q_UNUSED(time);
}

void AlgorithmCar2XSenderImplementation::Trigger(int time)
{
    Q_UNUSED(time);
    const auto movingObject = FillObjectInformation();

    GetWorld()->GetRadio().Send(GetAgent()->GetPositionX(), GetAgent()->GetPositionY(), signalStrength, movingObject);
}


osi3::MovingObject AlgorithmCar2XSenderImplementation::FillObjectInformation()
{

    osi3::MovingObject object;
    Common::Vector2d ownVelocity{0.0, 0.0};
    Common::Vector2d ownAcceleration{0.0, 0.0};
    
    for (auto information : informationToSend)
    {
        switch(information)
        {
            case SensorInformationType::PositionX:
                object.mutable_base()->mutable_position()->set_x(GetAgent()->GetPositionX());
                break;

            case SensorInformationType::PositionY:
                object.mutable_base()->mutable_position()->set_y(GetAgent()->GetPositionY());
                break;

            case SensorInformationType::Yaw:
                object.mutable_base()->mutable_orientation()->set_yaw(GetAgent()->GetYaw());
                break;

            case SensorInformationType::Velocity:
                ownVelocity = GetAgent()->GetVelocity();
                object.mutable_base()->mutable_velocity()->set_x(ownVelocity.x);
                object.mutable_base()->mutable_velocity()->set_y(ownVelocity.y);
                break;

            case SensorInformationType::Acceleration:
                ownAcceleration = GetAgent()->GetAcceleration();
                object.mutable_base()->mutable_acceleration()->set_x(ownAcceleration.x);
                object.mutable_base()->mutable_acceleration()->set_y(ownAcceleration.y);
                break;

            default:
                const std::string msg = COMPONENTNAME + " invalid sensorInformationType";
                LOG(CbkLogLevel::Debug, msg);
                break;
        }

    }
    
    return object;
}

