::################################################################################
::# Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
::#
::# This program and the accompanying materials are made available under the
::# terms of the Eclipse Public License 2.0 which is available at
::# http://www.eclipse.org/legal/epl-2.0.
::#
::# SPDX-License-Identifier: EPL-2.0
::################################################################################

@echo off
set ME=%0
set LC_ALL=C

if [%1] == [] (
  call :showUsage
  exit /b 1
)

set TARGET_PATH=%~f1

set MSYS2_TARGET_PATH=%TARGET_PATH%\msys64
set MSYS2_SHELL=%MSYS2_TARGET_PATH%\usr\bin\bash.exe -lc
set MSYS2_PACKAGE=msys2-base-x86_64-latest.sfx.exe
set MSYS2_URL=https://github.com/msys2/msys2-installer/releases/download/nightly-x86_64/%MSYS2_PACKAGE%

set PYTHON_TARGET_PATH=%TARGET_PATH%\Python
set PYTHON_VERSION=3.10.4
set PYTHON_PACKAGE=python-%PYTHON_VERSION%-amd64.exe
set PYTHON_URL=https://www.python.org/ftp/python/%PYTHON_VERSION%/%PYTHON_PACKAGE%

set MSVCRT_URL=https://aka.ms/highdpimfc2013x64enu
set MSVCRT_PACKAGE=vcredist_x64.exe

set OLD_PATH=%PATH%
set MSYSTEM=MINGW64
set CHERE_INVOKING=yes
set PATH=%MSYS2_TARGET_PATH%;%PATH%

call :getAbsPath %~dp0..\..
set REPO_ROOT=%ABS_PATH%

echo.
echo ==========================================
echo openPASS development environment bootstrap
echo ==========================================
echo.

pushd %TEMP%

echo Installing MCVCRT120...
if not exist %MSVCRT_PACKAGE% (
  echo   downloading...
  powershell -command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile(${env:MSVCRT_URL}, ${env:MSVCRT_PACKAGE})"
  if %ERRORLEVEL% neq 0 GOTO cleanup
)

echo   installing...
%MSVCRT_PACKAGE% /passive /quiet /norestart
if %ERRORLEVEL% neq 0 GOTO cleanup

if not exist %MSYS2_TARGET_PATH% (
  echo Installing msys2...
  if not exist %MSYS2_PACKAGE% (
    echo   downloading...
    rem curl -s -f -L -O %MSYS2_URL%
    powershell -command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile(${env:MSYS2_URL}, ${env:MSYS2_PACKAGE})"
    if %ERRORLEVEL% neq 0 GOTO cleanup
  )

  echo   installing...
  %MSYS2_PACKAGE% -y -o%TARGET_PATH%
  if %ERRORLEVEL% neq 0 GOTO cleanup

  echo First launch of msys2...
  %MSYS2_SHELL% "true"
)

echo.
echo Running msys2 core update...
%MSYS2_SHELL% "pacman -Syuu --noconfirm"
if %ERRORLEVEL% neq 0 GOTO cleanup

echo.
echo Running msys2 update...
%MSYS2_SHELL% "pacman -Syuu --noconfirm"
if %ERRORLEVEL% neq 0 GOTO cleanup

echo.
echo Installing/upgrading required MSYS packages...
rem sed command strips comments from msys2_packages.txt file
%MSYS2_SHELL% "pacman -S --noconfirm --needed $(sed -e '/^\W*#/d;s/#.*//;/^$/d' $(cygpath -a $REPO_ROOT)doc/source/installation_guide/_static/msys2_packages.txt)"
if %ERRORLEVEL% neq 0 GOTO cleanup

echo.
echo Upgrading pip...
%MSYS2_SHELL% "python -m pip install -U pip"
if %ERRORLEVEL% neq 0 GOTO cleanup

echo.
echo Installing/upgrading Python packages for documentation build...
%MSYS2_SHELL% "pip install -U -r $(cygpath -a $REPO_ROOT)doc/source/requirements.txt"
if %ERRORLEVEL% neq 0 GOTO cleanup

echo.
echo Installing Python for E2E tests...
if not exist %PYTHON_TARGET_PATH%\python.exe (
  if not exist %PYTHON_PACKAGE% (
    echo   downloading...
    rem curl -s -f -L -O %PYTHON_URL%
    powershell -command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile(${env:PYTHON_URL}, ${env:PYTHON_PACKAGE})"
    if %ERRORLEVEL% neq 0 GOTO cleanup
  )

  echo   installing...
  rem See https://docs.python.org/3/using/windows.html#installing-without-ui
  %PYTHON_PACKAGE% InstallAllUsers=1 AssociateFiles=0 Shortcuts=0 Include_doc=0 Include_launcher=0 Include_tcltk=0 Include_test=0 InstallLauncherAllUsers=0 DefaultAllUsersTargetDir=%PYTHON_TARGET_PATH%
  if %ERRORLEVEL% neq 0 GOTO cleanup
)

echo Upgrading pip...
"%PYTHON_TARGET_PATH%\python.exe" -m pip install -U pip
if %ERRORLEVEL% neq 0 GOTO cleanup

echo Installing/upgrading Python packages for E2E tests...
"%PYTHON_TARGET_PATH%\python.exe" -m pip install -U -r %REPO_ROOT%\sim\tests\endToEndTests\pyOpenPASS\requirements.txt
if %ERRORLEVEL% neq 0 GOTO cleanup

echo.
echo ====
echo DONE
echo ====
echo.

set PATH=%OLD_PATH%
popd
exit /b 0

:showUsage
echo Usage: %ME% ^<openPASS-build-env-prefix^>
goto :eof

:getAbsPath
SET ABS_PATH=%~dp1
goto :eof

:cleanup
set PATH=%OLD_PATH%
popd
exit /b 1
